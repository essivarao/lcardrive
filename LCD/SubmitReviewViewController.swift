//
//  SubmitReviewViewController.swift
//  LCD
//
//  Created by kvanaworld on 3/23/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class SubmitReviewViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    var instructorId: Int?
    var lessonId: Int?
    
    @IBOutlet weak var oneStar: UIButton!
    @IBOutlet weak var twoStar: UIButton!
    @IBOutlet weak var threeStar: UIButton!
    @IBOutlet weak var fourStar: UIButton!
    @IBOutlet weak var fiveStar: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageTextView: UITextView!
    var selectedIndex: Int?
    var details: [MasterDetail] = []
    var selectedRate = 0
    
    let columnNum: CGFloat = 2 //use number of columns instead of a static maximum cell width
    var cellWidth: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        details.append(contentsOf: AppDelegate.shared.masterDetails.filter({$0.masterTypeId == 8}))
        skipButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        submitButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        messageTextView.text = ""
        
        messageTextView.layer.cornerRadius = 5
        messageTextView.layer.borderWidth = 2
        messageTextView.layer.borderColor = UIColor.gray.cgColor

        collectionView.register(UINib(nibName: "LabelCollectionViewCell1", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10) //could not set in storyboard, don't know why
        }


    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func starClicked(_ sender: UIButton) {
        
        if oneStar == sender {
            oneStar.setImage(#imageLiteral(resourceName: "ic_Star_active"), for: .normal)
            
            twoStar.setImage(#imageLiteral(resourceName: "ic_Star_inactive"), for: .normal)
            threeStar.setImage(#imageLiteral(resourceName: "ic_Star_inactive"), for: .normal)
            fourStar.setImage(#imageLiteral(resourceName: "ic_Star_inactive"), for: .normal)
            fiveStar.setImage(#imageLiteral(resourceName: "ic_Star_inactive"), for: .normal)
            selectedRate = 1
        }
        else if twoStar == sender {
            oneStar.setImage(#imageLiteral(resourceName: "ic_Star_active"), for: .normal)
            twoStar.setImage(#imageLiteral(resourceName: "ic_Star_active"), for: .normal)
            
            threeStar.setImage(#imageLiteral(resourceName: "ic_Star_inactive"), for: .normal)
            fourStar.setImage(#imageLiteral(resourceName: "ic_Star_inactive"), for: .normal)
            fiveStar.setImage(#imageLiteral(resourceName: "ic_Star_inactive"), for: .normal)
            selectedRate = 2
        }
        else if threeStar == sender{
            oneStar.setImage(#imageLiteral(resourceName: "ic_Star_active"), for: .normal)
            twoStar.setImage(#imageLiteral(resourceName: "ic_Star_active"), for: .normal)
            threeStar.setImage(#imageLiteral(resourceName: "ic_Star_active"), for: .normal)
            
            fourStar.setImage(#imageLiteral(resourceName: "ic_Star_inactive"), for: .normal)
            fiveStar.setImage(#imageLiteral(resourceName: "ic_Star_inactive"), for: .normal)
            selectedRate = 3
        }
        else if fourStar == sender {
            oneStar.setImage(#imageLiteral(resourceName: "ic_Star_active"), for: .normal)
            twoStar.setImage(#imageLiteral(resourceName: "ic_Star_active"), for: .normal)
            threeStar.setImage(#imageLiteral(resourceName: "ic_Star_active"), for: .normal)
            fourStar.setImage(#imageLiteral(resourceName: "ic_Star_active"), for: .normal)
            
            fiveStar.setImage(#imageLiteral(resourceName: "ic_Star_inactive"), for: .normal)
            selectedRate = 4
        }
        else if fiveStar == sender{
            oneStar.setImage(#imageLiteral(resourceName: "ic_Star_active"), for: .normal)
            twoStar.setImage(#imageLiteral(resourceName: "ic_Star_active"), for: .normal)
            threeStar.setImage(#imageLiteral(resourceName: "ic_Star_active"), for: .normal)
            fourStar.setImage(#imageLiteral(resourceName: "ic_Star_active"), for: .normal)
            fiveStar.setImage(#imageLiteral(resourceName: "ic_Star_active"), for: .normal)
            selectedRate = 5
        }
        
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        //recalculate the collection view layout when the view layout changes
        collectionView.collectionViewLayout.invalidateLayout()
    }

    //Collection view
    override func viewDidLayoutSubviews()
    {
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let spaceBetweenCells = flowLayout.minimumInteritemSpacing * (columnNum - 1)
            let totalCellAvailableWidth = collectionView.frame.size.width - flowLayout.sectionInset.left - flowLayout.sectionInset.right - spaceBetweenCells
            cellWidth = floor(totalCellAvailableWidth / columnNum);
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! LabelCollectionViewCell1
        
        cell.textContent = details[indexPath.row].value ?? ""
        cell.configureWithIndexPath(indexPath: indexPath as NSIndexPath)
        if selectedIndex == indexPath.row {
            cell.backgroundColor = UIColorFromRGB(rgbValue: Color.APP_THEMECOLOR)
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if let cell = LabelCollectionViewCell1.fromNib() {
            cell.textContent = details[indexPath.row].value ?? ""

            let cellMargins = cell.layoutMargins.left + cell.layoutMargins.right
            cell.configureWithIndexPath(indexPath: indexPath as NSIndexPath)
            cell.messageLabel.preferredMaxLayoutWidth = cellWidth - cellMargins
            cell.mesglabelWidthLayoutConstraint.constant = cellWidth - cellMargins //adjust the width to be correct for the number of columns
            return cell.contentView.systemLayoutSizeFitting(UILayoutFittingCompressedSize) //apply auto layout and retrieve the size of the cell
        }
        return CGSize.zero
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        selectedIndex = indexPath.row
        collectionView.reloadData()
        
    }
    
    @IBAction func skipAction(_ sender: Any) {
        var payload:[String:Any] = [:]
        payload["LessonId"] = lessonId ?? 0
        payload["IsSkipped"] = true
        showProgress()
        NetworkInterface.fetchJSON(.submitRating, payload: payload) { (success, data, response, error) -> (Void) in
            self.hideProgress()
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if selectedRate == 0 {
            AppDelegate.shared.window?.makeToast("Please select rating", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        if selectedRate < 4 && selectedIndex == nil {
                AppDelegate.shared.window?.makeToast("Please select any feedback type", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        var payload:[String:Any] = [:]
            payload["LearnerId"] = Learner.shared.userID ?? 0
            payload["InstructorId"] = instructorId ?? 0
            payload["LessonId"] = lessonId ?? 0
            payload["Rating"] = selectedRate 
            payload["Comments"] = messageTextView.text
            payload["IsSkipped"] = false
        if selectedIndex != nil {
            payload["FeedbackTypeId"] = self.details[selectedIndex!].masterDetailId ?? 0
        }

        showProgress()
        NetworkInterface.fetchJSON(.submitRating, payload: payload) { (success, data, response, error) -> (Void) in
            self.hideProgress()
            DispatchQueue.main.async {
                if let data = data as? [String:Any], let serviceMessage = data["ServiceMessage"] as? String, data["StatusCode"] as? Int == 0 {
                    AppDelegate.shared.window?.makeToast(serviceMessage, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                }else if let data = data as? [String:Any], let serviceMessage = data["ServiceMessage"] as? String{
                    AppDelegate.shared.window?.makeToast(serviceMessage, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                }else{
                    AppDelegate.shared.window?.makeToast("Error on submiting review", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                }
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
}
