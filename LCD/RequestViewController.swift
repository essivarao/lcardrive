//
//  RequestViewController.swift
//  DrivingAUS
//
//  Created by Admin on 21/02/17.
//  Copyright © 2017 Sarella. All rights reserved.
//

import UIKit

class RequestViewController: UIViewController , UITableViewDelegate, UITableViewDataSource , UITextFieldDelegate{

    
    var expandedRows = Set<Int>()
    var filteredResult:[InstructorRequest] = []
    var totalResults:[InstructorRequest] = []

    @IBOutlet var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        searchTextField.addTarget(self, action: #selector(RequestViewController.textFieldDidChange(textField:)), for: .editingChanged)

        searchTextField.delegate = self
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        fetchRequests()
    }
    func textFieldDidChange(textField: UITextField){
        filteredResult.removeAll()
        
        if textField.text?.characters.count == 0 {
            filteredResult.append(contentsOf: totalResults)
        }else{
            filteredResult = totalResults.filter({ $0.learnerName?.lowercased().range(of: (textField.text?.lowercased())!) != nil })
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
    }
    
    // TableView DataSource methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredResult.count == 0 {
            self.tableView.isHidden = true
        }
        else{
            self.tableView.isHidden = false
        }
        return filteredResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ExpandableCell = tableView.dequeueReusableCell(withIdentifier: "ExpandableCell") as! ExpandableCell
        let request =  filteredResult[indexPath.row]
        cell.request = request
        cell.isExpanded = self.expandedRows.contains(indexPath.row)
        cell.img.backgroundColor = UIColor.clear
        
        cell.prefrencesButton.addTarget(self, action: #selector(prefrencesButtonClicked(sender:)), for: .touchUpInside)
        cell.prefrencesButton.tag = indexPath.row
        
        cell.viewMessageButton.addTarget(self, action: #selector(viewMessagesClicked(sender:)), for: .touchUpInside)
        cell.viewMessageButton.tag = indexPath.row

        cell.selectionStyle = .none
        
        
        cell.callButton.tag = indexPath.row
        cell.callButton.addTarget(self, action: #selector(LearnerRequestViewController.callButtonClicked(sender:)), for: .touchUpInside)
        
        cell.sendMessageButton.addTarget(self, action: #selector(sendMessageButtonClicked(sender:)), for: .touchUpInside)
        cell.sendMessageButton.tag = indexPath.row

        cell.applyDataOnCell(reuest: request)
        
        cell.bookLessonButton.addTarget(self, action: #selector(bookLessonClicked(sender:)), for: .touchUpInside)
        cell.bookLessonButton.tag = indexPath.row
        return cell
    }
    
    
 
    
    func callButtonClicked(sender : UIButton) {
        let user = filteredResult[sender.tag]
        let phoneNum = "+61\(user.learnerPhoneNumber!)"
        if let url = URL(string: "telprompt://\(phoneNum)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
        else {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window?.makeToast("Phone number not valid.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
        }
    }

    
    func bookLessonClicked(sender: UIButton) {
        let selectedRequest = filteredResult[sender.tag]
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "InstructorBookLess") as! InstructorBookLessViewController
        let request = BookLessonRequest()
        request.imageURL = selectedRequest.learnerProfilePic
        request.InstructorId = selectedRequest.instructorId
        request.LearnerId = selectedRequest.learnerId
        request.UserId = Instructor.shared.userID
        request.instructorName = selectedRequest.learnerName
        vc.request = request
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func prefrencesButtonClicked(sender : UIButton) {
        let indexPath = IndexPath.init(row: sender.tag, section: 0)
        guard let cell = tableView.cellForRow(at: indexPath) as? ExpandableCell
            else { return }
        switch cell.isExpanded
        {
        case true:
            self.expandedRows.remove(indexPath.row)
        case false:
            self.expandedRows.insert(indexPath.row)
        }
        
        cell.isExpanded = !cell.isExpanded
        
        self.tableView.beginUpdates()
        self.tableView.endUpdates()

    }
    
    func viewMessagesClicked(sender : UIButton)  {
        let request = filteredResult[sender.tag]

        let viewController:InstructorMessagesViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InstructorMessagesViewController") as! InstructorMessagesViewController
        viewController.requestId = request.requestId

        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    func sendMessageButtonClicked(sender : UIButton) {
        
        let request = filteredResult[sender.tag]
        let viewController:EnterMessageViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "enterMessageViewController") as! EnterMessageViewController
//        viewController.learnerId = request.learnerId
        viewController.instructorId = request.instructorId
        viewController.requestID = request.requestId
        
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: false, completion: nil)

    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 184.0
    }

    
}


extension RequestViewController{
    func fetchRequests(){
        showProgress()
        NetworkInterface.fetchJSON(.instructorRequestsList, params:["instructorId":"\(Instructor.shared.userID!)"]) { (success, data, response, error) -> (Void) in
            if let data = data as? [String:Any], let requstList = data["RequestList"] as? [[String:Any]]{
                self.expandedRows.removeAll()
                self.totalResults.removeAll()
                self.filteredResult.removeAll()
                requstList.forEach({ (dict) in
                    let obj = InstructorRequest.init(dictionary: dict as NSDictionary)
                    self.filteredResult.append(obj!)
                    self.totalResults.append(obj!)

                })
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            }
            DispatchQueue.main.async {
                self.hideProgress()
            }
        }
    }
}





