//
//  LearnerSignInViewController.swift
//  LCD
//
//  Created by Admin on 02/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit
import MBProgressHUD
import Firebase

protocol UserWithOutSessionDelegate {
    func userSessionCreated()
}

class LearnerSignInViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    let facebookButton = FBSDKLoginButton()

    var sessionDelegate: UserWithOutSessionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        facebookButton.readPermissions = ["public_profile","email"]
        facebookButton.delegate = self
        
        let fbButton = UIButton.init(frame: CGRect(x: 44, y: passwordTextField.frame.size.height + passwordTextField.frame.origin.y + 400 , width: self.view.frame.size.width - 88, height: 40))
        fbButton.center.x = self.view.center.x
        fbButton.titleLabel?.font = UIFont(name: FontName.REGULAR, size: CGFloat(FontSize.BUTTON_MEDIUM))
        fbButton.backgroundColor = UIColorFromRGB(rgbValue: 0x3b5998)
        fbButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        self.view.addSubview(fbButton)
        fbButton.setTitle("Login with Facebook", for: .normal)
        fbButton.addTarget(self, action: #selector(LCDSignInViewController.facebookButtonAction), for: .touchUpInside)
        
        
        let facebookBurronImage = UIImageView()
        facebookBurronImage.frame = CGRect(x: 40, y: 5, width: 30, height: 30)
        facebookBurronImage.image = #imageLiteral(resourceName: "facebookBurron")
        fbButton.addSubview(facebookBurronImage)
        fbButton.titleEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        
        if FBSDKAccessToken.current() != nil {
            FBSDKLoginManager().logOut()
        }
        
        // Do any additional setup after loading the view.
    }
    
    func facebookButtonAction(){
        facebookButton.sendActions(for: .touchUpInside)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func signInClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate

        if  (!isValid(email: emailTextField.text!)) && (!isValid(signInPhoneNumber: emailTextField.text!)) {
            appdelegate.window?.makeToast("Email/Phone number required.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        else if (!isValid(password: passwordTextField.text!)) {
            appdelegate.window?.makeToast("password required.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        else{
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            var params = ["UserName": emailTextField.text!, "Password":passwordTextField.text!, "UserType": "L"] as [String:Any]
            if let firToken = FIRInstanceID.instanceID().token() {
                params["FCMRegistrationId"] = firToken
                params["DeviceToken"] = AppDelegate.shared.deviceToken ?? ""
                params["DeviceTypeId"] = 18
            }
            NetworkInterface.fetchJSON(.login, headers: [:], params: [:], payload: params) { (success, data, response, error) -> (Void) in
                DispatchQueue.main.async {
                     MBProgressHUD.hide(for: self.view, animated: true)
                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
                    if  let data = data, let status = data["ServiceStatus"] as? [String:Any], status["StatusCode"] as? Int == 0, let message = status["ServiceMessage"] as? String {
                        appdelegate.window?.makeToast(message, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                        Learner.shared.create(dictionary: data)
                        Learner.shared.synchronize()
                        let storyboard = UIStoryboard(name: "Learner", bundle: nil)
                        let tabbar = storyboard.instantiateViewController(withIdentifier: "leaenerTabbar") as! UITabBarController
                        appdelegate.window?.rootViewController = tabbar
                        if SearchUserDetailWithOutSignInViewController.selectedUserInMemory != nil {
                            tabbar.selectedIndex = 0
                        }else{
                            tabbar.selectedIndex = 1
                        }
                        AppDelegate.shared.checkLernerNeedToRate()
                    }else if let status = data?["ServiceStatus"] as? [String:Any],let message = status["ErrorMessage"] as? String {
                         appdelegate.window?.makeToast(message, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    }else{
                         appdelegate.window?.makeToast("Please try again", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    }
                }
            }
        }
    }

    
    func displayAlert(_ title:String,message:String)  {
        let alert = UIAlertController(title: title , message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) -> Void in
            NSLog("button OK")
        }
        
        alert.addAction(defaultAction)
        present(alert, animated: true, completion:nil)  // 11
    }
    
}



extension LearnerSignInViewController: FBSDKLoginButtonDelegate {
    
    public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
        if result?.isCancelled == true {
            return
        }
        
        guard error == nil else {
            
            AppDelegate.shared.window?.makeToast("Error on facebook signin. Please try again.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)

            
            self.hideProgress()
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        //        let params = ["fields":"picture,email,name"]
        let params = ["fields":"picture,email,first_name,last_name"]
        
        let req:FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: params)
        req.start { (reqConnection, result, err) in
            
            guard let result = result as? [String:AnyObject],
                let email = result["email"] as? String,
                let first_name = result["first_name"] as? String,
                let id = result["id"] as? String,
                let last_name = result["last_name"] as? String
                else{
                    AppDelegate.shared.window?.makeToast("Error on facebook signin. Please try again.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    self.hideProgress()
                    return
            }
            let payload:Dictionary = ["FirstName":first_name,"LastName":last_name,"PhoneNumber":"" ,"Email":email,"UserType":"L","SCNKey":id] as Dictionary
            self.showProgress()
            NetworkInterface.fetchJSON(.facebookRegistration, payload: payload, requestCompletionHander: { (success, data, response, error) -> (Void) in
                DispatchQueue.main.async {
                    self.hideProgress()
                    if let ServiceStatus = data?["ServiceStatus"] as? [String:Any], ServiceStatus["StatusCode"] as? Int == 0 {
                        let lerner = Learner.shared
                        lerner.userID = data?["UserID"] as? Int
                        let name = data?["UserName"] as? String ?? " "
                        lerner.firstName = name.components(separatedBy: " ").first ?? ""
                        lerner.lastName = name.components(separatedBy: " ").count > 0 ? name.components(separatedBy: " ")[1] : ""
                        lerner.email = email
                        lerner.synchronize()
                        
                        //Change root here
                        AppDelegate.shared.learnerWithSession()
                        let storyboard = UIStoryboard(name: "Learner", bundle: nil)
                        let tabbar = storyboard.instantiateViewController(withIdentifier: "leaenerTabbar") as! UITabBarController
                        AppDelegate.shared.window?.rootViewController = tabbar
                        if SearchUserDetailWithOutSignInViewController.selectedUserInMemory != nil {
                            tabbar.selectedIndex = 0
                        }else{
                            tabbar.selectedIndex = 1
                        }
                        AppDelegate.shared.checkLernerNeedToRate()
                        
                        
                    }else{
                        AppDelegate.shared.window?.makeToast("Error on facebook signin. Please try again.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)

                        FBSDKLoginManager().logOut()
                    }
                }
            })
            
            
        }
    }
    
    
    
    public func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        NSLog("Logged out here")
        //Empty all the values here
        
    }
    
}

