//
//  LearnerLessonTableViewCell.swift
//  LCD
//
//  Created by Admin on 16/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class LearnerLessonTableViewCell: UITableViewCell {
   
    
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var rateLabel: UILabel!
    @IBOutlet var timeAndAddressLabel: UILabel!
    @IBOutlet var viewMessageButton: UIButton!
    @IBOutlet var sendmessageButton: UIButton!
    @IBOutlet var updateButton: UIButton!
    @IBOutlet var comfirmed: UIButton!
    @IBOutlet var updateButtonView: UIStackView!
    @IBOutlet var confirmButton: UIButton!
    @IBOutlet var rejectButton: UIButton!
    @IBOutlet var confirmButtonView: UIStackView!
    @IBOutlet var callButton: UIButton!
    @IBOutlet weak var rejectedLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.profileImageView.layer.cornerRadius = 35
        self.profileImageView.layer.masksToBounds = true
        
        
        let imageView = UIImageView()
        imageView.frame = CGRect(x: 10 , y: 7, width: 18, height: 18)
        imageView.image = #imageLiteral(resourceName: "call_plane")
        callButton.addSubview(imageView)
        callButton.titleEdgeInsets = UIEdgeInsetsMake(0, 20 , 0, 0)
        
        let imageView1 = UIImageView()
        imageView1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imageView1.image = #imageLiteral(resourceName: "view_messages")
        viewMessageButton.addSubview(imageView1)
        viewMessageButton.titleEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        
        let sendMessageView = UIImageView()
        sendMessageView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        sendMessageView.image = #imageLiteral(resourceName: "send_messages")
        sendmessageButton.addSubview(sendMessageView)
        sendmessageButton.titleEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        
        
//        completeButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
//        completeButton.layer.borderColor = UIColor.lightGray.cgColor
//        completeButton.layer.borderWidth = 2
        
        confirmButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        confirmButton.layer.borderColor = UIColor.lightGray.cgColor
        confirmButton.layer.borderWidth = 2
        
        rejectButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        rejectButton.layer.borderColor = UIColor.lightGray.cgColor
        rejectButton.layer.borderWidth = 2
        
        
        updateButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        updateButton.layer.borderColor = UIColor.lightGray.cgColor
        updateButton.layer.borderWidth = 2
        
        updateButtonView.isHidden = true
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func callButtonClicked(_ sender: Any) {
        
    }
  
    
    var lesson: LessonList? {
        didSet {
            nameLabel.text = (lesson?.instructorName)!
            descriptionLabel.text = (lesson?.lessonType)!
            timeAndAddressLabel.text = "\(lesson?.lessonDate?.formatterDate ?? "")@\((lesson?.suburb)!)"
            rateLabel.text = ""
            if let rate = lesson?.rate, let hour = lesson?.package?.hour {
                if hour == 1 || hour == 0{
                    rateLabel.text = "$\(rate) / Hour"
                }else{
                    rateLabel.text = "$\(rate) / \(hour)Hours"
                }
            }
            
            
            if let imageURL = lesson?.instructorProfilePic {
                let url = URL(string: imageURL)
                profileImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultFace"), options: nil, progressBlock: nil, completionHandler: nil)
            }else{
                profileImageView.image = #imageLiteral(resourceName: "defaultFace")
            }
            updateButton.isHidden = true
            comfirmed.isHidden = true
            
//            if lesson?.status != nil {
//                if (lesson?.status)! == "Rejected" {
//                confirmButton.backgroundColor = .clear
//                confirmButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
//                confirmButton.layer.borderColor = UIColor.white.cgColor
//                confirmButton.layer.borderWidth = 2
//                rejectButton.setTitle("REJECTED", for: .normal)
//                }
//           else if (lesson?.status)! ==  "Completed" {
//                confirmButton.backgroundColor = .clear
//                confirmButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
//                confirmButton.layer.borderColor = UIColor.white.cgColor
//                confirmButton.layer.borderWidth = 2
//                rejectButton.setTitle("COMPLETED", for: .normal)
//                }
//                else if (lesson?.status)! == "Confirmed" {
//                    confirmButton.backgroundColor = .clear
//                    confirmButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
//                    confirmButton.layer.borderColor = UIColor.white.cgColor
//                    confirmButton.layer.borderWidth = 2
//                    rejectButton.setTitle("COMPLETE", for: .normal)
//                }
//                else if (lesson?.status)! == "Pending" {
//                    confirmButton.setTitle("CONFIRM", for: .normal)
//                    rejectButton.setTitle("REJECTED", for: .normal)
//                    
//                    if !(lesson?.isInstructor)! {
//                        confirmButton.backgroundColor = .clear
//                        confirmButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
//                        confirmButton.layer.borderColor = UIColor.white.cgColor
//                        confirmButton.layer.borderWidth = 2
//                        rejectButton.setTitle("UPDATE", for: .normal)
//                    }
//            
//                }
//                
//    
//            }
            
        }
    }
}
