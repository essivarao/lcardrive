//
//  DictionaryExtention.swift
//  LCD
//
//  Created by Narendra Kumar R on 3/5/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import Foundation

extension Dictionary {
    func JSONStringify(prettyPrinted: Bool = true) -> String {
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : nil
        if JSONSerialization.isValidJSONObject(self) {
            do{
                var data: Data?
                if options == nil {
                    data = try JSONSerialization.data(withJSONObject: self)
                    
                }else{
                    data = try JSONSerialization.data(withJSONObject: self, options: options!)
                    
                }
                
                if let string = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) {
                    return string as String
                }
                
            }
            catch {
                print(error)
            }
        }
        
        return ""
    }
}

