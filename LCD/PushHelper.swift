//
//  PushHelper.swift
//  LCD
//
//  Created by Narendra Kumar R on 4/25/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import Foundation

extension AppDelegate{
    func handlePush(userInfo:[AnyHashable: Any]){
        
        guard let userInfo = userInfo as? [String:Any] else {
            return
        }
        
        if let notification_type = userInfo["notification_type"] as? String {
            if Instructor.shared.userID != nil {
                if notification_type == "2" {
                    if let rootvc = self.window?.rootViewController as? UITabBarController {
                        rootvc.selectedIndex = 1
                    }
                } else if notification_type == "4", let request_id = userInfo["request_id"] as? String {
                    
                    if let rootvc = self.window?.rootViewController as? UITabBarController {
                        rootvc.dismiss(animated: false, completion: nil)
                        rootvc.selectedIndex = 1
                        let viewController:InstructorMessagesViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InstructorMessagesViewController") as! InstructorMessagesViewController
                        viewController.requestId = Int(request_id)
                        
                        (rootvc.selectedViewController as? UINavigationController)?.pushViewController(viewController, animated: true)
                    }
                    
                } else if notification_type == "4", let request_id = userInfo["lesson_id"] as? String {
                    if let rootvc = self.window?.rootViewController as? UITabBarController {
                        rootvc.dismiss(animated: false, completion: nil)
                        rootvc.selectedIndex = 1
                        let viewController:InstructorMessagesViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InstructorMessagesViewController") as! InstructorMessagesViewController
                        viewController.requestId = Int(request_id)
                        viewController.isLesson = true
                        (rootvc.selectedViewController as? UINavigationController)?.pushViewController(viewController, animated: true)
                    }
                } else if notification_type == "4", let lessonDate = userInfo["lesson_date"] as? String {
                    if let rootvc = self.window?.rootViewController as? UITabBarController {
                        rootvc.selectedIndex = 2
                        ((rootvc.selectedViewController as? UINavigationController)?.topViewController as? InstructorLessonsViewController)?.selectedDate = dataFromString(string:lessonDate)
                        ((rootvc.selectedViewController as? UINavigationController)?.topViewController as? InstructorLessonsViewController)?.updateContent(date: dataFromString(string:lessonDate))

                    }
                } else if notification_type == "3" {
                    if let rootvc = self.window?.rootViewController as? UITabBarController {
                        rootvc.selectedIndex = 1
                    }
                }
            }else if Learner.shared.userID != nil{
                if notification_type == "4", let request_id = userInfo["request_id"] as? String {
                    
                    if let rootvc = self.window?.rootViewController as? UITabBarController {
                        rootvc.dismiss(animated: false, completion: nil)
                        rootvc.selectedIndex = 1
                        let viewController:InstructorMessagesViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InstructorMessagesViewController") as! InstructorMessagesViewController
                        viewController.requestId = Int(request_id)
                        
                        (rootvc.selectedViewController as? UINavigationController)?.pushViewController(viewController, animated: true)
                    }
                    
                }  else if notification_type == "4", let request_id = userInfo["lesson_id"] as? String {
                    if let rootvc = self.window?.rootViewController as? UITabBarController {
                        rootvc.dismiss(animated: false, completion: nil)
                        rootvc.selectedIndex = 1
                        let viewController:InstructorMessagesViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InstructorMessagesViewController") as! InstructorMessagesViewController
                        viewController.requestId = Int(request_id)
                        viewController.isLesson = true
                        (rootvc.selectedViewController as? UINavigationController)?.pushViewController(viewController, animated: true)
                    }
                }  else if notification_type == "4", let _ = userInfo["lesson_date"] as? String {
                    
                    if let rootvc = self.window?.rootViewController as? UITabBarController {
                        rootvc.selectedIndex = 2
                    }
                } else if notification_type == "3" {
                    if let rootvc = self.window?.rootViewController as? UITabBarController {
                        rootvc.selectedIndex = 1
                    }
                }
            }else{
                return
            }
        }
    }
    
    func dataFromString(string: String) -> Date?{
        let formatter = DateFormatter.init()
        formatter.dateFormat = "m/d/yyyy"
        return formatter.date(from: string)
    }
}
