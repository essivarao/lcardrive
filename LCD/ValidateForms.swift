//
//  ValidateForms.swift
//  LCD
//
//  Created by Admin on 03/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import Foundation

func isValid(password:String) -> Bool {
    
    if (password.characters.count < 31 ) && (password.characters.count > 4) {
        return true
    } else {
        return false
    }
}

func isValid(firstName:String) -> Bool {
    
    if  (firstName.characters.count <= 52 ) && (firstName.characters.count > 2){
        return true
    } else {
        return false
    }
}

func isValid(address:String) -> Bool {
    
    if  (address.characters.count < 100 ){
        return true
    } else {
        return false
    }
}
func isValid(aboutMe:String) -> Bool {
    
    if  (aboutMe.characters.count < 250 ){
        return true
    } else {
        return false
    }
}
func isValid(lastName:String) -> Bool {
    
    if (lastName.characters.count <= 52 ) && (lastName.characters.count > 0) {
        return true
    } else {
        return false
    }
}
func isValid(phoneNumber:String) -> Bool {
    
    if phoneNumber.characters.count == 9 {
        return true
    } else {
        return false
    }
}

func isValid(signInPhoneNumber:String) -> Bool {
    
    if signInPhoneNumber.characters.count == 10 {
        return true
    } else {
        return false
    }
}

func isValid(fullName:String) -> Bool {
    
    if fullName.characters.count < 20 {
        return true
    } else {
        return false
    }
}

func isValid(email:String) -> Bool {
    
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: email)

}


