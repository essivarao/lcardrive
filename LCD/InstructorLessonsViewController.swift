//
//  InstructorLessonsViewController.swift
//  LCD
//
//  Created by Admin on 15/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

/*
 Pending
 Confirmed
 Rejected
 Completed
 */

class InstructorLessonsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MZDayPickerDelegate, MZDayPickerDataSource {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var navigationBar: UIView!
    @IBOutlet var headerLabel: UILabel!
    @IBOutlet weak var dayPicker: MZDayPicker!
    var selectedDate: Date?
    var dateFormatter : DateFormatter!
    
    var lessonsHistory: [InstrctorBookLessonHistory] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dayPicker.delegate = self
        self.dayPicker.dataSource = self
        
        self.dayPicker.dayNameLabelFontSize = 12.0
        self.dayPicker.dayLabelFontSize = 18.0
        
        self.dateFormatter = DateFormatter()
        self.dateFormatter.dateFormat = "EE"
        
        let date = Calendar.current.date(byAdding: .month, value: 2, to: Date())
        let startDate = Calendar.current.date(byAdding: .year, value: -2, to: Date())
        self.dayPicker.setStart(startDate, end: date)
        self.dayPicker.bottomBorderColor = UIColorFromRGB(rgbValue: Color.APP_THEMECOLOR)
        if let selectedDate = selectedDate {
            self.dayPicker.setCurrentDate(selectedDate, animated: false)
        }else{
            self.dayPicker.setCurrentDate(NSDate() as Date, animated: false)
        }
        
        
  

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        if let selectedDate = selectedDate {
            self.dayPicker.setCurrentDate(selectedDate, animated: false)
            self.selectedDate = nil
        }else{
            self.dayPicker.setCurrentDate(NSDate() as Date, animated: false)
        }
        fetchHistory(date: self.dayPicker.currentDate.iso8601)
        
        self.navigationController?.isNavigationBarHidden = true

    }
    
    func updateContent(date: Date?){
        if let selectedDate = date, self.dayPicker != nil {
            self.dayPicker.setCurrentDate(selectedDate, animated: false)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TableView
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if lessonsHistory.count == 0 {
            self.tableView.isHidden = true
        }
        else {
            self.tableView.isHidden = false
        }
    
        return lessonsHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InstructorLessonsTableViewCell") as? InstructorLessonsTableViewCell
        cell?.lesson = lessonsHistory[indexPath.row]
        cell?.populateData()
        
        setupActions(cell:cell!, index: indexPath.row)
        
        return cell!
    }
    
    func setupActions(cell: InstructorLessonsTableViewCell, index: Int){
        cell.confirmButtonView.isHidden = true
        cell.updateButtonView.isHidden = true
        cell.updateButton.isHidden = true
        cell.completeButton.isHidden = true
        cell.rejectedLabel.isHidden = true
        cell.rejectButton.isHidden = true
        cell.confirmButton.isHidden = true
        
        let lesson = lessonsHistory[index]
        
        if lesson.isInstructor! == true{
            switch lesson.status!{
            case "Pending":
                cell.updateButtonView.isHidden = false
                cell.updateButton.isHidden = false
                break
            case "Confirmed":
                cell.updateButtonView.isHidden = false
                cell.completeButton.isHidden = false
                cell.updateButton.isHidden = false
                break
            case "Rejected":
                cell.rejectedLabel.isHidden = false
                cell.rejectedLabel.text = "Rejected"
                break
            case "Completed":
                cell.rejectedLabel.isHidden = false
                cell.rejectedLabel.text = "Completed"
                break
            default:
                break
            }
        }else{
            switch lesson.status!{
            case "Pending":
                cell.confirmButtonView.isHidden = false
                cell.confirmButton.isHidden = false
                cell.rejectButton.isHidden = false
                break
            case "Confirmed":
                cell.updateButtonView.isHidden = false
                cell.completeButton.isHidden = false
                cell.updateButton.isHidden = false
                break
            case "Rejected":
                cell.rejectedLabel.isHidden = false
                cell.rejectedLabel.text = "Rejected"
                break
            case "Completed":
                cell.rejectedLabel.isHidden = false
                cell.rejectedLabel.text = "Completed"
                break
            default:
                break
            }
        }
        
//        if lesson.status == "Pending" {
//            cell.confirmButtonView.isHidden = false
//        }else if lesson.status == "Confirmed"{
//            cell.updateButtonView.isHidden = false
//            cell.completeButton.isHidden = false
//        }else{
//            
//        }

        cell.viewMessageButton.addTarget(self, action: #selector(viewMessagesClicked(sender:)), for: .touchUpInside)
        cell.viewMessageButton.tag = index
        
        cell.sendmessageButton.addTarget(self, action: #selector(sendMessageButtonClicked(sender:)), for: .touchUpInside)
        cell.sendmessageButton.tag = index
        
        cell.callButton.addTarget(self, action: #selector(callButtonClicked(sender:)), for: .touchUpInside)
        cell.callButton.tag = index

        
        cell.updateButton.addTarget(self, action: #selector(updateLesson(sender:)), for: .touchUpInside)
        cell.completeButton.addTarget(self, action: #selector(completeLesson(sender:)), for: .touchUpInside)
        cell.rejectButton.addTarget(self, action: #selector(rejectLesson(sender:)), for: .touchUpInside)
        cell.confirmButton.addTarget(self, action: #selector(confirmLesson(sender:)), for: .touchUpInside)
        cell.updateButton.tag = index
        cell.rejectButton.tag = index
        cell.confirmButton.tag = index
        cell.completeButton.tag = index

    }
    
    func updateLesson(sender: UIButton){
        let lesson = lessonsHistory[sender.tag]
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "InstructorBookLess") as! InstructorBookLessViewController
        let request = BookLessonRequest()
        request.imageURL = lesson.learnerProfilePic
        request.InstructorId = lesson.instructorId
        request.LearnerId = lesson.learnerId
        request.UserId = Learner.shared.userID
        request.instructorName = lesson.learnerName
        request.Latitude = lesson.latitude
        request.Longitude = lesson.longitude
        request.Suburb = lesson.suburb
        request.PackageId = lesson.package?.packageId
        request.PostalCode = "\(lesson.postalCode ?? 0)"
        request.LessonTypeId = lesson.lessonTypeId
        request.LessonId = lesson.lessonId
        request.rate = lesson.rate
        request.RateModeTypeId = lesson.rateModeTypeId
        request.selectedDate = lesson.lessonDate?.dateFromISO8601
        request.hour = lesson.package?.hour
        request.packageType = lesson.package?.packageTypeId
        vc.request = request
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    private func callNumber(phoneNumber:String) {

        let phoneNum = "+61\(phoneNumber)"
        if let url = URL(string: "telprompt://\(phoneNum)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
        else {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window?.makeToast("Phone number not valid.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
        }

    
    }
    
    func callButtonClicked (sender : UIButton) {
        let request = lessonsHistory[sender.tag]
        
        callNumber(phoneNumber: request.learnerPhoneNumber ?? "0")
        
        
    }
    func viewMessagesClicked(sender : UIButton)  {
        let request = lessonsHistory[sender.tag]
        
        let viewController:InstructorMessagesViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InstructorMessagesViewController") as! InstructorMessagesViewController
        viewController.requestId = request.lessonId
        viewController.isLesson = true
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    func sendMessageButtonClicked(sender : UIButton) {
        let request = lessonsHistory[sender.tag]
        let viewController:EnterMessageViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "enterMessageViewController") as! EnterMessageViewController
        viewController.learnerId = request.learnerId
        viewController.lessonId = request.lessonId
        viewController.instructorId = request.instructorId
        viewController.isLesson = true
        
        //        viewController.instructorId = request.instructorId
        
        
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: false, completion: nil)
    }
    

    
    func completeLesson(sender: UIButton){
        let lesson = lessonsHistory[sender.tag]
        
        let payload = [
            "Lessonid": lesson.lessonId ?? 0,
            "UserId": Instructor.shared.userID ?? 0,
            "Status": "Completed",
            "comments": "sample string 4"
            ] as [String : Any]
        showProgress()
        NetworkInterface.fetchJSON(.updateLessonStatus, payload: payload) { (success, data, response, error) -> (Void) in
            if data?["StatusCode"] as? Int  == 0 {
                lesson.status = "Completed"
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            self.hideProgress()
        }
    }
    
    func rejectLesson(sender: UIButton){
        
        let lesson = lessonsHistory[sender.tag]
        sendRejectMessageButtonClicked(sender: sender)
    }
    
    func sendRejectMessageButtonClicked(sender : UIButton) {
        let request = lessonsHistory[sender.tag]
        let viewController:RejectedReasonViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "rejectedReasonViewController") as! RejectedReasonViewController
        viewController.lesson = request
//        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: false, completion: nil)
        
    }
    
    func confirmLesson(sender: UIButton){
        let lesson = lessonsHistory[sender.tag]
        
        let payload = [
            "Lessonid": lesson.lessonId ?? 0,
            "UserId": Instructor.shared.userID ?? 0,
            "Status": "Confirmed",
            "comments": "sample string 4"
        ] as [String : Any]
        showProgress()
        NetworkInterface.fetchJSON(.updateLessonStatus, payload: payload) { (success, data, response, error) -> (Void) in
            if data?["StatusCode"] as? Int  == 0 {
                lesson.status = "Confirmed"
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            self.hideProgress()
        }

    }
    
    func dayPicker(_ dayPicker: MZDayPicker, titleForCellDayNameLabelIn day: MZDay) -> String {
        return self.dateFormatter.string(from: day.date)
    }
    
    func dayPicker(_ dayPicker: MZDayPicker, didSelect day: MZDay) {
        print("Did select day \(day.date)")
        fetchHistory(date: day.date.iso8601)
        self.tableView.reloadData()
    }
    
    func dayPicker(_ dayPicker: MZDayPicker, willSelect day: MZDay) {
        print("Will select day \(day.date)")
    }

}

extension InstructorLessonsViewController{
    func fetchHistory(date: String){
        
        lessonsHistory.removeAll()
        tableView.reloadData()
        let payload = [
            "InstructorId": Instructor.shared.userID ?? 0,
            "LessonDate": date
            ] as [String : Any]
        self.showProgress()
        NetworkInterface.fetchJSON(.instructorBookingHistory, payload: payload) { (success, data, response, error) -> (Void) in
            if let data = data as? [String:Any], let servicecStatus = data["ServiceStatus"] as? [String:Any], servicecStatus["StatusCode"] as? Int == 0, let lessonList = data["LessonList"] as? [[String:Any]] {
                
                print(data.JSONStringify())

                lessonList.forEach({ (lessonDict) in
                    let lesson = InstrctorBookLessonHistory.init(dictionary: lessonDict as NSDictionary)
                    self.lessonsHistory.append(lesson!)
                
                })
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.hideProgress()
            }
        }
    }
}
