//
//  CustomImagePickerViewController.swift
//  DrivingAUS
//
//  Created by Narendra Kumar R on 3/7/17.
//  Copyright © 2017 Sarella. All rights reserved.
//

import UIKit

enum ImagePicFrom{
    case carImaes
    case profileImage
}


class CustomImagePickerViewController: UIImagePickerController {
    
    var userInfo: Any?
    var from: ImagePicFrom?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
