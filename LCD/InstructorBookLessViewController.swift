//
//  InstructorBookLessViewController.swift
//  LCD
//
//  Created by Narendra Kumar on 3/11/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit



class BookLessonRequest{
    var LessonId: Int?
    var LearnerId: Int?
    var InstructorId: Int?
    var UserId: Int?
    var PackageId: Int?
    var Suburb: String?
    var PostalCode: String?
    var Latitude: String?
    var Longitude: String?
    var LessonTypeId: Int?
    var LessonDate: String?
    var Message: String?
    var instructorName: String?
    var selectedDate: Date?
    var RateModeTypeId: Int?
    var rate: Int?
    var imageURL: String?
    var hour:Int?
    var packageType: Int?
}


class InstructorBookLessViewController: UIViewController , UITableViewDataSource, UITableViewDelegate, RequestMapDelegate, UITextFieldDelegate {
    
    @IBOutlet var profilePicture: UIImageView!
    @IBOutlet var fullNameLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var sendSupburbTextField: UITextField!
    @IBOutlet var messageTextView: UITextView!
    @IBOutlet var tableViewHeightConstraints: NSLayoutConstraint!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var sendButton: UIButton!
    @IBOutlet var selectPackageTextField: UITextField!
    @IBOutlet var selectDateAndTimeTextField: UITextField!
    @IBOutlet var navigationBar: UIView!
    @IBOutlet var headerLabel: UILabel!
    @IBOutlet var lookingForLabel: UILabel!
    var details:[LernerLeaanType] = []
    var topBar = UIView()
    var request: BookLessonRequest?
    var selectedUser: SearchedInstructor?
    var packagesArray: [InstructorPackages] = []
    
    let datePicker: UIDatePicker = UIDatePicker()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSetUp()
        datePicker.minimumDate = NSDate() as Date
        
        if let message = AppDelegate.shared.configValues.filter({$0.configKey == "LessonDefaultMsg"}).first?.configValue {
            self.messageTextView.text = message
        }
        
        let masters = AppDelegate.shared.masterDetails.filter({$0.masterType == "LessonType"})
        
        masters.forEach { (master) in
            let lernerTYpe = LernerLeaanType.init()
            lernerTYpe.master = master
            lernerTYpe.selected = false
            self.details.append(lernerTYpe)
        }
        
        topBar.isHidden = true
        
        
        messageTextView.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        messageTextView.layer.borderColor = UIColor.lightGray.cgColor
        messageTextView.layer.borderWidth = 2
        profilePicture.layer.cornerRadius = 25
        profilePicture.layer.masksToBounds = true
        sendSupburbTextField.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        sendSupburbTextField.layer.borderColor = UIColor.lightGray.cgColor
        sendSupburbTextField.layer.borderWidth = 2
        sendSupburbTextField.delegate = self
        
        
        if let imageURL = request?.imageURL,
            let url = URL(string: imageURL) {
            //            profileImageView.kf.indicatorType = .activity
            profilePicture.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultFace"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        fullNameLabel.text = request?.instructorName ?? ""
        
        
        if request?.LessonId != nil {
            sendSupburbTextField.text = request?.Suburb ?? ""
            if request?.packageType == 16 {
                selectPackageTextField.text = "Driving test package - \(request?.rate ?? 0)/Hour"

            }else{
                selectPackageTextField.text = "\(request?.rate ?? 0)/Hour"

            }
            self.details.filter({$0.master?.masterDetailId == request?.LessonTypeId}).first?.selected = true
            tableView.reloadData()
            
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
            let selectedDate: String = dateFormatter.string(from: request?.selectedDate ?? Date())
            self.selectDateAndTimeTextField.text = selectedDate
            
            if ((request?.LessonId)! > 0) {
                sendButton.setTitle("UPDATE", for: .normal)
            }
        }
        else{
            sendButton.setTitle("SEND", for: .normal)
            
        }
        
        if let imageURL = selectedUser?.profilePicImageUrl {
            let url = URL(string: imageURL)
            profilePicture.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultFace"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        tableViewHeightConstraints.constant = CGFloat((self.details.count ) * 50)
        
        let dict = ["userId":"\(request?.InstructorId ?? 0)"]
        
        NetworkInterface.fetchJSON(.instructorPackages, params: dict as NSDictionary?) { (success, data, response, error) -> (Void) in
            if let packages = data?["InstructorPackage"] as? NSArray {
                if let hourpackage = InstructorPackages.modelsFromDictionaryArray(array: packages).filter({$0.hour == 1}).first {
                    self.packagesArray.append(hourpackage)
                }
                let packgDescription = InstructorPackages.modelsFromDictionaryArray(array: packages).filter({$0.packageTypeId == 16})
                if packgDescription.count > 0 {
                    self.packagesArray.append(contentsOf: packgDescription)
                }
            }
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.resignFirstResponder()
        
        if (textField == sendSupburbTextField) {
            let storyBoard = UIStoryboard.init(name: "Learner", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "SelectPlaceForRequestView") as! SelectPlaceForRequestViewController
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else  if (textField == selectPackageTextField ){
            self.selectPackage()
        }
        else{
            self.selectDateAndTime()
            
        }
        
    }
    func selectDateAndTime() {
        
        topBar.isHidden = false
        
        topBar.frame = CGRect(x: 0, y: self.view.frame.size.height-250, width: self.view.frame.width, height: 50)
        topBar.backgroundColor = UIColorFromRGB(rgbValue: 0x19BC9D)
        self.view.addSubview(topBar)
        
        let cancelButton:UIButton = UIButton(frame: CGRect(x: 30, y: 5 , width: 100, height: 40))
        cancelButton.setTitle("CANCEL", for: .normal)
        cancelButton.titleLabel?.font = UIFont (name: "Montserrat-Light", size: 14)
        cancelButton.addTarget(self, action:#selector(self.cancelButtonClicked), for: .touchUpInside)
        topBar.addSubview(cancelButton)
        
        
        let saveButton:UIButton = UIButton(frame: CGRect(x: self.view.frame.size.width-120, y: 5 , width: 100, height: 40))
        saveButton.setTitle("SAVE", for: .normal)
        saveButton.titleLabel?.font = UIFont (name: "Montserrat-Light", size: 14)
        saveButton.addTarget(self, action:#selector(self.saveButtonClicked), for: .touchUpInside)
        topBar.addSubview(saveButton)
        
        
        // Posiiton date picket within a view
        datePicker.frame = CGRect(x: 0, y: self.view.frame.size.height-200 , width: self.view.frame.width, height: 200)
        // Set some of UIDatePicker properties
        //        datePicker.timeZone = NSTimeZone.local
        datePicker.datePickerMode = UIDatePickerMode.dateAndTime
        datePicker.backgroundColor = UIColor.white
        // Add an event to call onDidChangeDate function when value is changed.
        datePicker.addTarget(self, action: #selector(InstructorBookLessViewController.datePickerValueChanged(_:)), for: .valueChanged)
        // Add DataPicker to the view
        self.view.addSubview(datePicker)
        
    }
    func cancelButtonClicked() {
        topBar.isHidden = true
        datePicker.removeFromSuperview()
        
    }
    func saveButtonClicked()  {
        topBar.isHidden = true
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        // Set date format
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: datePicker.date)
        print("Selected value \(selectedDate)")
        request?.selectedDate = datePicker.date
        selectDateAndTimeTextField.text = selectedDate
        datePicker.removeFromSuperview()
        
    }
    
    func datePickerValueChanged(_ sender: UIDatePicker){
        //        // Create date formatter
        //        let dateFormatter: DateFormatter = DateFormatter()
        //        // Set date format
        //        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        //        // Apply date format
        //        let selectedDate: String = dateFormatter.string(from: sender.date)
        //        print("Selected value \(selectedDate)")
    }
    
    func selectPackage()  {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if let package = self.packagesArray.filter({$0.hour == 1}).first {
            let rate = "\(package.rate ?? 0)/Hour"
            let pkg1 = UIAlertAction(title: rate, style: .default, handler: { (action) -> Void in
                self.selectPackageTextField.text = rate
                self.request?.PackageId = package.packageId
                self.request?.rate = package.rate
                self.request?.RateModeTypeId = package.rateModeId
            })
            alertController.addAction(pkg1)
        }
        
        let packages = self.packagesArray.filter({$0.packageTypeId == 16})
        packages.forEach { (package) in
            var rate = ""
            if package.hour == 1  || package.hour == 0 {
                rate = "Driving test package - \(package.rate ?? 0) /Hour"

            }else{
                rate = "Driving test package - \(package.rate ?? 0) / \(package.hour ?? 0)Hours"

            }
            let  pkg2 = UIAlertAction(title: rate, style: .default, handler: { (action) -> Void in
                self.selectPackageTextField.text = rate
                self.request?.PackageId = package.packageId
                self.request?.rate = package.rate
                self.request?.RateModeTypeId = package.rateModeId
            })
            alertController.addAction(pkg2)
        }
        
        
        
        let cancelButton = UIAlertAction(title: "CANCEL", style: .cancel, handler: { (action) -> Void in
            
        })
        
        alertController.addAction(cancelButton)
        
        self.navigationController!.present(alertController, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TableView
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.details.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InstructorBookLessonTableViewCell") as? InstructorBookLessonTableViewCell
        
        cell?.fullTitleLabel.text = self.details[indexPath.row].master?.value ?? ""
        
        if (self.details[indexPath.row].selected == true) {
            cell?.selectedImageView.image = #imageLiteral(resourceName: "ic_radio_btn_select")
        }else{
            cell?.selectedImageView.image = #imageLiteral(resourceName: "ic_radio_btn_unselect")
        }
        
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for detail in details {
            detail.selected = false
        }
        self.details[indexPath.row].selected = true
        
        request?.LessonTypeId = self.details[indexPath.row].master?.masterDetailId
        tableView.reloadData()
    }
    
    
    @IBAction func backButtonClicked(_ sender: Any) {
        SearchUserDetailWithOutSignInViewController.selectedUserInMemory = nil
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendButtonClicked(_ sender: Any) {
        if selectPackageTextField.text?.characters.count == 0 {
            AppDelegate.shared.window?.makeToast("Please select any package", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }else if request?.selectedDate == nil {
            AppDelegate.shared.window?.makeToast("Please select any date time", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }else if self.request?.Suburb == nil {
            AppDelegate.shared.window?.makeToast("Please select any pickup location", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }else if self.details.filter({$0.selected == true}).count == 0 {
            AppDelegate.shared.window?.makeToast("Please select any learn type", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        
        /*
         {
         "Rate": "30.0",
         "RateModeTypeId": "10",
         "UserId": "56",
         "LessonDate": "19-3-2017 13:59",
         "LessonTypeId": "7",
         "Latitude": "17.338369669074094",
         "Longitude": "78.38835541158916",
         "Suburb": "Unnamed RoadAbhyudaya Nagar, Saibaba Nagar",
         "PostalCode": "500030",
         "Message": "hello message"
         }
         */
        
        var payload:[String:Any] = [:]
        if request?.LessonId != nil {
            payload["Lessonid"] = request?.LessonId ?? 0
            
        }
        payload["LearnerId"] = request?.LearnerId ?? 0
        payload["InstructorId"] = request?.InstructorId ?? 0
        payload["UserId"] = request?.UserId ?? 0
        payload["PackageId"] = request?.PackageId ?? 0
        payload["Suburb"] = request?.Suburb ?? 0
        payload["PostalCode"] = request?.PostalCode ?? 0
        payload["Latitude"] = "\((round(Double(request?.Latitude ?? "0")!*100)/100))"
        payload["Longitude"] = "\((round(Double(request?.Longitude ?? "0")!*100)/100))"
        payload["LessonTypeId"] = request?.LessonTypeId ?? 0
        payload["LessonDate"] = request?.selectedDate?.iso8601 ?? ""
        payload["Message"] = messageTextView.text ?? ""
        payload["Rate"] = request?.rate ?? 0
        payload["RateModeTypeId"] = request?.RateModeTypeId ?? 0
        payload["isActive"] = true
        payload["MessageDate"] = Date().iso8601

        
        showProgress()
        
        NetworkInterface.fetchJSON(.bookLesson, payload: payload) { (success, data, response, error) -> (Void) in
            DispatchQueue.main.async {
                self.hideProgress()
                if let data = data as? [String:Any], data["StatusCode"] as? Int == 0,let message = data["ServiceMessage"] as? String {
                    AppDelegate.shared.window?.makeToast(message, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    SearchUserDetailWithOutSignInViewController.selectedUserInMemory = nil
                    _ = self.navigationController?.popToRootViewController(animated: false)
                    let tabBarController = AppDelegate.shared.window?.rootViewController as! UITabBarController
                    tabBarController.selectedIndex = 2
                    
                }else if let data = data as? [String:Any], let message = data["ErrorMessage"] as? String{
                    AppDelegate.shared.window?.makeToast(message, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    
                }else{
                    AppDelegate.shared.window?.makeToast("Error", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    print(data ?? "No data")
                }
            }
        }
        
    }
    
    @IBOutlet var enterMessageLabel: UILabel!
    
    
    func didSelectLocation(location: SelectedSendRequestLocation) {
        self.request?.Suburb = location.locality
        self.request?.Latitude = location.lat
        self.request?.Longitude = location.long
        self.request?.PostalCode = location.pin
        self.sendSupburbTextField.text = location.locality ?? ""
    }
    
}
extension InstructorBookLessViewController{
    func viewSetUp() {
        navigationBar.backgroundColor = UIColorFromRGB(rgbValue: Color.APP_THEMECOLOR)
        headerLabel.font = UIFont (name:FontName.REGULAR , size: CGFloat(FontSize.TEXTFILED_SMALL))
        fullNameLabel.font = UIFont (name:FontName.REGULAR , size: CGFloat(FontSize.TEXTFILED_LARGE))
        lookingForLabel.font = UIFont (name:FontName.REGULAR , size: CGFloat(FontSize.TEXTFILED_MEDIUM))
        enterMessageLabel.font = UIFont (name:FontName.REGULAR , size: CGFloat(FontSize.TEXTFILED_MEDIUM))
        cancelButton.titleLabel?.font = UIFont(name: FontName.LIGHT, size:CGFloat(FontSize.BUTTON_MEDIUM) )
        sendButton.titleLabel?.font = UIFont(name: FontName.LIGHT, size:CGFloat(FontSize.BUTTON_MEDIUM) )
        cancelButton.backgroundColor = UIColorFromRGB(rgbValue: Color.GRAY_COLOR)
        sendButton.backgroundColor = UIColorFromRGB(rgbValue: Color.APP_THEMECOLOR)
        
        
    }
}
