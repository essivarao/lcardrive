//
//  LCDInstructorSearchTableViewCell.swift
//  LCD
//
//  Created by Admin on 01/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class LCDInstructorSearchTableViewCell: UITableViewCell {
    
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var nameTextField: UILabel!
    @IBOutlet var maleExpLabel: UILabel!
    @IBOutlet var drivingSchoolLabel: UILabel!
    @IBOutlet var modeLabel: UILabel!
    @IBOutlet var ratingLabel: UILabel!
    @IBOutlet var noOfRatingsLabel: UILabel!
    @IBOutlet var ratePerHourLabel: UILabel!
    @IBOutlet var callButton: UIButton!
    @IBOutlet var carTypeLabel: UILabel!
    @IBOutlet var priceOneLabel: UILabel!
    @IBOutlet var pricetwoLabel: UILabel!
    @IBOutlet var priceThreeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.profileImageView.layer.cornerRadius = 35
        self.profileImageView.layer.masksToBounds = true
        
        let imageView = UIImageView()
        imageView.frame = CGRect(x: 10 , y: 7, width: 18, height: 18)
        imageView.image = #imageLiteral(resourceName: "call_plane")
        callButton.addSubview(imageView)
        callButton.titleEdgeInsets = UIEdgeInsetsMake(0, 20 , 0, 0)
        
        nameTextField.font = UIFont (name: FontName.REGULAR , size: CGFloat(FontSize.LABEL_SMALL) )
        maleExpLabel.font = UIFont (name: FontName.LIGHT , size: CGFloat(FontSize.LABEL_SMALL) )
        drivingSchoolLabel.font = UIFont (name: FontName.LIGHT , size: CGFloat(FontSize.LABEL_SMALL) )
        modeLabel.font = UIFont (name: FontName.LIGHT , size: CGFloat(FontSize.LABEL_SMALL) )
        carTypeLabel.font = UIFont (name: FontName.LIGHT , size: CGFloat(FontSize.LABEL_SMALL) )
        ratingLabel.font = UIFont (name: FontName.LIGHT , size: CGFloat(FontSize.LABEL_SMALL) )
        noOfRatingsLabel.font = UIFont (name: FontName.LIGHT , size: CGFloat(FontSize.LABEL_SMALL) )
        
        priceOneLabel.font = UIFont (name: FontName.LIGHT , size: CGFloat(FontSize.LABEL_SMALL) )
        pricetwoLabel.font = UIFont (name: FontName.LIGHT , size: CGFloat(FontSize.LABEL_SMALL) )
        priceThreeLabel.font = UIFont (name: FontName.LIGHT , size: CGFloat(FontSize.LABEL_SMALL) )
        
        ratePerHourLabel.font = UIFont (name: FontName.REGULAR , size: CGFloat(FontSize.LABEL_LARGE) )
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func callButtonClicked(_ sender: Any) {
    }
}
