//
//  ImageCollectionViewCell.swift
//  LCD
//
//  Created by kvanaworld on 3/23/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    let schoolImageView: UIImageView = {
//        let imageView = UIImageView()
        let imageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(schoolImageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
