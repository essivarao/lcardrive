//
//  ProfileTestPackageTableViewCell.swift
//  LCD
//
//  Created by Sashank on 01/04/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class ProfileTestPackageTableViewCell: UITableViewCell {


    @IBOutlet weak var priceTextLabel: UILabel!
    @IBOutlet weak var descriptionTextLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
