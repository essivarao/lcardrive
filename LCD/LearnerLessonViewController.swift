//
//  LearnerLessonViewController.swift
//  LCD
//
//  Created by Admin on 16/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class LearnerLessonViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var navigationBar: UIView!
    @IBOutlet var headerLabel: UILabel!
    
    var totalResults:[LessonList] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSetUp()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchRequests()
    }
    
    //TableView
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if totalResults.count == 0  {
            self.tableView.isHidden = true
        }
        else {
            self.tableView.isHidden = false
        }
        
        return totalResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LearnerLessonTableViewCell") as? LearnerLessonTableViewCell
        let lesson =  totalResults[indexPath.row]
        cell?.lesson = lesson
       setupActions(cell:cell!, index: indexPath.row)
        return cell!
    }
    
    func setupActions(cell: LearnerLessonTableViewCell, index: Int){
        
        let lesson = totalResults[index]

        cell.viewMessageButton.addTarget(self, action: #selector(viewMessagesClicked(sender:)), for: .touchUpInside)
        cell.viewMessageButton.tag = index
        
        cell.sendmessageButton.addTarget(self, action: #selector(sendMessageButtonClicked(sender:)), for: .touchUpInside)
        cell.sendmessageButton.tag = index
        
        cell.callButton.addTarget(self, action: #selector(callButtonClicked(sender:)), for: .touchUpInside)
        cell.callButton.tag = index
        
        
        
        cell.updateButton.isHidden = true
        cell.comfirmed.isHidden = true
        cell.updateButtonView.isHidden = true
        cell.confirmButton.isHidden = true
        cell.rejectButton.isHidden = true
        cell.confirmButtonView.isHidden = true
        cell.rejectedLabel.isHidden = true

        cell.updateButton.removeTarget(nil, action: nil, for: .allEvents)
//        cell.comfirmed.removeTarget(nil, action: nil, for: .allEvents)
        cell.rejectButton.removeTarget(nil, action: nil, for: .allEvents)
        cell.confirmButton.removeTarget(nil, action: nil, for: .allEvents)

        cell.updateButton.addTarget(self, action: #selector(updateLesson(sender:)), for: .touchUpInside)
//        cell.comfirmed.addTarget(self, action: #selector(completeLesson(sender:)), for: .touchUpInside)
        cell.rejectButton.addTarget(self, action: #selector(rejectLesson(sender:)), for: .touchUpInside)
        cell.confirmButton.addTarget(self, action: #selector(confirmLesson(sender:)), for: .touchUpInside)
        cell.updateButton.tag = index
        cell.rejectButton.tag = index
        cell.confirmButton.tag = index
//        cell.comfirmed.tag = index
        
        if !(lesson.isInstructor!){
            switch lesson.status!{
            case "Pending":
                cell.updateButtonView.isHidden = false
                cell.updateButton.isHidden = false
                cell.comfirmed.isHidden = false
                cell.comfirmed.titleLabel?.text = ""
                break
            case "Confirmed":
                cell.updateButtonView.isHidden = false
                cell.comfirmed.isHidden = false
                cell.comfirmed.titleLabel?.text = "Confirmed"
                cell.updateButton.isHidden = false
                break
            case "Rejected":
                cell.rejectedLabel.isHidden = false
                cell.rejectedLabel.text = "Rejected"
                break
            case "Completed":
                cell.rejectedLabel.isHidden = false
                cell.rejectedLabel.text = "Completed"
                break
            default:
                break
            }
        }else{
            switch lesson.status!{
            case "Pending":
                cell.confirmButtonView.isHidden = false
                cell.confirmButton.isHidden = false
                cell.rejectButton.isHidden = false
                break
            case "Confirmed":
                cell.updateButtonView.isHidden = false
                cell.comfirmed.isHidden = false
                cell.updateButton.isHidden = false
                break
            case "Rejected":
                cell.rejectedLabel.isHidden = false
                cell.rejectedLabel.text = "Rejected"
                break
            case "Completed":
                cell.rejectedLabel.isHidden = false
                cell.rejectedLabel.text = "Completed"
                break
            default:
                break
            }
        }

        
        

        
        

        
    }

    func updateLesson(sender: UIButton){
        let lesson = totalResults[sender.tag]
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "InstructorBookLess") as! InstructorBookLessViewController
        let request = BookLessonRequest()
        request.imageURL = lesson.instructorProfilePic
        request.InstructorId = lesson.instructorId
        request.LearnerId = lesson.learnerId
        request.UserId = Learner.shared.userID
        request.instructorName = lesson.instructorName
        request.Latitude = lesson.latitude
        request.Longitude = lesson.longitude
        request.Suburb = lesson.suburb
        request.PackageId = lesson.package?.packageId
        request.PostalCode = lesson.postalCode
        request.LessonTypeId = lesson.lessonTypeId
        request.LessonId = lesson.lessonId
        request.rate = lesson.rate
        request.RateModeTypeId = lesson.rateModeTypeId
        request.selectedDate = lesson.lessonDate?.dateFromISO8601
        vc.request = request
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func completeLesson(sender: UIButton){
        let lesson = totalResults[sender.tag]
        
        let payload = [
            "Lessonid": lesson.lessonId ?? 0,
            "UserId": Learner.shared.userID ?? 0,
            "Status": "Completed",
            "comments": "sample string 4"
            ] as [String : Any]
        showProgress()
        NetworkInterface.fetchJSON(.updateLessonStatus, payload: payload) { (success, data, response, error) -> (Void) in
            if data?["StatusCode"] as? Int  == 0 {
                lesson.status = "Completed"
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            self.hideProgress()
        }
    }
    
    func rejectLesson(sender: UIButton){
        let lesson = totalResults[sender.tag]
         if !(lesson.isInstructor)! {
            updateLesson(sender: sender)
         }else {
            
            sendRejectMessageButtonClicked(sender: sender)
        
        }
    }
    
    func sendRejectMessageButtonClicked(sender : UIButton) {
        let request = totalResults[sender.tag]
        let viewController:EnterMessageViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "enterMessageViewController") as! EnterMessageViewController
        viewController.learnerId = request.learnerId
        viewController.lessonId = request.lessonId
        viewController.instructorId = request.instructorId
        viewController.isLesson = false
        viewController.isReject = true
        viewController.lesson = request
        viewController.tableView = self.tableView
        
        //        viewController.instructorId = request.instructorId
        
        
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: false, completion: nil)
        
    }
    
    func confirmLesson(sender: UIButton){
        let lesson = totalResults[sender.tag]
        
        let payload = [
            "Lessonid": lesson.lessonId ?? 0,
            "UserId": Learner.shared.userID ?? 0,
            "Status": "Confirmed",
            "comments": "sample string 4"
            ] as [String : Any]
        showProgress()
        NetworkInterface.fetchJSON(.updateLessonStatus, payload: payload) { (success, data, response, error) -> (Void) in
            if data?["StatusCode"] as? Int  == 0 {
                lesson.status = "Confirmed"
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            self.hideProgress()
        }
        
    }
    
    private func callNumber(phoneNumber:String) {
        
//        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
//            
//            let application:UIApplication = UIApplication.shared
//            if (application.canOpenURL(phoneCallURL)) {
//                if #available(iOS 10.0, *) {
//                    application.open(phoneCallURL, options: [:], completionHandler: nil)
//                } else {
//                    // Fallback on earlier versions
//                    let url:NSURL = NSURL(string: phoneNumber)!
//                    UIApplication.shared.openURL(url as URL)
//                }
//            }
//        }

    
        let phoneNum = "+61\(phoneNumber)"
        if let url = URL(string: "telprompt://\(phoneNum)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
        else {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window?.makeToast("Phone number not valid.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
        }
    }
    
    func callButtonClicked (sender : UIButton) {
        let request = totalResults[sender.tag]
        
        callNumber(phoneNumber: request.instructorPhoneNumber!)
        
        
    }
    func viewMessagesClicked(sender : UIButton)  {
        let request = totalResults[sender.tag]
        
        let viewController:InstructorMessagesViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InstructorMessagesViewController") as! InstructorMessagesViewController
        viewController.requestId = request.lessonId
        viewController.isLesson = true
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    func sendMessageButtonClicked(sender : UIButton) {
        let request = totalResults[sender.tag]
        let viewController:EnterMessageViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "enterMessageViewController") as! EnterMessageViewController
        viewController.learnerId = request.learnerId
        viewController.lessonId = request.lessonId
        viewController.instructorId = request.instructorId
        viewController.isLesson = true
        
        //        viewController.instructorId = request.instructorId
       
        
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: false, completion: nil)
    }
    
    
    
}

extension LearnerLessonViewController{
    func viewSetUp ()  {
        navigationBar.backgroundColor = UIColorFromRGB(rgbValue: Color.APP_THEMECOLOR)
        headerLabel.font = UIFont (name:FontName.REGULAR , size: CGFloat(FontSize.TEXTFILED_SMALL))
    }
    
    func fetchRequests(){
        showProgress()
         let params:Dictionary = ["learnerid": "\(Learner.shared.userID!)"]
        
        NetworkInterface.fetchJSON(.learnerLessons, headers: [:], params: [:], payload: params) { (success, data, response, error) -> (Void) in
            if let data = data as? [String:Any], let requstList = data["LessonList"] as? [[String:Any]]{
                self.totalResults.removeAll()
                
                requstList.forEach({ (dict) in
                    let obj = LessonList.init(dictionary: dict as NSDictionary)
                    self.totalResults.append(obj!)
                   
                    
                })
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            }
            DispatchQueue.main.async {
                self.hideProgress()
            }
        }
    }
}
