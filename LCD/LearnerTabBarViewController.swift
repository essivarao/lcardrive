//
//  LearnerTabBarViewController.swift
//  LCD
//
//  Created by kvanaworld on 4/2/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class LearnerTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 10.0, *) {
            self.tabBar.unselectedItemTintColor = UIColor.white
        } else {
            
            let arrayOfImageNameForSelectedState = ["Search","Requests","lessons","profileTab"]
            let arrayOfImageNameForUnselectedState = ["Search","Requests","lessons","profileTab"]
            if let count = self.tabBar.items?.count {
                for i in 0...(count-1) {
                    let imageNameForSelectedState   = arrayOfImageNameForSelectedState[i]
                    let imageNameForUnselectedState = arrayOfImageNameForUnselectedState[i]
                    
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                    self.tabBar.items?[i].image = UIImage(named: imageNameForUnselectedState)?.withRenderingMode(.alwaysOriginal)
                }
            }
        }
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for:.normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.black], for:.selected)
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
