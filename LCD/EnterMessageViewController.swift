//
//  EnterMessageViewController.swift
//  LCD
//
//  Created by Admin on 02/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


class EnterMessageViewController: UIViewController {
    
    @IBOutlet var messageTextView: UITextView!
    var learnerId: Int?
    var lessonId: Int?
    var instructorId:Int?
    var requestID:Int?
    var isLesson: Bool = false
    var isReject: Bool = false
    var lesson: LessonList?
    var tableView: UITableView?
    
     override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.clear
        view.isOpaque = true
        
        
        messageTextView.becomeFirstResponder()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func cancelClicked(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
        
    }
    @IBAction func sendButtonClicked(_ sender: Any) {
        
        if isReject {
          
            if (messageTextView.text.characters.count == 0){
            AppDelegate.shared.window?.makeToast("Please provide reason to reject ", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                return
            }
            sendRejectMessage(lesson:lesson!)
            return
        }
        let payload = [
            "RequestId": requestID ?? "",
            "LearnerId": learnerId ?? "",
            "InstructorId": instructorId ?? "",
            "LessonId":lessonId ?? "",
            "Message": messageTextView.text ?? "",
            "IsRead": true,
            "MessageDate": Date().iso8601
            ] as [String:Any]
        showProgress()
        print(payload.JSONStringify())
        NetworkInterface.fetchJSON(isLesson ? .learnerSendMessage : .sendMessage, payload: payload) { (success, data, response, error) -> (Void) in
            if data as? [String:Any] != nil{
                DispatchQueue.main.async{
                    self.dismiss(animated: false, completion: nil)
                    AppDelegate.shared.window?.makeToast("Message sent successfully", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                }
            }else{
                DispatchQueue.main.async{
                    AppDelegate.shared.window?.makeToast("Error in sending message", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                }
            }
            self.hideProgress()
        }
    }
    
    
    
    func sendRejectMessage(lesson: LessonList){
        let payload = [
            "Lessonid": lesson.lessonId ?? 0,
            "UserId": Learner.shared.userID ?? 0,
            "Status": "Rejected",
            "comments": messageTextView.text ?? ""
            ] as [String : Any]
        showProgress()
        NetworkInterface.fetchJSON(.updateLessonStatus, payload: payload) { (success, data, response, error) -> (Void) in
           
            if data?["StatusCode"] as? Int  == 0 {
                lesson.status = "Rejected"
                
                DispatchQueue.main.async {
                    self.tableView?.reloadData()
                }
            }
            self.hideProgress()
            
             self.dismiss(animated: false, completion: nil)
        }

    }
}
