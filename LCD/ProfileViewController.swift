//
//  ProfileViewController.swift
//  DrivingAUS
//
//  Created by Kiran Sarella on 16/10/16.
//  Copyright © 2016 Sarella. All rights reserved.
//

import UIKit
import GSImageViewerController
class ProfileViewController: UIViewController , UIGestureRecognizerDelegate{

    @IBOutlet weak var packagesView: UIView!
    @IBOutlet weak var avalableLocationsView: UIView!
    @IBOutlet weak var heightForPackageView: NSLayoutConstraint!
    @IBOutlet weak var heightForLocationsView: NSLayoutConstraint!
    
    @IBOutlet weak var aboutView: UIView!
    @IBOutlet weak var heightForAboutView: NSLayoutConstraint!
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var experiencLabel: UILabel!
    @IBOutlet weak var licenceLabel: UILabel!
    @IBOutlet weak var drivingSchoolNameLabel: UILabel!
    @IBOutlet weak var carModelLabel: UILabel!
    @IBOutlet weak var automaticLabel: UILabel!

    @IBOutlet weak var mobileNumberLabel: UILabel!
    @IBOutlet weak var pricePerHourLabel: UILabel!
    @IBOutlet weak var aboutTextView: UITextView!
    @IBOutlet var logoutView: UIView!
    @IBOutlet var rightBarButton: UIButton!
    @IBOutlet var viewRatingView: UIView!
    @IBOutlet var ratingLabel: UILabel!

    @IBOutlet var noOfReviewsLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet weak var scrollImageViewWidth: NSLayoutConstraint!
    @IBOutlet weak var scrollImageView: UIView!
    @IBOutlet weak var imagesScrollView: UIScrollView!
//    var userprofile: UserProfile?
    var isNavigating = false


    @IBOutlet weak var tableViewContainer: UIView!

    @IBOutlet weak var packageTableview: UITableView!
    var instructorPackages : Array<InstructorPackages>?
    var tempArray = [[String: String]]()

    @IBOutlet weak var heightConstraint:NSLayoutConstraint!;




    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        rightBarButton.isSelected = false
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        logoutView.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        logoutView.layer.borderColor = UIColor.lightGray.cgColor
        logoutView.layer.borderWidth = 1
        

//
        
        profilePicture.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(profileFullImage(sender:)))
        tap.delegate = self
        profilePicture.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
        
   
    }

    func profileFullImage(sender: UITapGestureRecognizer? = nil) {
   
        let imageView = UIImageView()
        if let image = profilePicture.image {
            fullViewImageShow(image  , imageView:  imageView)
        }
    }
    

    
    func applyDataOnProfile(){
        let user =  Instructor.shared
//        mobileNumberLabel.text = user.phoneNumber ?? ""
        fullNameLabel.text = (user.firstName ?? "") + " " + (user.lastName ?? "")
        addressLabel.text = user.address ?? ""
        sexLabel.text = "\(user.gender?.lowercased() == "m" ? "Male" : "Female")"
        experiencLabel.text = "Exp: \(user.experience ?? 0)yrs"
        if (user.transmissionType == "Both" ){
            automaticLabel.text = "Automatic/Manual"
        }
        else{
            automaticLabel.text = user.transmissionType
        }
        drivingSchoolNameLabel.text = user.schoolName
        carModelLabel.text = user.carBodyType
        pricePerHourLabel.text = "$\(user.instructorPackages?.first?.rate ?? 0) / \(user.instructorPackages?.first?.hour == 1 ? "Hour" : "\(user.instructorPackages?.first?.hour ?? 0) Hours")"
        mobileNumberLabel.text = "Call" //user.phoneNumber ?? ""
        aboutTextView.text = user.aboutMe ?? ""
        ratingLabel.text = user.ratings ?? ""
        
        noOfReviewsLabel.text = "(\(user.noofRatings ?? 0) Reviews)"

//        if let package = user.instructorPackages?.filter({$0.packageTypeId == 16}).first {
//            priceLabel.text = "PRICE : $\(package.rate ?? 0)"
//            descriptionLabel.text = "DESCRIPTION : \(package.packageDesc ?? "")"
//        } else {
//            priceLabel.text = "PRICE : --"
//            descriptionLabel.text = "DESCRIPTION : --"
//        }


        instructorPackages = (user.instructorPackages?.filter({$0.packageTypeId == 16}))

        if (self.instructorPackages?.count == 0) {
            print("if")

            let tempDict = ["price":"PRICE : --","description":"DESCRIPTION : --"];
            tempArray.removeAll()
            tempArray.append(tempDict)
            self.packageTableview.separatorStyle = .none
            self.heightConstraint.constant = 104;
            self.view.layoutIfNeeded()

        } else {
            
            if (self.instructorPackages?.count == 1   ) {
                self.heightConstraint.constant = 104;
            }
            else if (self.instructorPackages?.count == 2) {
                self.heightConstraint.constant = 110 + 70;
            }
            else {
                self.heightConstraint.constant = 110 + 90;
            }
            self.view.layoutIfNeeded()

          print("else")

        }


        ratingLabel.backgroundColor = UIColorFromRGB(rgbValue: 0x19BC9D)
        if (noOfReviewsLabel.text == "(0 Reviews)") {
            ratingLabel.backgroundColor = UIColor.clear
            noOfReviewsLabel.text = ""
        }
        
        if user.isLicensed == true {
            licenceLabel.text = "Accredited Instructor"
        }else{
            licenceLabel.text = "Not Accredited Instructor"

        }
        
        if let imageURL = user.profilePicImageUrl {
            let url = URL(string: imageURL)
            profilePicture.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultFace"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        
    }

   /* override func performSegue(withIdentifier identifier: String, sender: Any?) {
        
        self.isNavigating = true
        
    }*/
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        logoutView.isHidden = true
        
        if Instructor.shared.isInstructorEdit == true {
            performSegue(withIdentifier: "EditProfileInstructor", sender: self)
        }else{
            applyDataOnProfile()
            self.showProgress()
            let params = ["userId":  "\(Instructor.shared.userID ?? 0)"]
            NetworkInterface.fetchJSON(.instructorProfile, headers: [:], params: params as NSDictionary?) { (success, data, response, error) -> (Void) in
                DispatchQueue.main.async {
                    self.hideProgress()
                    if data?["ProfileId"] != nil,let data = data {
                        Instructor.shared.updateProfile(dictionary: data)
                    }
                    self.packagesAvailbeView()
                    self.locationsAvailbeView()
                    self.imageScrollViewSetup()
                    
                    self.aboutView.backgroundColor = UIColor.clear
                    self.viewRatingView.backgroundColor = UIColor.clear
                    
                    self.profilePicture.layer.masksToBounds = true
                    self.profilePicture.layer.cornerRadius = self.profilePicture.frame.size.width / 2
                    self.editButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
                    
                    let imageView = UIImageView()
                    imageView.frame = CGRect(x: 10, y: 15, width: 30, height: 30)
                    imageView.image = #imageLiteral(resourceName: "call")
                    self.mobileNumberLabel.addSubview(imageView)
                    self.applyDataOnProfile()
                    self.packageTableview.reloadData()
                    
                    let imageView1 = UIImageView()
                    imageView1.frame = CGRect(x: 15, y: 20, width: 20, height: 20)
                    imageView1.image = #imageLiteral(resourceName: "ic_driver")
                    self.pricePerHourLabel.addSubview(imageView1)
                }
            }
        }
    }
    
    func imageScrollViewSetup () {
        
        //        let imagesCount = 15
        scrollImageView.subviews.forEach { (subview) in
            if subview is UIImageView {
                subview.removeFromSuperview()
            }
        }
        let schoolImages = (Instructor.shared.drivingSchoolImages)
        
        for index in 0..<(schoolImages?.count ?? 0){
            let imageView = UIImageView()
            let imageURL = schoolImages?[index].imageUrl
            let url = URL(string: imageURL!)
            imageView.kf.indicatorType = .activity
            imageView.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            imageView.layer.cornerRadius = 20
            imageView.layer.masksToBounds = true
            imageView.frame = CGRect(x: (40 * index + 10 * index), y: 0, width: 40, height: 41)
            scrollImageView.addSubview(imageView)
            
            imageView.tag = index
            imageView.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
            tap.delegate = self
            imageView.addGestureRecognizer(tap)
            
        }
        
        scrollImageViewWidth.constant = (CGFloat(10 * (schoolImages?.count)! + 40 * (schoolImages?.count)!))
        
        
    }
    
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        let view = sender?.view
//        print(view?.tag)
        let schoolImages = (Instructor.shared.drivingSchoolImages)
        let imageURL = schoolImages?[(view?.tag)!].imageUrl
        let url = URL(string: imageURL!)

        let imageView = UIImageView()
        imageView.frame = CGRect(x: self.imagesScrollView.frame.origin.x , y: self.imagesScrollView.frame.origin.x , width: 40, height: 41)
        imageView.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
        if let image = imageView.image {
            fullViewImageShow(image  , imageView:  imageView)
        }
        
    }
    
    func fullViewImageShow(_ image: UIImage , imageView : UIImageView) {
//        DispatchQueue.main.async(execute: {
            let imageInfo      = GSImageInfo(image: image, imageMode: .aspectFit, imageHD: nil)
            let transitionInfo = GSTransitionInfo(fromView: imageView)
            let imageViewer    = GSImageViewerController(imageInfo: imageInfo, transitionInfo: transitionInfo)
            self.present(imageViewer, animated: false, completion: nil)
//        })
    }
    
    func packagesAvailbeView()  {
        packagesView.backgroundColor = UIColor.clear
        packagesView.subviews.forEach { (subview) in
            if let _ = subview as? PackageButton {
                subview.removeFromSuperview()
            }
        }
        
        let packages:[String] = (Instructor.shared.instructorPackages?.filter({$0.packageTypeId != 16}).map({
            if let rate = $0.rate, let hour = $0.hour {
                if hour == 1 {
                    return "$\(rate) / Hour"
                }else{
                    return "$\(rate) / \(hour)Hours"
                }
                
            }
            return ""
        }) ?? [])
        var packagesArray = packages.filter({$0 != ""})
      
//        var packagesArray = [ "A", "Short"]
        
//        heightForPackageView.constant = CGFloat(60 + (packagesArray.count/4) * 40)

        var heightOfPackagesView = 0.0
        
        for index in 0..<packagesArray.count {
            let spaceAdded = "  "  + packagesArray[index] + "  "
            packagesArray[index] = spaceAdded
        }
        
        var indexOfLeftmostButtonOnCurrentLine = 0
        var buttons = [Any]()
        var runningWidth = 0.0
        let maxWidth = Double(self.view.frame.size.width - 40)
        let horizontalSpaceBetweenButtons = 10.0
        let verticalSpaceBetweenButtons = 10.0
        
        for index in 0..<packagesArray.count {
            let label = PackageButton()
            label.sizeToFit()
            label.backgroundColor = UIColor.lightGray
            label.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
            label.titleLabel?.font = UIFont (name: "Montserrat-Light", size: 14)
//            if !(index == packagesArray.count){
            label.setTitle(packagesArray[index], for: .normal)
//            }
            label.translatesAutoresizingMaskIntoConstraints = false
            packagesView.addSubview(label)
            // check if first button or button would exceed maxWidth
            if ((index == 0) ||  (runningWidth + Double(label.intrinsicContentSize.width) > maxWidth)) {
                // wrap around into next line
                runningWidth = Double(label.intrinsicContentSize.width)
                
                if (index == 0) {
                    
                    heightOfPackagesView = 40
                    let horizontalConstraint = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: packagesView, attribute: .left, multiplier: 1.0, constant: 20)
                    packagesView.addConstraint(horizontalConstraint)
                    // vertical position:
                    let verticalConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: packagesView, attribute: .top, multiplier: 1.0, constant: 40.0)
                 packagesView.addConstraint(verticalConstraint)
                    
                    
                } else {
                    heightOfPackagesView = heightOfPackagesView + 40

                    // put it in new line
                    let previousLeftmostButton = buttons[indexOfLeftmostButtonOnCurrentLine] as? UIButton
                    // horizontal position: same as previous leftmost button (on line above)
                    let horizontalConstraint = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: previousLeftmostButton, attribute: .left, multiplier: 1.0, constant: 0.0)
                    packagesView.addConstraint(horizontalConstraint)
                    // vertical position:
                    let verticalConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: previousLeftmostButton, attribute: .bottom, multiplier: 1.0, constant: CGFloat(verticalSpaceBetweenButtons))
                    packagesView.addConstraint(verticalConstraint)
                    indexOfLeftmostButtonOnCurrentLine = index
                    
                }
            } else {
                
                // put it right from previous buttom
                runningWidth += Double(label.intrinsicContentSize.width) + horizontalSpaceBetweenButtons
                let previousButton = buttons[(index - 1)] as? UIButton
                // horizontal position: right from previous button
                let horizontalConstraint = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: previousButton, attribute: .right, multiplier: 1.0, constant: CGFloat(horizontalSpaceBetweenButtons))
                packagesView.addConstraint(horizontalConstraint)
                // vertical position same as previous button
                let verticalConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: previousButton, attribute: .top, multiplier: 1.0, constant: 0.0)
                packagesView.addConstraint(verticalConstraint)
            }
            
            buttons.append(label)
        }
        
         heightForPackageView.constant = CGFloat(heightOfPackagesView) + 20
        
    }

    func locationsAvailbeView()  {
        avalableLocationsView.backgroundColor = UIColor.clear
        avalableLocationsView.subviews.forEach { (subview) in
            if subview as? UIButton != nil {
                subview.removeFromSuperview()
            }
        }
        
//        guard self.userprofile != nil else {
//            return;
//        }
        
        var heightOfLocationsView = 0.0
//
        let locations:[String] = (Instructor.shared.instructorSuburbs?.map({
            if let suburb = $0.suburb {
                return "\(suburb)"
            }
            return ""
        }) ?? [])
        
        var locationsArray = locations.filter({$0 != ""})

        
//        var locationsArray = [ "A" ]


        var indexOfLeftmostButtonOnCurrentLine = 0
        var buttons = [Any]()
        var runningWidth = 0.0
        let maxWidth = Double(self.view.frame.size.width - 40)
        let horizontalSpaceBetweenButtons = 10.0
        let verticalSpaceBetweenButtons = 10.0
        
        
        for index in 0..<locationsArray.count {
            let spaceAdded = "  "  + locationsArray[index] + "  "
            locationsArray[index] = spaceAdded
        }
        
        for index in 0..<locationsArray.count {
            let label = UIButton()
            label.sizeToFit()
            label.backgroundColor = UIColor.lightGray
            label.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
//            label.titleEdgeInsets = UIEdgeInsetsMake(10,10,10,10)
//            label.titleLabel?.font = UIFont.systemFont(ofSize: 11)
            label.titleLabel?.font = UIFont (name: "Montserrat-Light", size: 14)
                label.setTitle(locationsArray[index], for: .normal)
            label.translatesAutoresizingMaskIntoConstraints = false
            
            avalableLocationsView.addSubview(label)
            // check if first button or button would exceed maxWidth
            if ((index == 0) ||  (runningWidth + Double(label.intrinsicContentSize.width) > maxWidth)) {
                // wrap around into next line
                runningWidth = Double(label.intrinsicContentSize.width)
                
                if (index == 0) {
                    
                    heightOfLocationsView = 40
                    let horizontalConstraint = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: avalableLocationsView, attribute: .left, multiplier: 1.0, constant: 20)
                    avalableLocationsView.addConstraint(horizontalConstraint)
                    // vertical position:
                    let verticalConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: avalableLocationsView, attribute: .top, multiplier: 1.0, constant: 40.0)
                    avalableLocationsView.addConstraint(verticalConstraint)
                    
                    
                } else {
                    // put it in new line
                    heightOfLocationsView = heightOfLocationsView + 40

                    let previousLeftmostButton = buttons[indexOfLeftmostButtonOnCurrentLine] as? UIButton
                    // horizontal position: same as previous leftmost button (on line above)
                    let horizontalConstraint = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: previousLeftmostButton, attribute: .left, multiplier: 1.0, constant: 0.0)
                    avalableLocationsView.addConstraint(horizontalConstraint)
                    // vertical position:
                    let verticalConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: previousLeftmostButton, attribute: .bottom, multiplier: 1.0, constant: CGFloat(verticalSpaceBetweenButtons))
                    avalableLocationsView.addConstraint(verticalConstraint)
                    indexOfLeftmostButtonOnCurrentLine = index
                }
            } else {

                // put it right from previous buttom
                runningWidth += Double(label.intrinsicContentSize.width) + horizontalSpaceBetweenButtons
                let previousButton = buttons[(index - 1)] as? UIButton
                // horizontal position: right from previous button
                let horizontalConstraint = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: previousButton, attribute: .right, multiplier: 1.0, constant: CGFloat(horizontalSpaceBetweenButtons))
                avalableLocationsView.addConstraint(horizontalConstraint)
                // vertical position same as previous button
                let verticalConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: previousButton, attribute: .top, multiplier: 1.0, constant: 0.0)
                avalableLocationsView.addConstraint(verticalConstraint)
            }
            
            buttons.append(label)
        }

//        heightForLocationsView.constant = CGFloat(50 + (locationsArray.count/4) * 55)
          heightForLocationsView.constant = CGFloat(heightOfLocationsView) + 30
        
    }

    
    override func viewDidLayoutSubviews() {

    }
    
    @IBAction func editButtonClicked(_ sender: Any) {
        

        
    }
   
    @IBAction func changePasswordClicked(_ sender: Any) {
          self.changePassword()
    }
    @IBAction func logOutClicked(_ sender: Any) {
        Instructor.shared.logout()
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.setupRootForNoSession()
    }
    @IBAction func optionsButtonClicked(_ sender: Any) {
   
        if rightBarButton.isSelected == true{
            rightBarButton.isSelected = false
            logoutView.isHidden = true
        }
            
        else{
            rightBarButton.isSelected = true
            logoutView.isHidden = false
        }

        
//        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        
//        let sendButton = UIAlertAction(title: "CHANGE PASSWORD", style: .default, handler: { (action) -> Void in
//            self.changePassword()
//        })
//        
//        let  deleteButton = UIAlertAction(title: "LOGOUT", style: .default, handler: { (action) -> Void in
//            
//            Instructor.shared.logout()
//            let appdelegate = UIApplication.shared.delegate as! AppDelegate
//            appdelegate.setupRootForNoSession()
//        })
//        
//        let cancelButton = UIAlertAction(title: "CANCEL", style: .cancel, handler: { (action) -> Void in
//            
//         
//        })
//        
//        
//        alertController.addAction(sendButton)
//        alertController.addAction(deleteButton)
//        alertController.addAction(cancelButton)
//        
//        self.navigationController!.present(alertController, animated: true, completion: nil)
        
    }
    
    func changePassword() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ChangePassword")
        self.present(controller, animated: true, completion: nil)
    }
    @IBAction func addmoreClicked(_ sender: Any) {
    

    }

}

extension ProfileViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if (self.instructorPackages?.count == 0) {
            return tempArray.count
        }
        else {
            return (self.instructorPackages?.count ?? 0)
        }


    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilePackageCell") as? ProfileTestPackageTableViewCell


        if (self.instructorPackages?.count == 0) {

            let pachage =  tempArray[indexPath.row]
            cell?.priceTextLabel.text = pachage["price"]
            cell?.descriptionTextLabel.text = pachage["description"]
            return cell!
        }
        else {

            let pachage =  instructorPackages?[indexPath.row]
            cell?.priceTextLabel.text = "PRICE : $\(pachage?.rate ?? 0)"
            cell?.descriptionTextLabel.text = "DESCRIPTION : \(pachage?.packageDesc ?? "")"
            return cell!
        }
    }
}





