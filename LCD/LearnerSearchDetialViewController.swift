//
//  LearnerSearchDetialViewController.swift
//  LCD
//
//  Created by Admin on 07/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit
import GSImageViewerController
class LearnerSearchDetialViewController: UIViewController,UIGestureRecognizerDelegate {
    @IBOutlet var packagesView: UIView!
    @IBOutlet var avalableLocationsView: UIView!
    @IBOutlet var heightForPackageView: NSLayoutConstraint!

    @IBOutlet var heightForAboutView: NSLayoutConstraint!
    @IBOutlet var aboutView: UIView!
    @IBOutlet var heightForLocationsView: NSLayoutConstraint!
    @IBOutlet var profilePicture: UIImageView!
    @IBOutlet var fullNameLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var sexLabel: UILabel!
    @IBOutlet var experiencLabel: UILabel!
    @IBOutlet var licenceLabel: UILabel!
    @IBOutlet var drivingSchoolNameLabel: UILabel!
    @IBOutlet var carModelLabel: UILabel!
    @IBOutlet var automaticLabel: UILabel!
    @IBOutlet var mobileNumberLabel: UILabel!
    @IBOutlet var pricePerHourLabel: UILabel!
    @IBOutlet var aboutMeTextView: UITextView!
    @IBOutlet var noOfReviewsLabel: UILabel!
    @IBOutlet var ratingLabel: UILabel!
    @IBOutlet weak var bookLessonLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var scrollImagesViewWidth: NSLayoutConstraint!
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var scrollImagesView: UIView!
    var selectedUser: SearchedInstructor?

    @IBOutlet weak var tableViewContainer: UIView!

    @IBOutlet weak var packageTableview: UITableView!
    var instructorPackages : Array<InstructorPackages>?
    var tempArray = [[String: String]]()

    @IBOutlet weak var heightConstraint:NSLayoutConstraint!;

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        self.packagesAvailbeView()
        self.locationsAvailbeView()
        self.imageScrollViewSetup()
        
        self.aboutView.backgroundColor = UIColor.clear
        
        self.profilePicture.layer.masksToBounds = true
        self.profilePicture.layer.cornerRadius = self.profilePicture.frame.size.width / 2
        
        let imageView = UIImageView()
        imageView.frame = CGRect(x: 10, y: 15, width: 30, height: 30)
        imageView.image = #imageLiteral(resourceName: "call")
        self.mobileNumberLabel.addSubview(imageView)
        
        
        let imageView1 = UIImageView()
        imageView1.frame = CGRect(x: 15, y: 20, width: 20, height: 20)
        imageView1.image = #imageLiteral(resourceName: "ic_driver")
        self.bookLessonLabel.addSubview(imageView1)
        
        
        self.applyDataOnProfile()
        
        mobileNumberLabel.isUserInteractionEnabled = true
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(mobileNumberClicked))
        tap1.delegate = self
        mobileNumberLabel.addGestureRecognizer(tap1)
        
        bookLessonLabel.isUserInteractionEnabled = true
        let bookLessonGeasture = UITapGestureRecognizer(target: self, action: #selector(bookLessonClicked))
        tap1.delegate = self
        bookLessonLabel.addGestureRecognizer(bookLessonGeasture)
        
        
      self.sendRequestViewClicked.layer.cornerRadius =  CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        
        
    }
    
    func mobileNumberClicked()  {
        let user =  selectedUser!
        let mobileNum = user.phoneNumber ?? ""
        let phoneNum = "+61\(mobileNum)"
        if let url = URL(string: "telprompt://\(phoneNum)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
        else {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window?.makeToast("Phone number not valid.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
        }
        
    }
    
    func bookLessonClicked(){
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "InstructorBookLess") as! InstructorBookLessViewController
        let request = BookLessonRequest()
        request.imageURL = selectedUser?.profilePicImageUrl
        request.InstructorId = self.selectedUser?.userId
        request.LearnerId = Learner.shared.userID
        request.UserId = Learner.shared.userID
        request.instructorName = (selectedUser?.firstName ?? "") + " " + (selectedUser?.lastName ?? "")
        vc.request = request
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
    func applyDataOnProfile(){
        let user =  selectedUser!
        mobileNumberLabel.text =  "Call"//user.phoneNumber ?? ""
        fullNameLabel.text = (user.firstName ?? "") + " " + (user.lastName ?? "")
        addressLabel.text = user.address ?? ""
        sexLabel.text = "\(user.gender == "M" ? "Male" : "Female")"
        experiencLabel.text = "Exp: \(user.experience ?? 0)yrs"
        if (user.transmissionType == "Both" ){
            automaticLabel.text = "Automatic/Manual"
        }
        else{
            automaticLabel.text = user.transmissionType
        }
        drivingSchoolNameLabel.text = user.schoolName
        carModelLabel.text = user.carBodyType
//        pricePerHourLabel.text = "$\(user.instructorPackages?.first?.rate ?? 0) \(user.instructorPackages?.first?.rateMode ?? "")"
//        mobileNumberLabel.text = user.phoneNumber ?? ""
        aboutMeTextView.text = user.aboutMe ?? ""
        ratingLabel.text = user.ratings ?? ""
        
        noOfReviewsLabel.text = "(\(user.noofRatings ?? 0) Reviews)"

//        if let package = user.instructorPackages?.filter({$0.packageTypeId! != 15}).first {
//            pricePerHourLabel.text = "PRICE : $\(package.rate ?? 0)"
//            descriptionLabel.text = "DESCRIPTION : \(package.packageDesc ?? "")"
//        }else{
//            pricePerHourLabel.text = "PRICE : --"
//            descriptionLabel.text = "DESCRIPTION : --"
//        }

        instructorPackages = (user.instructorPackages?.filter({$0.packageTypeId != 15}))

        if (self.instructorPackages?.count == 0) {
            print("if")

            let tempDict = ["price":"PRICE : --","description":"DESCRIPTION : --"];
            tempArray.removeAll()
            tempArray.append(tempDict)
            self.heightConstraint.constant = 104;
            self.view.layoutIfNeeded()

        } else {

            if (self.instructorPackages?.count == 1 ) {
                self.heightConstraint.constant = 104;
            }
            else if (self.instructorPackages?.count == 2) {
                self.heightConstraint.constant = 110 + 70;
            }
            else {
                self.heightConstraint.constant = 110 + 90;
            }
            self.view.layoutIfNeeded()
            
            print("else")
        }

        ratingLabel.backgroundColor = UIColorFromRGB(rgbValue: 0x19BC9D)
        if (noOfReviewsLabel.text == "(0 Reviews)") {
            ratingLabel.backgroundColor = UIColor.clear
            noOfReviewsLabel.text = ""
        }
        
        if user.isLicensed == true {
            licenceLabel.text = "Accredited Instructor"
        }else{
            licenceLabel.text = "Not Accredited Instructor"
            
        }
        
        if let imageURL = user.profilePicImageUrl {
            let url = URL(string: imageURL)
            profilePicture.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultFace"), options: nil, progressBlock: nil, completionHandler: nil)
        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func packagesAvailbeView()  {
        packagesView.backgroundColor = UIColor.clear
        
        
        let packages:[String] = (selectedUser?.instructorPackages?.map({
            if let rate = $0.rate, let hour = $0.hour {
                if hour == 1 {
                    return "$\(rate) / Hour"
                }else{
                    return "$\(rate) / \(hour)Hours"
                }
            }

            return ""
        }) ?? [])
        var packagesArray = packages.filter({$0 != ""})
        
        var heightOfPackagesView = 0.0
        
        
        
        //        heightForPackageView.constant = CGFloat(60 + (packagesArray.count/4) * 40)
        
        for index in 0..<packagesArray.count {
            let spaceAdded = "  "  + packagesArray[index] + "  "
            packagesArray[index] = spaceAdded
        }
        
        var indexOfLeftmostButtonOnCurrentLine = 0
        var buttons = [Any]()
        var runningWidth = 0.0
        let maxWidth = Double(self.view.frame.size.width - 40)
        let horizontalSpaceBetweenButtons = 10.0
        let verticalSpaceBetweenButtons = 10.0
        
        for index in 0..<packagesArray.count {
            let label = UIButton()
            label.sizeToFit()
            label.backgroundColor = UIColor.lightGray
            label.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
            //            label.titleLabel?.font = UIFont (name: "Montserrat-Light", size: 14)
            label.titleLabel?.font = UIFont(name: FontName.LIGHT, size:CGFloat(FontSize.LABEL_MEDIUM) )
            
            //            label.titleLabel?.font = UIFont.systemFont(ofSize: 11)
            //            if !(index == packagesArray.count){
            label.setTitle(packagesArray[index], for: .normal)
            //            }
            label.translatesAutoresizingMaskIntoConstraints = false
            packagesView.addSubview(label)
            // check if first button or button would exceed maxWidth
            if ((index == 0) ||  (runningWidth + Double(label.intrinsicContentSize.width) > maxWidth)) {
                // wrap around into next line
                runningWidth = Double(label.intrinsicContentSize.width)
                
                if (index == 0) {
                    heightOfPackagesView = 40
                    
                    let horizontalConstraint = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: packagesView, attribute: .left, multiplier: 1.0, constant: 20)
                    packagesView.addConstraint(horizontalConstraint)
                    
                    
                    // vertical position:
                    let verticalConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: packagesView, attribute: .top, multiplier: 1.0, constant: 40.0)
                    packagesView.addConstraint(verticalConstraint)
                    
                    
                } else {
                    
                    heightOfPackagesView = heightOfPackagesView + 40
                    
                    // put it in new line
                    let previousLeftmostButton = buttons[indexOfLeftmostButtonOnCurrentLine] as? UIButton
                    // horizontal position: same as previous leftmost button (on line above)
                    let horizontalConstraint = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: previousLeftmostButton, attribute: .left, multiplier: 1.0, constant: 0.0)
                    packagesView.addConstraint(horizontalConstraint)
                    // vertical position:
                    let verticalConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: previousLeftmostButton, attribute: .bottom, multiplier: 1.0, constant: CGFloat(verticalSpaceBetweenButtons))
                    packagesView.addConstraint(verticalConstraint)
                    indexOfLeftmostButtonOnCurrentLine = index
                }
            } else {
                
                // put it right from previous buttom
                runningWidth += Double(label.intrinsicContentSize.width) + horizontalSpaceBetweenButtons
                let previousButton = buttons[(index - 1)] as? UIButton
                // horizontal position: right from previous button
                let horizontalConstraint = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: previousButton, attribute: .right, multiplier: 1.0, constant: CGFloat(horizontalSpaceBetweenButtons))
                packagesView.addConstraint(horizontalConstraint)
                // vertical position same as previous button
                let verticalConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: previousButton, attribute: .top, multiplier: 1.0, constant: 0.0)
                packagesView.addConstraint(verticalConstraint)
            }
            
            buttons.append(label)
        }
        
        heightForPackageView.constant = CGFloat(heightOfPackagesView) + 20
        
        
    }
    
    func imageScrollViewSetup () {
        
        //        let imagesCount = 15
        
        let schoolImages = (selectedUser?.drivingSchoolImages)
        
        for index in 0..<(schoolImages?.count ?? 0){
            let imageView = UIImageView()
            let imageURL = schoolImages?[index].imageUrl
            let url = URL(string: imageURL!)
            imageView.kf.indicatorType = .activity
            imageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultFace"), options: nil, progressBlock: nil, completionHandler: nil)
            imageView.layer.cornerRadius = 20
            imageView.layer.masksToBounds = true
            
            imageView.frame = CGRect(x: (40 * index + 10 * index), y: 0, width: 40, height: 41)
            scrollImagesView.addSubview(imageView)
            imageView.tag = index
            imageView.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
            tap.delegate = self
            imageView.addGestureRecognizer(tap)
            
        }
        
        scrollImagesViewWidth.constant = (CGFloat(10 * (schoolImages?.count)! + 40 * (schoolImages?.count)!))
        
        
    }
    
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        let view = sender?.view
        //        print(view?.tag)
        let schoolImages = (selectedUser?.drivingSchoolImages)
        let imageURL = schoolImages?[(view?.tag)!].imageUrl
        let url = URL(string: imageURL!)
        
        let imageView = UIImageView()
        imageView.frame = CGRect(x: self.imageScrollView.frame.origin.x , y: self.imageScrollView.frame.origin.x , width: 40, height: 41)
        imageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultFace"), options: nil, progressBlock: nil, completionHandler: nil)
        
        fullViewImageShow(imageView.image! , imageView:  imageView)
        
    }
    
    func fullViewImageShow(_ image: UIImage , imageView : UIImageView) {
        //        DispatchQueue.main.async(execute: {
        let imageInfo      = GSImageInfo(image: image, imageMode: .aspectFit, imageHD: nil)
        let transitionInfo = GSTransitionInfo(fromView: imageView)
        let imageViewer    = GSImageViewerController(imageInfo: imageInfo, transitionInfo: transitionInfo)
        self.present(imageViewer, animated: false, completion: nil)
        //        })
    }
    func locationsAvailbeView()  {
        avalableLocationsView.backgroundColor = UIColor.clear
        
        
        let locations:[String] = (selectedUser?.instructorSuburbs?.map({
            if let suburb = $0.suburb {
                return "\(suburb)"
            }
            return ""
        }) ?? [])
        
        var locationsArray = locations.filter({$0 != ""})
        
        
        
        var heightOfLocationsView = 0.0
        
        var indexOfLeftmostButtonOnCurrentLine = 0
        var buttons = [Any]()
        var runningWidth = 0.0
        let maxWidth = Double(self.view.frame.size.width - 40)
        let horizontalSpaceBetweenButtons = 10.0
        let verticalSpaceBetweenButtons = 10.0
        
        
        for index in 0..<locationsArray.count {
            let spaceAdded = "  "  + locationsArray[index] + "  "
            locationsArray[index] = spaceAdded
        }
        
        for index in 0..<locationsArray.count {
            let label = UIButton()
            label.sizeToFit()
            label.backgroundColor = UIColor.lightGray
            label.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
            //            label.titleEdgeInsets = UIEdgeInsetsMake(10,10,10,10)
            //            label.titleLabel?.font = UIFont (name: "Montserrat-Light", size: 14)
            label.titleLabel?.font = UIFont(name: FontName.LIGHT, size:CGFloat(FontSize.LABEL_MEDIUM))
            label.setTitle(locationsArray[index], for: .normal)
            label.translatesAutoresizingMaskIntoConstraints = false
            
            avalableLocationsView.addSubview(label)
            // check if first button or button would exceed maxWidth
            if ((index == 0) ||  (runningWidth + Double(label.intrinsicContentSize.width) > maxWidth)) {
                // wrap around into next line
                runningWidth = Double(label.intrinsicContentSize.width)
                
                if (index == 0) {
                    heightOfLocationsView = 40
                    
                    let horizontalConstraint = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: avalableLocationsView, attribute: .left, multiplier: 1.0, constant: 20)
                    avalableLocationsView.addConstraint(horizontalConstraint)
                    
                    
                    // vertical position:
                    let verticalConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: avalableLocationsView, attribute: .top, multiplier: 1.0, constant: 40.0)
                    avalableLocationsView.addConstraint(verticalConstraint)
                    
                    
                } else {
                    heightOfLocationsView = heightOfLocationsView + 40
                    
                    // put it in new line
                    let previousLeftmostButton = buttons[indexOfLeftmostButtonOnCurrentLine] as? UIButton
                    // horizontal position: same as previous leftmost button (on line above)
                    let horizontalConstraint = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: previousLeftmostButton, attribute: .left, multiplier: 1.0, constant: 0.0)
                    avalableLocationsView.addConstraint(horizontalConstraint)
                    // vertical position:
                    let verticalConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: previousLeftmostButton, attribute: .bottom, multiplier: 1.0, constant: CGFloat(verticalSpaceBetweenButtons))
                    avalableLocationsView.addConstraint(verticalConstraint)
                    indexOfLeftmostButtonOnCurrentLine = index
                }
            } else {
                
                // put it right from previous buttom
                runningWidth += Double(label.intrinsicContentSize.width) + horizontalSpaceBetweenButtons
                let previousButton = buttons[(index - 1)] as? UIButton
                // horizontal position: right from previous button
                let horizontalConstraint = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: previousButton, attribute: .right, multiplier: 1.0, constant: CGFloat(horizontalSpaceBetweenButtons))
                avalableLocationsView.addConstraint(horizontalConstraint)
                // vertical position same as previous button
                let verticalConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: previousButton, attribute: .top, multiplier: 1.0, constant: 0.0)
                avalableLocationsView.addConstraint(verticalConstraint)
            }
            
            buttons.append(label)
        }
        
        heightForLocationsView.constant = CGFloat(heightOfLocationsView) + 30
        
    }

    @IBAction func backButtonClicked(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
   
    @IBOutlet var sendRequestViewClicked: UIButton!

    @IBAction func sendReqButtonClicked(_ sender: Any) {
        let storyBoard = UIStoryboard.init(name: "Learner", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "preferredDaysSelectionViewController") as! PreferredDaysSelectionViewController
        let request = SendRequest()
        request.instructorImageURL = Learner.shared.profilePicImageUrl
        request.userId = Learner.shared.userID
        request.instructorId = selectedUser?.userId
        request.learnerId = Learner.shared.userID
        vc.request = request
        vc.selectedUser = selectedUser
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension LearnerSearchDetialViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (self.instructorPackages?.count == 0) {
            return tempArray.count
        }
        else {
            return (self.instructorPackages?.count ?? 0)
        }
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilePackageCell") as? ProfileTestPackageTableViewCell
        
        
        if (self.instructorPackages?.count == 0) {
            
            let pachage =  tempArray[indexPath.row]
            cell?.priceTextLabel.text = pachage["price"]
            cell?.descriptionTextLabel.text = pachage["description"]
            return cell!
        }
        else {
            
            let pachage =  instructorPackages?[indexPath.row]
            cell?.priceTextLabel.text = "PRICE : $\(pachage?.rate ?? 0)"
            cell?.descriptionTextLabel.text = "DESCRIPTION : \(pachage?.packageDesc ?? "")"
            return cell!
        }
    }
}
