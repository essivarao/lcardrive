//
//  LeranerRegistrationViewController.swift
//  LCD
//
//  Created by Admin on 02/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit
import MBProgressHUD

class LernerLeaanType{
    var master: MasterDetail?
    var selected: Bool = false
}

class LeranerRegistrationViewController: UIViewController ,UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate {
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var eamilTextField: UITextField!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet var tableView: UITableView!
    var details:[LernerLeaanType] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        let masters = AppDelegate.shared.masterDetails.filter({$0.masterType == "LessonType"})
        
        masters.forEach { (master) in
            let lernerTYpe = LernerLeaanType.init()
            lernerTYpe.master = master
            lernerTYpe.selected = false
            self.details.append(lernerTYpe)
        }
        
        firstName.delegate = self
        lastNameTextField.delegate = self
        phoneTextField.delegate = self
        passwordTextField.delegate = self
        eamilTextField.delegate = self
        
       phoneTextField.keyboardType = .numberPad
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == firstName) || (textField == lastNameTextField){
            let characterSet = CharacterSet.letters
            if string.rangeOfCharacter(from: characterSet.inverted) != nil {
                return false
            }
            else if  ((textField.text?.characters.count)! >= 50  && string != ""){
                return false
                
            }
        }
       else if (textField == phoneTextField) {
//            let characterSet = CharacterSet.letters
//            if string.rangeOfCharacter(from: characterSet.inverted) == nil {
//                return false
//            }
//            else
                if  ((textField.text?.characters.count)! >= 9  && string != ""){
                return false
            }
        }
        else if (textField == eamilTextField) {
            if  ((textField.text?.characters.count)! >= 50  && string != ""){
                return false
                
            }
        }

        else if (textField == passwordTextField) {
            if  ((textField.text?.characters.count)! >= 30  && string != ""){
                return false
                
            }
        }

            return true
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func registerClicked(_ sender: Any) {
        let firstName = self.firstName.text
        let lastName = self.lastNameTextField.text
        let mobileNumber = self.phoneTextField.text
        let emailId = self.eamilTextField.text
        let password = self.passwordTextField.text
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate

        
        if (!isValid(firstName: firstName!)) {
            //            displayAlert("ERROR", message: "INVALID FIRSTNAME")
            appdelegate.window?.makeToast("Please enter valid first name", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        else if (!isValid(lastName: lastName!)) {
            appdelegate.window?.makeToast("Please enter valid last name", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        else if (!isValid(phoneNumber: mobileNumber!)) {
            appdelegate.window?.makeToast("Mobile number should be 9 digits", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        else if (!isValid(email: emailId!)) {
            appdelegate.window?.makeToast("Please enter valid email address", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        else if (!isValid(password: password!)) {
            appdelegate.window?.makeToast("Please enter password", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        
//        if selectedType == nil {
//            displayAlert("ERROR", message: "Please select any of learn type")
//            return
//
//        }
        
        else{
            let selectedType = self.details.filter({$0.selected == true}).first
            var payload = ["FirstName": firstName!,"LastName": lastName!, "PhoneNumber": mobileNumber!, "Email": emailId!, "Password":password!, "UserType": "L"] as [String : Any]
            if selectedType != nil {
                payload["UserPreferenceId"] = selectedType?.master?.masterDetailId ?? 0
            }
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            NetworkInterface.fetchJSON(.regiseterInstructor, payload: payload) { (success, data, response, error) -> (Void) in
                DispatchQueue.main.async {
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let serviceStatus = data?["ServiceStatus"] as? [String:Any],serviceStatus["StatusCode"] as? Int == 1,  let errorMessage = serviceStatus["ErrorMessage"] as? String {
                        appdelegate.window?.makeToast(errorMessage, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    }else
                        if let data = data, data["UserID"] as? Int != nil {
                            Learner.shared.create(dictionary: data)
        
                            if let serviceStatus = data["ServiceStatus"] as? [String:Any] , let successMessage = serviceStatus["ServiceMessage"] as? String {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                    appdelegate.window?.makeToast(successMessage, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                                }
                            }
                            
                            let storyboard = UIStoryboard(name: "Learner", bundle: nil)
                            appdelegate.window?.rootViewController = storyboard.instantiateViewController(withIdentifier: "leaenerTabbar") as! UITabBarController
                            
                        }
                        else{
                            appdelegate.window?.makeToast("Register failed, Please try again", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    }
                }
            }
        }
    }
    @IBAction func cancelClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func displayAlert(_ title:String,message:String)  {
        let alert = UIAlertController(title: title , message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) -> Void in
            NSLog("button OK")
        }
        
        alert.addAction(defaultAction)
        present(alert, animated: true, completion:nil)  // 11
    }
    
    //TableView
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.details.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LearnerRegistration") as? LearnerRegistrationTableViewCell

        cell?.buttonLabel.text = self.details[indexPath.row].master?.value ?? ""
        
        if (self.details[indexPath.row].selected == true) {
            cell?.selectedImageView.image = #imageLiteral(resourceName: "ic_radio_btn_select")
        }else{
            cell?.selectedImageView.image = #imageLiteral(resourceName: "ic_radio_btn_unselect")
        }
        
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for detail in details {
            detail.selected = false
        }
        self.details[indexPath.row].selected = true
        tableView.reloadData()
    }
    
}

