//
//  DateExtension.swift
//  LCD
//
//  Created by Narendra Kumar R on 3/6/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import Foundation

extension Date {
    static let iso8601Formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
//        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return formatter
    }()
    
    
    var iso8601: String {
        return Date.iso8601Formatter.string(from: self)
    }
}

extension String {
    var dateFromISO8601: Date? {
        return Date.iso8601Formatter.date(from: self)
    }
    
    var formatterDate: String {
        let date = Date.iso8601Formatter.date(from: self)
        let formatter = DateFormatter.init()
        formatter.dateFormat = "dd/MM/yyyy hh:mm a"
        return formatter.string(from: date ?? Date())
    }
    
    var time: String {
        let date = Date.iso8601Formatter.date(from: self)
        let formatter = DateFormatter.init()
        formatter.dateFormat = "hh:mm a"
        return formatter.string(from: date ?? Date())
    }
    
    
}
