/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 

public class LearnerRequest {
	public var unReadMessagesCount : Int?
	public var instructorId : Int?
	public var instructorPhoneNumber : String?
	public var latitude : String?
	public var suburb : String?
	public var instructorName : String?
	public var longitude : String?
	public var lessonType : String?
	public var learnerId : Int?
	public var requestId : Int?
	public var postalCode : Int?
	public var lessonTypeId : Int?
	public var instructorProfilePic : String?
	public var learnerPreferredDays : [LearnerPreferredDays]?

    required public init?(dictionary: NSDictionary) {

		unReadMessagesCount = dictionary["UnReadMessagesCount"] as? Int
		instructorId = dictionary["InstructorId"] as? Int
		instructorPhoneNumber = dictionary["InstructorPhoneNumber"] as? String
		latitude = dictionary["Latitude"] as? String
		suburb = dictionary["Suburb"] as? String
		instructorName = dictionary["InstructorName"] as? String
		longitude = dictionary["Longitude"] as? String
		lessonType = dictionary["LessonType"] as? String
		learnerId = dictionary["LearnerId"] as? Int
		requestId = dictionary["RequestId"] as? Int
		postalCode = dictionary["PostalCode"] as? Int
		lessonTypeId = dictionary["LessonTypeId"] as? Int
		instructorProfilePic = dictionary["InstructorProfilePic"] as? String
		if (dictionary["LearnerPreferredDays"] != nil) { learnerPreferredDays = LearnerPreferredDays.modelsFromDictionaryArray(array: dictionary["LearnerPreferredDays"] as! NSArray) }
	}


}
