//
//  EnterPriceAndHoursViewController.swift
//  DrivingAUS
//
//  Created by Narendra Kumar on 2/18/17.
//  Copyright © 2017 Sarella. All rights reserved.
//

import UIKit

protocol PackagePriceAndHoursDelegate {
    func didSavePackage(package: Package?)
    func didCancelPackage(package: Package?)
}

class EnterPriceAndHoursViewController: UIViewController, UITextFieldDelegate {
    var delegate: PackagePriceAndHoursDelegate?
    var package: Package?

    @IBOutlet weak var generalDetailsView: UIView!
    @IBOutlet weak var testDriveDetailsView: UIView!
    
    @IBOutlet weak var tesrDrivePriceTF: UITextField!
    
    @IBOutlet weak var priceForGeneralLabel: UITextField!
    @IBOutlet weak var descriptionLabel: UITextView!
    
    
    @IBOutlet weak var descriptionTV: UITextView!
    @IBOutlet weak var priceLabel: UITextField!
    @IBOutlet weak var hoursLabel: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        view.isOpaque = true
        
        priceForGeneralLabel.delegate = self
        priceLabel.delegate = self
        descriptionLabel.layer.borderColor = UIColor.lightGray.cgColor
        descriptionLabel.layer.borderWidth = 1
        descriptionLabel.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        descriptionLabel.layer.masksToBounds = true
        
//        priceForGeneralLabel
        priceForGeneralLabel.layer.borderColor = UIColor.lightGray.cgColor
        priceForGeneralLabel.layer.borderWidth = 1
        priceForGeneralLabel.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        priceForGeneralLabel.layer.masksToBounds = true

        hoursLabel.keyboardType = .numberPad
        
        if package?.type == 16 {
            testDriveDetailsView.isHidden = false
            descriptionTV.text = package?.discription ?? ""
            if let rate  = package?.rate {
                priceForGeneralLabel.text = "\(rate)"
            }
        }else{
            generalDetailsView.isHidden = false
            if let rate  = package?.rate {
                priceLabel.text = "\(rate)"
            }
            if let hours  = package?.hours {
                hoursLabel.text = "\(hours)"
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        guard textField === priceForGeneralLabel || textField === tesrDrivePriceTF, let text = textField.text else { return true }
//        let newLength = text.characters.count + string.characters.count - range.length
        if textField == priceForGeneralLabel{
            if  ((textField.text?.characters.count)! >= 5  && string != ""){
                return false
            }
        }
        
        if textField == priceLabel{
            if  ((textField.text?.characters.count)! >= 5  && string != ""){
                return false
            }
        }
        
        return true
        
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        delegate?.didCancelPackage(package: package)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func doneButtonClicked(_ sender: Any) {
        switch package!.type! {
        case 15:
            package?.rate = Int(priceLabel.text ?? "0")
            package?.hours = Int(hoursLabel.text ?? "0")
            break
        case 16:
            if descriptionTV.text.characters.count == 0 {
                AppDelegate.shared.window?.makeToast("Please enter driving test package description", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                return
            }
            package?.rate = Int(priceForGeneralLabel?.text ?? "0")
            package?.discription = descriptionTV?.text
            break
        default:
            break
        }
        delegate?.didSavePackage(package: package)
        self.dismiss(animated: true, completion: nil)
    }
}


