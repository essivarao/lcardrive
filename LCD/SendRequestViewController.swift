//
//  SendRequestViewController.swift
//  LCD
//
//  Created by Admin on 08/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class SendRequestViewController: UIViewController , UITableViewDelegate , UITableViewDataSource, UITextFieldDelegate{

    @IBOutlet var profilePicture: UIImageView!
    @IBOutlet var fullNameLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var sendSupburbTextField: UITextField!
    @IBOutlet var messageTextView: UITextView!
    @IBOutlet var tableViewHeightConstraints: NSLayoutConstraint!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var sendButton: UIButton!
    var selectedLocation: SelectedSendRequestLocation?
    var details:[LernerLeaanType] = []

    var request: SendRequest?
    var selectedUser: SearchedInstructor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let masters = AppDelegate.shared.masterDetails.filter({$0.masterType == "LessonType"})
        
        masters.forEach { (master) in
            let lernerTYpe = LernerLeaanType.init()
            lernerTYpe.master = master
            lernerTYpe.selected = false
            self.details.append(lernerTYpe)
        }
        
        
        if let message = AppDelegate.shared.configValues.filter({$0.configKey == "RequestDefaultMsg"}).first?.configValue {
            self.messageTextView.text = message
        }


        messageTextView.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        messageTextView.layer.borderColor = UIColor.lightGray.cgColor
        messageTextView.layer.borderWidth = 2
        profilePicture.layer.cornerRadius = 25
        profilePicture.layer.masksToBounds = true
        sendSupburbTextField.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        sendSupburbTextField.layer.borderColor = UIColor.lightGray.cgColor
        sendSupburbTextField.layer.borderWidth = 2
        
        
        cancelButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        sendButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        
        self.fullNameLabel.text = (selectedUser?.firstName ?? "") + " " + (selectedUser?.lastName ?? "")

        if let imageURL = selectedUser?.profilePicImageUrl {
            let url = URL(string: imageURL)
            profilePicture.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultFace"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        tableViewHeightConstraints.constant = CGFloat((self.details.count ) * 50)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.resignFirstResponder()
        
        let storyBoard = UIStoryboard.init(name: "Learner", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SelectPlaceForRequestView") as! SelectPlaceForRequestViewController
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //TableView
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.details.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SendRequestLookingForTableViewCell") as? SendRequestLookingForTableViewCell
        
        cell?.fullTitleLabel.text = self.details[indexPath.row].master?.value ?? ""
        
        if (self.details[indexPath.row].selected == true) {
            cell?.selectedImageView.image = #imageLiteral(resourceName: "ic_radio_btn_select")
        }else{
            cell?.selectedImageView.image = #imageLiteral(resourceName: "ic_radio_btn_unselect")
        }
        
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for detail in details {
            detail.selected = false
        }
        self.details[indexPath.row].selected = true
        tableView.reloadData()
    }
    
   
    @IBAction func backButtonClicked(_ sender: Any) {
//        SearchUserDetailWithOutSignInViewController.selectedUserInMemory = nil

        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func cancelClicked(_ sender: Any) {
//        SearchUserDetailWithOutSignInViewController.selectedUserInMemory = nil

        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func sendButtonClicked(_ sender: Any) {
        
        if self.details.filter({$0.selected == true}).count == 0 {
            AppDelegate.shared.window?.makeToast("Please select any learn type", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }

        
        
        self.sendRequest()
    }
}

extension SendRequestViewController {
    func sendRequest(){
        var preferDays:[[String:Any]] = []
        request?.preferDays.forEach({ (preference) in
            preferDays.append([ "WeekDay": preference.weekDay?.uppercased() ?? "",
                                "Period": preference.period?.uppercased() ?? ""])
        })
        
        if selectedLocation == nil {
            AppDelegate.shared.window?.makeToast("Please select any suburb", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        
        var payload:[String : Any] = [:]
            payload["LearnerId"] = request?.learnerId ?? 0
            payload["InstructorId"] = request?.instructorId ?? 0
            payload["UserId"] = request?.userId ?? 0
            payload["Suburb"] = selectedLocation?.locality ?? ""
            payload["PostalCode"] = selectedLocation?.pin ?? 0
            payload["Latitude"] = selectedLocation?.lat ?? 0
            payload["Longitude"] = selectedLocation?.long ?? 0
            payload["Message"] = self.messageTextView.text ?? ""
            payload["RequestPreferredDays"] =  preferDays
            payload["MessageDate"] = Date().iso8601 
        
        let selectedType = self.details.filter({$0.selected == true}).first
        if selectedType != nil {
            payload["LessonTypeId"] = selectedType?.master?.masterDetailId ?? 0
        }
        showProgress()
        NetworkInterface.fetchJSON(.sendRequest, payload: payload) { (success, data, response, error) -> (Void) in
            self.hideProgress()
            if let status = data as? [String:Any], status["StatusCode"] as? Int == 0, let message = status["ServiceMessage"] as? String {
                DispatchQueue.main.async {
                    AppDelegate.shared.window?.makeToast(message , duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    SearchUserDetailWithOutSignInViewController.selectedUserInMemory = nil
                    _ = self.navigationController?.popToRootViewController(animated: false)
                    let tabBarController = AppDelegate.shared.window?.rootViewController as! UITabBarController
                    tabBarController.selectedIndex = 1
                    
                }
            }
            else if let status = data as? [String:Any] , let errorMessage = status["ErrorMessage"] as? String {
                    DispatchQueue.main.async {
                    AppDelegate.shared.window?.makeToast(errorMessage, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                }
                }
            }
          
        }
        
    }

extension SendRequestViewController:RequestMapDelegate{
    func didSelectLocation(location: SelectedSendRequestLocation){
        self.sendSupburbTextField.text = location.locality ?? ""
        self.selectedLocation = location
    }
}
