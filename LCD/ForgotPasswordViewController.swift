//
//  ForgotPasswordViewController.swift
//  LCD
//
//  Created by Admin on 01/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var cacelButton: UIButton!
    @IBOutlet var submitButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSetup()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func submitClicked(_ sender: Any) {
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate

        if(!isValid(email: emailTextField.text!)) && (!isValid(phoneNumber: emailTextField.text!)) {
//            displayAlert("ERROR", message: "Enter valid email address/phone number")
            
            appdelegate.window?.makeToast("Please enter valid email address/phone number", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        else{
            self.showProgress()
            NetworkInterface.fetchJSON(.forgetPassword, payload: ["EmailOrMobileNumber":emailTextField.text!], requestCompletionHander: { (success, data, response, errir) -> (Void) in
                DispatchQueue.main.async {
                    self.hideProgress()
                    if let status = data?["ServiceStatus"] as? [String:Any], status["StatusCode"] as? Int == 0, let message = status["ServiceMessage"] as? String {
                        appdelegate.window?.makeToast(message, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                        self.dismiss(animated: true, completion: {
                        })
                    }else if let status = data?["ServiceStatus"] as? [String:Any],let message = status["ErrorMessage"] as? String {
                        appdelegate.window?.makeToast(message, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    }else{
                        appdelegate.window?.makeToast("Please try later", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    }
                }
            })
            

            
        }
        
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func displayAlert(_ title:String,message:String)  {
        let alert = UIAlertController(title: title , message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) -> Void in
            NSLog("button OK")
        }
        
        alert.addAction(defaultAction)
        present(alert, animated: true, completion:nil)  // 11
    }
    
    
}
extension ForgotPasswordViewController{
    
    func  viewSetup()  {
    
        
        cacelButton.titleLabel?.font = UIFont (name:FontName.REGULAR , size: CGFloat(FontSize.BUTTON_MEDIUM))
        submitButton.titleLabel?.font = UIFont (name:FontName.REGULAR , size: CGFloat(FontSize.BUTTON_MEDIUM))
        cacelButton.backgroundColor = UIColorFromRGB(rgbValue: Color.GRAY_COLOR)
        submitButton.backgroundColor = UIColorFromRGB(rgbValue: Color.APP_THEMECOLOR)
        
    }
    
}
