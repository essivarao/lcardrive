//
//  Learner.swift
//  LCD
//
//  Created by Narendra Kumar R on 3/3/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import Foundation

class Learner:NSObject,NSCoding {
    
    static let shared = Learner()
    
    public var userID : Int?
    public var firstName : String?
    public var lastName : String?
    public var phoneNumber : String?
    public var email : String?
    public var userType : String?
    public var loginTypeId : Int?
    public var loginType : String?
    public var sCNKey : String?
    public var profilePicImageUrl : String?
    public var isInstructorEdit : String?
    
    func create(dictionary: NSDictionary) {
        userID = dictionary["UserID"] as? Int
        firstName = dictionary["FirstName"] as? String
        lastName = dictionary["LastName"] as? String
        phoneNumber = dictionary["PhoneNumber"] as? String
        email = dictionary["Email"] as? String
        userType = dictionary["UserType"] as? String
        loginTypeId = dictionary["LoginTypeId"] as? Int
        loginType = dictionary["LoginType"] as? String
        sCNKey = dictionary["SCNKey"] as? String
        profilePicImageUrl = dictionary["ProfilePicImageUrl"] as? String
        isInstructorEdit = dictionary["IsInstructorEdit"] as? String
    }
    
    convenience required init(coder aDecoder: NSCoder) {
        self.init()
        userID = aDecoder.decodeObject(forKey: "userID") as? Int
        firstName = aDecoder.decodeObject(forKey: "firstName") as? String
        lastName = aDecoder.decodeObject(forKey: "lastName") as? String
        phoneNumber = aDecoder.decodeObject(forKey: "phoneNumber") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        userType = aDecoder.decodeObject(forKey: "userType") as? String
        loginTypeId = aDecoder.decodeObject(forKey: "loginTypeId") as? Int
        loginType = aDecoder.decodeObject(forKey: "loginType") as? String
        sCNKey = aDecoder.decodeObject(forKey: "sCNKey") as? String
        profilePicImageUrl = aDecoder.decodeObject(forKey: "profilePicImageUrl") as? String
        isInstructorEdit = aDecoder.decodeObject(forKey: "isInstructorEdit") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(userID, forKey: "userID")
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(phoneNumber, forKey: "phoneNumber")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(userType, forKey: "userType")
        aCoder.encode(loginTypeId, forKey: "loginTypeId")
        aCoder.encode(loginType, forKey: "loginType")
        aCoder.encode(sCNKey, forKey: "sCNKey")
        aCoder.encode(profilePicImageUrl, forKey: "profilePicImageUrl")
        aCoder.encode(isInstructorEdit, forKey: "isInstructorEdit")
    }
    
    func logout(){
        userID = nil
        firstName = nil
        lastName = nil
        phoneNumber = nil
        email = nil
        userType = nil
        loginTypeId = nil
        loginType = nil
        sCNKey = nil
        profilePicImageUrl = nil
        isInstructorEdit = nil
        SelectedLocation.shared.delete()
        self.synchronize()
    }
}
extension Learner {

    func synchronize(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(Learner.shared, toFile: Utils.sessionObjectPath().path)
        if !isSuccessfulSave {
            print("Failed to save user...")
        }
    }
    
    func unarchive() {
        let user = NSKeyedUnarchiver.unarchiveObject(withFile: Utils.sessionObjectPath().path) as? Learner
        userID = user?.userID
        firstName = user?.firstName
        lastName = user?.lastName
        phoneNumber = user?.phoneNumber
        email = user?.email
        userType = user?.userType
        loginTypeId = user?.loginTypeId
        loginType = user?.loginType
        sCNKey = user?.sCNKey
        profilePicImageUrl = user?.profilePicImageUrl
        isInstructorEdit = user?.isInstructorEdit
    }
   
}
