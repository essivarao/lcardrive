//
//  FullViewImageViewController.swift
//  LCD
//
//  Created by Admin on 19/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

protocol FullImageDelegate {
    func didDeleteImage()
}

class FullViewImageViewController: UIViewController {
    
    @IBOutlet weak var img: UIImageView!
    var currentnImageIndex:NSInteger = 0

    var arrayOfImages : Array<Any> = []
    var image: DrivingSchoolImages?
    var delegate: FullImageDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        if let imageURL = image?.imageUrl, let url = URL(string: imageURL) {
            img.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
        }

        img.isUserInteractionEnabled = true
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(FullViewImageViewController.swiped(_:)))
        img.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(FullViewImageViewController.swiped(_:)))
        img.addGestureRecognizer(swipeLeft)
 
    }
    
    func swiped(_ gesture: UIGestureRecognizer)
    {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer{
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("right swipe")
            case UISwipeGestureRecognizerDirection.left:
                print("left swipe")
            default:
                print("other swipe")
            }
        }
        
    }
    @IBAction func deleteButtonClicked(_ sender: Any) {
        image?.isActive = false
        delegate?.didDeleteImage()
        self.dismiss(animated: false, completion: nil)
  }
    @IBAction func cancelButtonClicked(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
