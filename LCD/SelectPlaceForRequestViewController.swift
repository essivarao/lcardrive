//
//  SelectPlaceForRequestViewController.swift
//  LCD
//
//  Created by Narendra Kumar on 3/11/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

protocol RequestMapDelegate {
    func didSelectLocation(location: SelectedSendRequestLocation)
}

class SelectedSendRequestLocation{
    var lat: String?
    var long:String?
    var locality: String?
    var pin: String?
}

class SelectPlaceForRequestViewController: UIViewController, GMSMapViewDelegate, UITextFieldDelegate, GMSAutocompleteViewControllerDelegate {
    

    
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet var searchTextField: UITextField!
    var delegate: RequestMapDelegate?
    let geocoder = GMSGeocoder()
    private var selectedAddress = SelectedSendRequestLocation()

    var locationManager = CLLocationManager()

    @IBOutlet weak var mapView: GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        searchTextField
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 40, height: 40))
        searchTextField.leftView = paddingView
        searchTextField.leftViewMode = UITextFieldViewMode.always
        searchTextField.delegate = self

        //Location Manager code to fetch current location
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        
        let imageView = UIImageView()
        imageView.frame = CGRect(x: mapView.frame.size.width/2 - 15 , y: mapView.frame.size.height/2 - 15, width: 30, height: 30)
        imageView.image = #imageLiteral(resourceName: "address_marker")
        mapView.addSubview(imageView)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let googleAutoVC = GMSAutocompleteViewController.init()
        googleAutoVC.delegate = self
        googleAutoVC.view.backgroundColor = UIColor.clear
        let filter = GMSAutocompleteFilter.init()
        filter.country = "AU"
//        filter.type = .region

        googleAutoVC.autocompleteFilter = filter
        
        self.present(googleAutoVC, animated: true) {
            
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt cameraPosition: GMSCameraPosition) {

        geocoder.reverseGeocodeCoordinate(cameraPosition.target) { (response, error) in
            guard error == nil else {
                return
            }
            
            if let result = response?.firstResult() {
                
                var address = ""
                guard let lines = result.lines else {
                    return
                }
                self.selectedAddress.lat = "\(result.coordinate.latitude)"
                self.selectedAddress.long = "\(result.coordinate.longitude)"
                self.selectedAddress.pin = result.postalCode
                address = lines.joined(separator: ", ")
                self.searchTextField.text = address
                self.selectedAddress.locality = address

            }
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    @IBAction func backButtonClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)

    }
    @IBAction func cancelButtonClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func continueButtonClicked(_ sender: Any) {
        self.delegate?.didSelectLocation(location: selectedAddress)
        _ = self.navigationController?.popViewController(animated: true)

    }
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        selectedAddress.lat = "\(place.coordinate.latitude)"
        selectedAddress.long = "\(place.coordinate.longitude)"
        let suburbString = place.formattedAddress?.replacingOccurrences(of: ", Australia", with: "")
        selectedAddress.locality = suburbString
        
        if let postalCodeComponent =  place.addressComponents?.filter({$0.type == "postal_code"}).first {
            selectedAddress.pin = postalCodeComponent.name
        }
        mapView.clear()
        CATransaction.begin()
        CATransaction.setAnimationDuration(1.0)
        let marker = GMSMarker()
        marker.position = place.coordinate
        marker.title = selectedAddress.locality ?? ""
        marker.snippet = "Australia"
        marker.map = mapView
        CATransaction.commit()
        let camera = GMSCameraPosition.camera(withTarget: place.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        mapView.camera = camera

        self.searchTextField.text = place.addressComponents!.map{$0.name}.joined(separator: ",")
        viewController.dismiss(animated: true) {
            
        }
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error.localizedDescription)")
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        viewController.dismiss(animated: true, completion: nil)
    }
}

extension SelectPlaceForRequestViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        
        mapView.animate(to: camera)
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
        
    }
    
    
}

