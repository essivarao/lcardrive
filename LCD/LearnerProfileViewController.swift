//
//  LearnerProfileViewController.swift
//  DrivingAUS
//
//  Created by Narendra Kumar on 2/20/17.
//  Copyright © 2017 Sarella. All rights reserved.
//

import UIKit

class LearnerProfileViewController: UIViewController , UITextFieldDelegate{
    
    @IBOutlet weak var profilePicImageView: UIImageView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var optionsView: UIView!
    @IBOutlet var editButton: UIButton!
    var rightBarButton = UIButton()
    var saveButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setSearchAndFilterAsRightNavigationItems()
        profilePicImageView.isUserInteractionEnabled = false
        
        
        firstNameTextField.isEnabled = false
        lastNameTextField.isEnabled = false
        phoneNumberTextField.isEnabled = false
        emailTextField.isEnabled = false
        
        firstNameTextField.text = Learner.shared.firstName ?? ""
        lastNameTextField.text = Learner.shared.lastName ?? ""
        phoneNumberTextField.text = Learner.shared.phoneNumber ?? ""
        emailTextField.text = Learner.shared.email ?? ""
        
        if let imageURL = Learner.shared.profilePicImageUrl {
            let url = URL(string: imageURL)
            profilePicImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultFace"), options: nil, progressBlock: nil, completionHandler: nil)
        }

        optionsView.isHidden = true
        
        editButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)

        gestureAddedOnImageView()
        optionsView.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        optionsView.layer.borderColor = UIColor.lightGray.cgColor
        optionsView.layer.borderWidth = 1
        // Do any additional setup after loading the view.
    }
    
    func gestureAddedOnImageView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(profilePictapped))
        profilePicImageView.addGestureRecognizer(tap)
    }
    
    func profilePictapped (){
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.mediaTypes = ["public.image"]
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        rightBarButton.isSelected = true
        
        saveButton.isHidden = true
        editButton.isHidden = false
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setSearchAndFilterAsRightNavigationItems(){
        
        self.title = "PROFILE"
        saveButton = UIButton.init(type: .custom)
        saveButton.setTitle("SAVE", for: .normal)
        saveButton.frame = CGRect.init(x: 0, y: 0, width: 80, height: 40)
        saveButton.addTarget(self, action: #selector(saveButtonClicked(sender:)), for: .touchUpInside)
        let leftEditButton = UIBarButtonItem.init(customView: saveButton)
        
        saveButton.isHidden = true
        
        rightBarButton = UIButton.init(type: .custom)
//        rightBarButton.setImage(#imageLiteral(resourceName: "ic_more"), for: .normal)
        rightBarButton.setTitle("Settings", for: .normal)
        rightBarButton.titleLabel?.font = UIFont(name: FontName.REGULAR, size:CGFloat(FontSize.BUTTON_MEDIUM) )
        rightBarButton.addTarget(self, action: #selector(rightBarButtonClicked), for: .touchUpInside)
        rightBarButton.frame = CGRect.init(x: 0, y: 0, width: 100, height: 40)
        let filter = UIBarButtonItem.init(customView: rightBarButton)
        self.navigationItem.rightBarButtonItem = filter
        
        self.navigationItem.leftBarButtonItem = leftEditButton
    }
    
    func saveButtonClicked(sender : UIButton) {
        optionsView.isHidden = true
        editButton.isHidden = false
        saveButton.isHidden = true

        firstNameTextField.isEnabled = false
        lastNameTextField.isEnabled = false
        phoneNumberTextField.isEnabled = false
        emailTextField.isEnabled = false

        
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        
        
        if !isValid(firstName: firstNameTextField.text!){
            appdelegate.window?.makeToast("Please enter valid first name", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        else if !isValid(lastName: lastNameTextField.text!){
            appdelegate.window?.makeToast("Please enter valid last name", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
            
        else if !isValid(phoneNumber: phoneNumberTextField.text!){
            appdelegate.window?.makeToast("Mobile number should be 9 digits", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        else{
            let imgData = UIImageJPEGRepresentation(profilePicImageView.image!, 0.5)! as Data
            let baseEncode = imgData.base64EncodedString()
            var dictUser:[String:Any] = [:]
            dictUser["UserID"] = Learner.shared.userID!
            dictUser["FirstName"] = firstNameTextField.text!
            dictUser["LastName"] = lastNameTextField.text!
            dictUser["PhoneNumber"] = phoneNumberTextField.text!
            dictUser["Email"] = emailTextField.text!
            dictUser["ProfilePicImage"] = baseEncode
            self.showProgress()
            NetworkInterface.fetchJSON(.learnerUpdateProfile, payload: dictUser) { (success, data, response, error) -> (Void) in
                DispatchQueue.main.async {
                    self.hideProgress()
                    
                    if let status = data?["ServiceMessage"] as? String, data?["StatusCode"] as? Int == 0  {
                        self.dismiss(animated: true, completion: {
                        })
                        appdelegate.window?.makeToast( status , duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                        Learner.shared.firstName = self.firstNameTextField.text
                        Learner.shared.lastName = self.lastNameTextField.text
                        Learner.shared.phoneNumber = self.phoneNumberTextField.text
                        Learner.shared.email = self.emailTextField.text
                        
                        Learner.shared.synchronize()
                        
                    }else if let message = data?["ErrorMessage"] as? String, data?["StatusCode"] as? Int == 1 {
                        appdelegate.window?.makeToast( message , duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    }else{
                        appdelegate.window?.makeToast( "Please try again" , duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    }
                    
                    
                    if let data = data as? [String:Any] {
                        print(data)
                    }
                }
                
            }
            
        }
        
    }
    func rightBarButtonClicked(sender : UIButton )  {
        
        
        if rightBarButton.isSelected == true{
            
            rightBarButton.isSelected = false
            optionsView.isHidden = false
            
        }
            
        else{
            rightBarButton.isSelected = true
            optionsView.isHidden = true
            
        }
        
        
        
    }
    func updateUserDetails()  {
        
        
        //
    }
    
    //    func didUpdateUser(response: Dictionary<String, Any>) {
    //        DispatchQueue.main.async {
    //
    //            MBProgressHUD.hide(for: self.view, animated: true)
    //            User.shared.learner?.email = self.emailTextField.text
    //            User.shared.learner?.mobile = self.phoneNumberTextField.text
    //            User.shared.learner?.firstName = self.firstNameTextField.text
    //            User.shared.learner?.lastName = self.lastNameTextField.text
    //            User.shared.learner?.persistData()
    //
    //        }
    //
    //    }
    
    @IBAction func changePasswordClicked(_ sender: Any) {
        optionsView.isHidden = true
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "changePassword") as! ChangePassword
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    @IBAction func logoutClicked(_ sender: UIButton) {
        Learner.shared.logout()
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.setupRootForNoSession()
    }
    
    @IBAction func editButtonClicked(_ sender: Any) {
        saveButton.isHidden = false
        editButton.isHidden = true
        
        firstNameTextField.isEnabled = true
        lastNameTextField.isEnabled = true
        phoneNumberTextField.isEnabled = true
        profilePicImageView.isUserInteractionEnabled = true

        //        emailTextField.isEnabled = false
        
    }
    
}
extension LearnerProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        print("Image -\(info)")
        var image = info[UIImagePickerControllerOriginalImage] as! UIImage
        image = image.fixOrientation()
        
        profilePicImageView.image = image
        
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
