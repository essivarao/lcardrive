//
//  RejectedReasonViewController.swift
//  LCD
//
//  Created by kvanaworld on 4/2/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class RejectedReasonViewController: UIViewController {
    var lesson: InstrctorBookLessonHistory?
    @IBOutlet weak var reasonTextView: UITextView!

    override func viewDidLoad() {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    @IBAction func cancelButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func sendButtonClicked(_ sender: Any) {
        let payload = [
            "Lessonid": lesson?.lessonId ?? 0,
            "UserId": Learner.shared.userID ?? 0,
            "Status": "Rejected",
            "comments": reasonTextView.text ?? ""
            ] as [String : Any]
        showProgress()
        NetworkInterface.fetchJSON(.updateLessonStatus, payload: payload) { (success, data, response, error) -> (Void) in
            
            if data?["StatusCode"] as? Int  == 0 {
                self.lesson?.status = "Rejected"
            }
            self.hideProgress()
            DispatchQueue.main.async {
                self.dismiss(animated: false, completion: nil)
            }
        }

    }
}
