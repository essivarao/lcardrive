//
//  InstructorMessagesViewController.swift
//  LCD
//
//  Created by Admin on 10/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class InstructorMessagesViewController: UIViewController ,UITableViewDelegate , UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    var requestId: Int?
    var isLesson: Bool = false;
    var messages:[InstructorMessage] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isLesson {
            fetchMessages(.learnerMessages,headers: ["LessonId":"\(requestId ?? 0)"])
        }else{
        fetchMessages()
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InstructorMessagesTableViewCell") as? InstructorMessagesTableViewCell
        
        let message = messages[indexPath.row]
        cell?.messageLabel.text = message.message ?? ""
        if let learnerName = message.learner?.trimmingCharacters(in: CharacterSet.init(charactersIn: " ")), learnerName.characters.count > 0 {
            
            cell?.nameLabel.text = learnerName
        }else if let insrectorName = message.instructor{
            cell?.nameLabel.text = insrectorName
        }
        
        cell?.timeLabel.text = message.messageDate?.formatterDate ?? ""
        
        
        if let lerrnerPic = message.learnerProfilePic {
            let url = URL(string: lerrnerPic)
            cell?.profileImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultFace"), options: nil, progressBlock: nil, completionHandler: nil)
        }else if let instrPic = message.instructorProfilePic{
                let url = URL(string: instrPic)
                cell?.profileImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultFace"), options: nil, progressBlock: nil, completionHandler: nil)
        }else{
            cell?.profileImageView.image = #imageLiteral(resourceName: "defaultFace")
        }
        
        
        cell?.selectionStyle = .none
        return cell!
    }

    @IBAction func cancelClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension InstructorMessagesViewController{
    func fetchMessages(){
        self.showProgress()
        NetworkInterface.fetchJSON(.instructorMessageList, params: ["RequestId":"\(requestId ?? 0)"]) { (success, data, response, error) -> (Void) in
            
            if let data = data as? [String:Any], let messagesArray = data["Messages"] as? [[String:Any]] {
                self.messages.removeAll()
                messagesArray.forEach({ (messageDict) in
                    self.messages.append(InstructorMessage.init(fromDictionary: messageDict as NSDictionary))
                })
                
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.hideProgress()
            }
            
            
        }
    }
    
    func fetchMessages(_ requestType:IRequestType,headers:NSDictionary? = [:]){
        self.showProgress()
        NetworkInterface.fetchJSON(requestType, params: headers) { (success, data, response, error) -> (Void) in
            
            if let data = data as? [String:Any], let messagesArray = data["Messages"] as? [[String:Any]] {
                self.messages.removeAll()
                messagesArray.forEach({ (messageDict) in
                    self.messages.append(InstructorMessage.init(fromDictionary: messageDict as NSDictionary))
                })
                
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.hideProgress()
            }
            
            
        }
    }
}











