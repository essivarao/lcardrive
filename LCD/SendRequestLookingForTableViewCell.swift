//
//  SendRequestLookingForTableViewCell.swift
//  LCD
//
//  Created by Admin on 08/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class SendRequestLookingForTableViewCell: UITableViewCell {


    @IBOutlet var selectedImageView: UIImageView!
    @IBOutlet var backGroundColorView: UIView!
    @IBOutlet var fullTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        backGroundColorView.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
