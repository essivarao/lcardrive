//
//  MasterDetailsManager.swift
//  LCD
//
//  Created by Narendra Kumar R on 5/13/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import Foundation

class MasterDetalsManager {
    private static let kMasterDetails = "kMasterInfo"
    private static let kConfigDetails = "kCongigInfo"
    private static let defaults = UserDefaults.standard
    static func saveMasterDetails(array : NSArray) {
        let namesArrayData = NSKeyedArchiver.archivedData(withRootObject: array)

        defaults.set(namesArrayData, forKey: kMasterDetails)
        defaults.synchronize()
    }
    
    static func masterDetails()-> [[String:Any]]? {
        let array = UserDefaults.standard.value(forKey: kMasterDetails) as? NSData
        if let retriveArrayData = array{
            return NSKeyedUnarchiver.unarchiveObject(with: retriveArrayData as Data) as? [[String:Any]]
        }
        return nil
    }
    
    static func saveConfig(array : [[String:Any]]) {
        let namesArrayData = NSKeyedArchiver.archivedData(withRootObject: array)
        defaults.set(namesArrayData, forKey: kConfigDetails)
        defaults.synchronize()
    }
    
    static func config()-> [[String:Any]]? {
        let array = UserDefaults.standard.value(forKey: kConfigDetails) as? NSData
        if let retriveArrayData = array{
            return NSKeyedUnarchiver.unarchiveObject(with: retriveArrayData as Data) as? [[String:Any]]
        }
        return nil
    }
}
