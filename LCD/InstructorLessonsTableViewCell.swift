//
//  InstructorLessonsTableViewCell.swift
//  LCD
//
//  Created by Admin on 15/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class InstructorLessonsTableViewCell: UITableViewCell {

    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var rateLabel: UILabel!
    @IBOutlet var timeAndAddressLabel: UILabel!
    @IBOutlet var viewMessageButton: UIButton!
    @IBOutlet var sendmessageButton: UIButton!
    @IBOutlet var updateButton: UIButton!
    @IBOutlet var completeButton: UIButton!
    @IBOutlet var updateButtonView: UIStackView!
    @IBOutlet var confirmButton: UIButton!
    @IBOutlet var rejectButton: UIButton!
    @IBOutlet var confirmButtonView: UIStackView!
    @IBOutlet var callButton: UIButton!
    @IBOutlet weak var rejectedLabel: UILabel!
    
    var lesson: InstrctorBookLessonHistory?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.profileImageView.layer.cornerRadius = 35
        self.profileImageView.layer.masksToBounds = true
        
        
        let imageView = UIImageView()
        imageView.frame = CGRect(x: 10 , y: 7, width: 18, height: 18)
        imageView.image = #imageLiteral(resourceName: "call_plane")
        callButton.addSubview(imageView)
        callButton.titleEdgeInsets = UIEdgeInsetsMake(0, 20 , 0, 0)
        
        let imageView1 = UIImageView()
        imageView1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imageView1.image = #imageLiteral(resourceName: "view_messages")
        viewMessageButton.addSubview(imageView1)
        viewMessageButton.titleEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        
        let sendMessageView = UIImageView()
        sendMessageView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        sendMessageView.image = #imageLiteral(resourceName: "send_messages")
        sendmessageButton.addSubview(sendMessageView)
        sendmessageButton.titleEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        
        
        completeButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        completeButton.layer.borderColor = UIColor.lightGray.cgColor
        completeButton.layer.borderWidth = 2
        
        confirmButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        confirmButton.layer.borderColor = UIColor.lightGray.cgColor
        confirmButton.layer.borderWidth = 2
        
        rejectButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        rejectButton.layer.borderColor = UIColor.lightGray.cgColor
        rejectButton.layer.borderWidth = 2
        
        updateButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        updateButton.layer.borderColor = UIColor.lightGray.cgColor
        updateButton.layer.borderWidth = 2
        
        confirmButtonView.isHidden = true
        rejectedLabel.isHidden = true
        
    }
    
    func populateData(){
        nameLabel.text = lesson?.learnerName ?? ""
        descriptionLabel.text = lesson?.lessonType ?? ""
        
        rateLabel.text = "\(lesson?.rate ?? 0) /\((lesson?.package?.hour == 1 || lesson?.package?.hour == 0 || lesson?.package?.hour == nil) ? "Hour" : "\(lesson?.package?.hour ?? 0)Hours")"
        timeAndAddressLabel.text = "\(lesson?.lessonDate?.formatterDate ?? "") @ \(lesson?.suburb ?? "")"
        if let imageURL = lesson?.learnerProfilePic {
            let url = URL(string: imageURL)
            profileImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultFace"), options: nil, progressBlock: nil, completionHandler: nil)
        }else{
            profileImageView.image = #imageLiteral(resourceName: "defaultFace")
        }
        
        
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func callButtonClicked(_ sender: Any) {
    }
}
