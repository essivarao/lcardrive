//
//  ChangePassword.swift
//  DrivingAUS
//
//  Created by Naresh Babu Katta on 2/16/17.
//  Copyright © 2017 Sarella. All rights reserved.
//

import Foundation
import MBProgressHUD

class ChangePassword: UIViewController {
    
    @IBOutlet weak var currentPasswordTxtFld: UITextField!
    
    @IBOutlet weak var newPasswordTxtFld: UITextField!
    
    @IBOutlet weak var confirmPasswordTxtFld: UITextField!
    
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var submitButton: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewSetUp()
    }
    
//    func validateInputFields() {
//        guard let currentPwd = currentPasswordTxtFld.text,
//                let newPwd = newPasswordTxtFld.text,
//                let confirmPwd = confirmPasswordTxtFld.text else {
//            print("Form is not valid")
//            return
//        }
//        
//        /*
//         
//         {
//         "UserId": 1,
//         "OldPassword": "sample string 2",
//         "NewPassword": "sample string 3"
//         }
// */
//
//        
//        
//    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
   
    @IBAction func submitButtonClicked(_ sender: Any) {
   
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        

        
        if (!isValid(password: currentPasswordTxtFld.text!)) {
            appdelegate.window?.makeToast("Please enter current password", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
            
        else if (!isValid(password: newPasswordTxtFld.text!)) {
            appdelegate.window?.makeToast("Please enter new password", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
            
        else if (!isValid(password: confirmPasswordTxtFld.text!)) {
            appdelegate.window?.makeToast("Please enter confirm password", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        else if (newPasswordTxtFld.text != confirmPasswordTxtFld.text) {
            appdelegate.window?.makeToast("Password doesn't match with new password", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
            
        else if (currentPasswordTxtFld.text == confirmPasswordTxtFld.text) {
            appdelegate.window?.makeToast("New password cannot be same as old password", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
            
        else{
            let payload = [
                "UserId": Instructor.shared.userID ?? "",
                "OldPassword": currentPasswordTxtFld.text!,
                "NewPassword": newPasswordTxtFld.text!
                ] as [String : Any]
            
            self.showProgress()
            NetworkInterface.fetchJSON(.changePassword, payload: payload) { (success, data, response, error) -> (Void) in
                DispatchQueue.main.async {
                    self.hideProgress()
                    if data?["StatusCode"] as? Int == 0, let message = data?["ServiceMessage"] as? String {
                        self.dismiss(animated: true, completion: { 
//                            self.alert(title: "Success", message: message)
                            appdelegate.window?.makeToast( message, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)

                        })
                    }else if let message = data?["ErrorMessage"] as? String {
//                        self.alert(title: "Error", message: message)
                        appdelegate.window?.makeToast(message, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)

                    }else{
//                        self.alert(title: "Error", message: "Please try again.")
                        appdelegate.window?.makeToast("Please try again.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)

                    }
                }
            }
            
        }
    }
    
    
    func displayAlert(_ title:String,message:String)  {
        let alert = UIAlertController(title: title , message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) -> Void in
            NSLog("button OK")
        }
        
        alert.addAction(defaultAction)
        present(alert, animated: true, completion:nil)  // 11
    }
    
}
extension ChangePassword{
    
    func viewSetUp(){
        submitButton.titleLabel?.font = UIFont(name: FontName.REGULAR, size:CGFloat(FontSize.BUTTON_MEDIUM) )
        submitButton.backgroundColor = UIColorFromRGB(rgbValue: Color.APP_THEMECOLOR)

        cancelButton.titleLabel?.font = UIFont(name: FontName.REGULAR, size:CGFloat(FontSize.BUTTON_MEDIUM) )
        cancelButton.backgroundColor = UIColorFromRGB(rgbValue: Color.GRAY_COLOR)
        
    }
    
}
