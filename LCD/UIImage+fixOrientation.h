//
//  UIImage+fixOrientation.h
//  BusinessCardVerification
//
//  Created by Shravan on 9/15/16.
//  Copyright © 2016 Nadia Ye. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)
- (UIImage *)fixOrientation;

@end
