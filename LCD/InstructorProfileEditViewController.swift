//
//  InstructorProfileEditViewController.swift
//  LCD
//
//  Created by Admin on 05/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit
import Kingfisher
import GooglePlaces

class InstructorProfileEditViewController: UIViewController ,UIGestureRecognizerDelegate , UICollectionViewDataSource , UICollectionViewDelegate, UITextFieldDelegate {

    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var firstNAmeTextField: UITextField!
    @IBOutlet var lastNameTextField: UITextField!
    @IBOutlet var emailIdTextField: UITextField!
    @IBOutlet var phoneNumberTextField: UITextField!
    @IBOutlet var ratePerHourTextField: UITextField!
    @IBOutlet var schoolNameTextField: UITextField!
    @IBOutlet var addressTextView: UITextView!
    @IBOutlet var aboutMeTextView: UITextView!
    @IBOutlet var availableLocationsView: UIView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var addressview: UIView!
    @IBOutlet var aboutMeView: UIView!
    @IBOutlet var experienceTextField: UITextField!
    
    @IBOutlet var carTypeSegmentedControl: UISegmentedControl!
    @IBOutlet var availableLocationViewHeight: NSLayoutConstraint!
    
    @IBOutlet var gearTypeSegmentedControl: UISegmentedControl!
    @IBOutlet var transmissionBothImageView: UIImageView!
    @IBOutlet var transmissionManualImageView: UIImageView!
    @IBOutlet var selectedAutomaticImageView: UIImageView!
    @IBOutlet var licencedSelectedImageView: UIImageView!
    @IBOutlet var femaleSelectedImageView: UIImageView!
    @IBOutlet var maleSelectedImageView: UIImageView!
    @IBOutlet var addlocationButton: UIButton!
    @IBOutlet var maleView: UIView!
    @IBOutlet var femaleView: UIView!
    @IBOutlet var licencedView: UIView!
    @IBOutlet var manualView: UIView!
    @IBOutlet var bothView: UIView!
    @IBOutlet var automaticView: UIView!
    @IBOutlet var collectionView: UICollectionView!
    
    
    var cellId = "Cell"

    
    var locations: [InstructorSuburbs] = []
    var drivingScoolImages: [DrivingSchoolImages] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        
        profileImageView.layer.cornerRadius = 40
        profileImageView.layer.masksToBounds = true
        ratePerHourTextField.delegate = self
        experienceTextField.delegate = self
        
        addlocationButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        
        self.registerForKeyboardNotifications()
        self.scrollView.keyboardDismissMode = .interactive
      
        // Do any additional setup after loading the view.
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self
        maleView.addGestureRecognizer(tap)
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(handleTap1))
        tap1.delegate = self
        femaleView.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(handleTap2))
        tap2.delegate = self
        licencedView.addGestureRecognizer(tap2)
        
//        let tap3 = UITapGestureRecognizer(target: self, action: #selector(handleTap3))
//        tap3.delegate = self
//        automaticView.addGestureRecognizer(tap3)
//        
//        
//        let tap4 = UITapGestureRecognizer(target: self, action: #selector(handleTap4))
//        tap4.delegate = self
//        manualView.addGestureRecognizer(tap4)
//        
//        
//        let tap5 = UITapGestureRecognizer(target: self, action: #selector(handleTap5))
//        tap5.delegate = self
//        bothView.addGestureRecognizer(tap5)
        
        
        profileImageView.isUserInteractionEnabled = true
        let tap6 = UITapGestureRecognizer(target: self, action: #selector(addimageClicked))
        tap6.delegate = self
        profileImageView.addGestureRecognizer(tap6)
        if let images = Instructor.shared.drivingSchoolImages {
            drivingScoolImages.append(contentsOf: images)

        }
        ratePerHourTextField.keyboardType = .numberPad
        phoneNumberTextField.keyboardType = .numberPad
        experienceTextField.keyboardType = .numberPad
        maleSelectedImageView.image = #imageLiteral(resourceName: "ic_radio_btn_unselect")

        collectionView.register(ImageCollectionViewCell.self, forCellWithReuseIdentifier: cellId)

        
        populateData()
        viewSetting()
    }

    func viewSetting(){
        
        let cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        firstNAmeTextField.layer.cornerRadius = CGFloat(cornerRadius)
        lastNameTextField.layer.cornerRadius = CGFloat(cornerRadius)
        emailIdTextField.layer.cornerRadius = CGFloat(cornerRadius)
        phoneNumberTextField.layer.cornerRadius = CGFloat(cornerRadius)
        ratePerHourTextField.layer.cornerRadius = CGFloat(cornerRadius)
        schoolNameTextField.layer.cornerRadius = CGFloat(cornerRadius)
        addressview.layer.cornerRadius = CGFloat(cornerRadius)
        aboutMeView.layer.cornerRadius = CGFloat(cornerRadius)
        experienceTextField.layer.cornerRadius = CGFloat(cornerRadius)
        

        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 40, height: 30))
        emailIdTextField.leftView = paddingView
        emailIdTextField.leftViewMode = UITextFieldViewMode.always
       
        let paddingView1 = UIView(frame:CGRect(x: 0, y: 0, width: 40, height: 30))
        firstNAmeTextField.leftView = paddingView1
        firstNAmeTextField.leftViewMode = UITextFieldViewMode.always
        
        let paddingView2 = UIView(frame:CGRect(x: 0, y: 0, width: 10, height: 30))
        lastNameTextField.leftView = paddingView2
        lastNameTextField.leftViewMode = UITextFieldViewMode.always
        
        let paddingView3 = UIView(frame:CGRect(x: 0, y: 0, width: 70, height: 30))
        phoneNumberTextField.leftView = paddingView3
        phoneNumberTextField.leftViewMode = UITextFieldViewMode.always
        
        let paddingView4 = UIView(frame:CGRect(x: 0, y: 0, width: 40, height: 30))
        ratePerHourTextField.leftView = paddingView4
        ratePerHourTextField.leftViewMode = UITextFieldViewMode.always
        
        let paddingView5 = UIView(frame:CGRect(x: 0, y: 0, width: 40, height: 30))
        schoolNameTextField.leftView = paddingView5
        schoolNameTextField.leftViewMode = UITextFieldViewMode.always
        
        let paddingView6 = UIView(frame:CGRect(x: 0, y: 0, width: 40, height: 30))
        experienceTextField.leftView = paddingView6
        experienceTextField.leftViewMode = UITextFieldViewMode.always
    }
    
    func populateData(){
        let user = Instructor.shared
        self.firstNAmeTextField.text = Instructor.shared.firstName ?? ""
        self.lastNameTextField.text = Instructor.shared.lastName ?? ""
        self.emailIdTextField.text = user.email ?? ""
        self.phoneNumberTextField.text = user.phoneNumber ?? ""
        self.schoolNameTextField.text = user.schoolName ?? ""
        self.addressTextView.text = user.address ?? ""
        self.aboutMeTextView.text = user.aboutMe ?? ""
        
        if user.gender?.lowercased() == "f" {
            handleTap1()
        }else if user.gender?.lowercased() == "m"{
            handleTap()
        }
        
        if user.isLicensed == true{
            licencedSelectedImageView.image = #imageLiteral(resourceName: "Checked Checkbox-48")
        }else{
            licencedSelectedImageView.image = #imageLiteral(resourceName: "Unchecked Checkbox-48")

        }
        gearTypeSegmentedControl.selectedSegmentIndex = UISegmentedControlNoSegment

        if user.transmissionType?.lowercased() == "automatic" {
            gearTypeSegmentedControl.selectedSegmentIndex = 0
        }else if user.transmissionType?.lowercased() == "manual" {
            gearTypeSegmentedControl.selectedSegmentIndex = 1
        }else if user.transmissionType?.lowercased() == "both" {
            gearTypeSegmentedControl.selectedSegmentIndex = 2
        }
        carTypeSegmentedControl.selectedSegmentIndex = UISegmentedControlNoSegment

        if user.carBodyTypeId == 5 {
            carTypeSegmentedControl.selectedSegmentIndex = 1
        }else if user.carBodyTypeId == 4 {
            carTypeSegmentedControl.selectedSegmentIndex = 0
        }else if user.carBodyTypeId == 3{
            carTypeSegmentedControl.selectedSegmentIndex = 2
        }
        if let exp = user.experience {
            experienceTextField.text = "\(exp)"

        }
        
        
        if user.instructorSuburbs != nil {
            locations.append(contentsOf: user.instructorSuburbs!)
        }
        
        if let imageURL = user.profilePicImageUrl,
            let url = URL(string: imageURL) {
//            profileImageView.kf.indicatorType = .activity
            profileImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultFace"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        if let existedRatePackage = Instructor.shared.instructorPackages?.filter({$0.hour == 1}).first {
            ratePerHourTextField.text = "\(existedRatePackage.rate!)"
        }
        applyLocationsData()
        collectionView.reloadData()

    }
    
    
    func applyLocationsData(){
//        locations.removeAll()
        locationsAvailbeView()

    }
    
    
    
    func updateUserProfile(){
        
        let user = Instructor.shared
        let data = UIImageJPEGRepresentation(profileImageView.image!, 0.5)
        var gender: String!
        
        var cardBodyTypeId: Int!
        var cardBodyTypeString: String!
        if carTypeSegmentedControl.selectedSegmentIndex == 0 {
            cardBodyTypeId =  4
            cardBodyTypeString = "Hatchback"
        }else if carTypeSegmentedControl.selectedSegmentIndex == 1 {
            cardBodyTypeId =  5
            cardBodyTypeString = "Sedan"
        }else if carTypeSegmentedControl.selectedSegmentIndex == 2 {
            cardBodyTypeId =  3
            cardBodyTypeString = "SUV"
        }
        
        var isLicenced: Bool = false
        if licencedSelectedImageView.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            isLicenced = true
        }
        
        
        if maleSelectedImageView.image == #imageLiteral(resourceName: "ic_radio_btn_select"){
            gender = "m"
        }else if femaleSelectedImageView.image == #imageLiteral(resourceName: "ic_radio_btn_select") {
            gender = "f"
        }else{
            //Error
            gender = "m"
        }
        
        var transmitionId: Int!
        var transmissionType: String!
        
        if gearTypeSegmentedControl.selectedSegmentIndex == 0 {
            transmitionId = 6
            transmissionType = "Automatic"

        }else if gearTypeSegmentedControl.selectedSegmentIndex == 1 {
            transmitionId = 7
            transmissionType = "Manual"
        }else if gearTypeSegmentedControl.selectedSegmentIndex == 2{
            transmitionId = 8
            transmissionType = "Both"
        }
        
        var locationsPayload:[[String:Any]] = []
        locations.forEach { (location) in
            var locationDict = [
                
                "Suburb": location.suburb ?? "",
                "PostalCode": location.postalCode ?? "0",
                "Latitude": location.latitude ?? 0.0,
                "Longitude": location.longitude ?? 0.0,
                "IsActive": location.isActive ?? true
                ] as [String : Any]
            if location.suburbId != nil {
                locationDict["SuburbId"] = location.suburbId!
            }
            locationsPayload.append(locationDict)
        }
        
        var drivingScoolPayload:[[String:Any]] = []
        drivingScoolImages.forEach{ (image) in
            let driveDict = [
                "ImageId": image.imageId!,
                "IsActive": image.isActive ?? false
            ] as [String : Any]
            drivingScoolPayload.append(driveDict)
        }
        
        var package = [
            "UserId": Instructor.shared.userID!,
            "Rate": Int(ratePerHourTextField.text ?? "0")!,
            "Hour": 1,
            "IsActive": true,
        ] as [String : Any]

        
        
        let existedRatePackage = Instructor.shared.instructorPackages?.filter({$0.hour == 1}).first
        if existedRatePackage != nil {
            package["PackageId"] = existedRatePackage?.packageId!
        }
        
        var payload:[String:Any] = [:]
        if let profileid = user.profileId {
            payload["ProfileId"] = profileid
        }

        payload["UserId"] = user.userID!
        payload["FirstName"] =  firstNAmeTextField.text!
        payload["LastName"] =  lastNameTextField.text!
        payload["PhoneNumber"] = phoneNumberTextField.text!
        payload["Email"] =  emailIdTextField.text!
        payload["ProfilePicImage"] =  data!.base64EncodedString()
        payload["SchoolName"] = schoolNameTextField.text!
        payload["Address"] = addressTextView.text!
        payload["AboutMe"] = aboutMeTextView.text!
        payload["Gender"] = gender
        payload["CarBodyTypeId"] = cardBodyTypeId
        payload["TransmissionTypeId"] = transmitionId
        payload["IsLicensed"] = isLicenced
        payload["IsActive"] = true
        payload["TransmissionType"] = transmissionType
        payload["CarBodyType"] = cardBodyTypeString
        payload["InstructorSuburbs"] = locationsPayload
        payload["DrivingSchoolImages"] = drivingScoolPayload
        payload["InstructorPackages"] = [package]
        payload["Experience"] = Int(experienceTextField.text ?? "0")
        self.showProgress()
        NetworkInterface.fetchJSON(.updateInstructorProfile, payload: payload) { (success, data, response, error) -> (Void) in
            self.hideProgress()
            if data?["StatusCode"] as? Int == 0, let message = data?["ServiceMessage"] as? String {
                
                Instructor.shared.isInstructorEdit = false
                Instructor.shared.synchronize()
                DispatchQueue.main.async {
                    AppDelegate.shared.window?.makeToast(message, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    self.dismiss(animated: true, completion: nil)
                }
            }else if let message = data?["ErrorMessage"] as? String{
                DispatchQueue.main.async {
                    AppDelegate.shared.window?.makeToast(message, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                }
            }
        }
    }


    func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        maleSelectedImageView.image = #imageLiteral(resourceName: "ic_radio_btn_select")
        femaleSelectedImageView.image = #imageLiteral(resourceName: "ic_radio_btn_unselect")
    }
    
    func handleTap1(sender: UITapGestureRecognizer? = nil) {
     
        femaleSelectedImageView.image = #imageLiteral(resourceName: "ic_radio_btn_select")
        maleSelectedImageView.image = #imageLiteral(resourceName: "ic_radio_btn_unselect")
        // handling code
    }
    
    func handleTap2(sender: UITapGestureRecognizer? = nil) {
        // handling code
       
        if licencedSelectedImageView.image == #imageLiteral(resourceName: "Checked Checkbox-48"){
               licencedSelectedImageView.image = #imageLiteral(resourceName: "Unchecked Checkbox-48")
        }
        else{
        licencedSelectedImageView.image = #imageLiteral(resourceName: "Checked Checkbox-48")
        }
        
    }

    
    
    func addimageClicked(sender: UITapGestureRecognizer? = nil) {
        let picker = CustomImagePickerViewController()
        picker.delegate = self
        picker.from = .profileImage
        present(picker, animated: true, completion: nil)

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func loadView() {
        super.loadView()
    }
    
    func locationsAvailbeView()  {
        availableLocationsView.backgroundColor = UIColor.clear
        
        availableLocationsView.subviews.forEach { (subview) in
            if subview as? LocationButton != nil {
                subview.removeFromSuperview()
            }
        }

        var indexOfLeftmostButtonOnCurrentLine = 0
        var buttons = [Any]()
        var runningWidth = 0.0
        let maxWidth = Double(self.view.frame.size.width - 40)
        let horizontalSpaceBetweenButtons = 10.0
        let verticalSpaceBetweenButtons = 10.0
        
        var locationViewHeight = 0.0
        
        let visibleLocations = locations.filter({$0.isActive == true})
        
        for index in 0..<visibleLocations.count {
            let label = LocationButton()
            label.suburb = visibleLocations[index]
            label.addTarget(self, action: #selector(InstructorProfileEditViewController.didDeleteLocation(sender:)), for: .touchUpInside)
            label.sizeToFit()
            label.backgroundColor = UIColor.lightGray
            label.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)

            label.titleLabel?.font = UIFont (name: "Montserrat-Light", size: 14)
            let title = " " + (visibleLocations[index].suburb ?? "") + "   X "
            label.setTitle(title, for: .normal)
            label.translatesAutoresizingMaskIntoConstraints = false
            
            availableLocationsView.addSubview(label)
            // check if first button or button would exceed maxWidth
            if ((index == 0) ||  (runningWidth + Double(label.intrinsicContentSize.width) > maxWidth)) {
                // wrap around into next line
                runningWidth = Double(label.intrinsicContentSize.width)
                
                if (index == 0) {
                    locationViewHeight = 40
                    let horizontalConstraint = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: availableLocationsView, attribute: .left, multiplier: 1.0, constant: 5)
                    availableLocationsView.addConstraint(horizontalConstraint)
                    
                    
                    // vertical position:
                    let verticalConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: availableLocationsView, attribute: .top, multiplier: 1.0, constant: 40.0)
                    availableLocationsView.addConstraint(verticalConstraint)
                    
                    
                } else {
                    // put it in new line
                    
                    locationViewHeight = locationViewHeight + 40
                    
                    let previousLeftmostButton = buttons[indexOfLeftmostButtonOnCurrentLine] as? UIButton
                    // horizontal position: same as previous leftmost button (on line above)
                    let horizontalConstraint = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: previousLeftmostButton, attribute: .left, multiplier: 1.0, constant: 0.0)
                    availableLocationsView.addConstraint(horizontalConstraint)
                    // vertical position:
                    let verticalConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: previousLeftmostButton, attribute: .bottom, multiplier: 1.0, constant: CGFloat(verticalSpaceBetweenButtons))
                    availableLocationsView.addConstraint(verticalConstraint)
                    indexOfLeftmostButtonOnCurrentLine = index
                }
            } else {
                
                // put it right from previous buttom
                runningWidth += Double(label.intrinsicContentSize.width) + horizontalSpaceBetweenButtons
                let previousButton = buttons[(index - 1)] as? UIButton
                // horizontal position: right from previous button
                let horizontalConstraint = NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: previousButton, attribute: .right, multiplier: 1.0, constant: CGFloat(horizontalSpaceBetweenButtons))
                availableLocationsView.addConstraint(horizontalConstraint)
                // vertical position same as previous button
                let verticalConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: previousButton, attribute: .top, multiplier: 1.0, constant: 0.0)
                availableLocationsView.addConstraint(verticalConstraint)
            }
            
            buttons.append(label)
        }
        
        availableLocationViewHeight.constant = CGFloat(locationViewHeight) + 30
        
    }
    
    func didDeleteLocation(sender: LocationButton){
        let suburb = sender.suburb
        suburb?.isActive = false
        applyLocationsData()
    }


    @IBAction func locationManageButtonClicked(_ sender: Any) {
    }
    @IBAction func addLocationButtonClicked(_ sender: Any) {
        
        if let count = AppDelegate.shared.configValues.filter({$0.configKey == "MaxLocations"}).first?.configValue,let countValue = Int(count), locations.count >= countValue {
            AppDelegate.shared.window?.makeToast("Yoc can add maximum of \(count) suburbs", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        
        let googleAutoVC = GMSAutocompleteViewController.init()
        
        googleAutoVC.delegate = self
        googleAutoVC.view.backgroundColor = UIColor.clear
        let filter = GMSAutocompleteFilter.init()
        filter.country = "AU"
        filter.type = .region
        googleAutoVC.autocompleteFilter = filter
        
        self.present(googleAutoVC, animated: true) {
            
        }
    }
    @IBAction func segmentedButtonChanged(_ sender: Any) {
    }
    
    @IBAction func saveButtonClicked(_ sender: Any) {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        
        let activeSuburbs = locations.filter({$0.isActive == true})

        if (!isValid(firstName: firstNAmeTextField.text!)) {
            appdelegate.window?.makeToast("Please enter first name", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
        }
        else if (!isValid(lastName: lastNameTextField.text!)){
            appdelegate.window?.makeToast("Please enter last name", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
        }
        else if (!isValid(email: emailIdTextField.text!)){
            appdelegate.window?.makeToast("Please enter email number", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
        }
        else if (!isValid(phoneNumber: phoneNumberTextField.text!)){
            appdelegate.window?.makeToast("Please enter phone number", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
        }
//        else if (!isValid(firstName: schoolNameTextField.text!)) {
//            appdelegate.window?.makeToast("Please enter school name", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
//
//        }
        else if (!isValid(address: addressTextView.text!)) {
            appdelegate.window?.makeToast("Please enter address", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)

        }
        else if (!isValid(aboutMe: aboutMeTextView.text!)) {
            appdelegate.window?.makeToast("Please enter about me ", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
        }
        else if (aboutMeTextView.text.characters.count >= 251 ) {
            appdelegate.window?.makeToast("About me cannot be more than 250 characters", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            
        }
        else if (experienceTextField.text?.characters.count)! == 0 {
            appdelegate.window?.makeToast("Please enter experience", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
        }else if (ratePerHourTextField.text?.characters.count)! == 0 {
            appdelegate.window?.makeToast("Please enter rate per hour", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
        }else if activeSuburbs.count == 0 {
            appdelegate.window?.makeToast("Please select atleast one suburb", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
        }
        else if maleSelectedImageView.image == #imageLiteral(resourceName: "ic_radio_btn_unselect") &&  femaleSelectedImageView.image == #imageLiteral(resourceName: "ic_radio_btn_unselect"){
            appdelegate.window?.makeToast("Please select gender", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
        }
        else if carTypeSegmentedControl.selectedSegmentIndex == UISegmentedControlNoSegment {
            appdelegate.window?.makeToast("Please select car type", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)

        }
        else if gearTypeSegmentedControl.selectedSegmentIndex == UISegmentedControlNoSegment {
            appdelegate.window?.makeToast("Please select gear type", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)

        }
        else{

            self.updateUserProfile()
        }
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        if Instructor.shared.isInstructorEdit == false {
            
            locations.forEach({ (suburb) in
                suburb.isActive = true
            })
            
            self.dismiss(animated: true, completion: nil)
        }else{
            Instructor.shared.logout()
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.setupRootForNoSession()
        }
    
   
    }
    // Mark: Manage content when keyboard pops up
    var activeField: UITextField?
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(InstructorProfileEditViewController.keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(InstructorProfileEditViewController.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWasShown(_ notification: Notification) {
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil {
            if (!aRect.contains(activeField!.frame.origin))
            {
                self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
            }
        }
    }
    
    func keyboardWillBeHidden(_ notification: Notification) {
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0,-keyboardSize!.height+keyboardSize!.height, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard textField === ratePerHourTextField || textField === experienceTextField, let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        if experienceTextField === textField {
            return newLength <= 2
        }else if textField === ratePerHourTextField  {
            return newLength <= 5
        }
        return true

    }
    
    //Collection view
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if visibleCarImages.count == carImagesMaxLimit {
            return visibleCarImages.count
        }else{
            return visibleCarImages.count + 1
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ImageCollectionViewCell
        if indexPath.row == 0 && visibleCarImages.count < carImagesMaxLimit {
           cell.schoolImageView.image = UIImage.init(named: "add_icon_large")
        }else{
            
            let imageObj = visibleCarImages[visibleCarImages.count < carImagesMaxLimit ? indexPath.row - 1 : indexPath.row]
            if let url = imageObj.imageUrl {
                let url = URL(string: url)
                let processor = RoundCornerImageProcessor(cornerRadius: 25)
                cell.schoolImageView.kf.indicatorType = .activity
                cell.schoolImageView.kf.setImage(with: url, placeholder: nil, options: [.processor(processor)], progressBlock: nil, completionHandler: nil)
            }
            
        }
//
//        cell.addSubview(imageView)
        
        return cell
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 50, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 && visibleCarImages.count < carImagesMaxLimit {
           let picker = CustomImagePickerViewController()
            picker.delegate = self
            picker.from = .carImaes
            picker.userInfo = indexPath
            present(picker, animated: true, completion: nil)

        }
        else if indexPath.row != 0 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "FullViewImageViewController") as! FullViewImageViewController
            controller.arrayOfImages = visibleCarImages
            if visibleCarImages.count < carImagesMaxLimit {
                controller.image = visibleCarImages[indexPath.row-1]
            }else{
                controller.image = visibleCarImages[indexPath.row]
            }
            controller.delegate = self

            self.present(controller, animated: true, completion: nil)
            
        }
    }
    var visibleCarImages:[DrivingSchoolImages]{
        return drivingScoolImages.filter({$0.isActive == true})
    }
}

extension InstructorProfileEditViewController:FullImageDelegate{
    func didDeleteImage() {
        collectionView.reloadData()
    }
    var carImagesMaxLimit:Int {
        return Int(AppDelegate.shared.configValues.filter({$0.configKey == "MaxCarImagesCount"}).first?.configValue ?? "0") ?? 0
    }
}

extension InstructorProfileEditViewController:UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: CustomImagePickerViewController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if picker.from == .profileImage {
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            self.profileImageView.image = image.fixOrientation()

        }else{
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            saveCarImage(image: image.fixOrientation())
        }
        dismiss(animated: true, completion: nil)
    }
}

extension InstructorProfileEditViewController{
    func saveCarImage(image: UIImage){
        
        if let imageData = UIImageJPEGRepresentation(image, 0.5) {
            let payload = ["UserId": Instructor.shared.userID!, "ImageName": "CarImage_\(Int(Date().timeIntervalSince1970)).jpeg", "Image": imageData.base64EncodedString()] as [String : Any]
            self.showProgress()
            NetworkInterface.fetchJSON(.uploadCarImage, payload: payload) { (success, data, response, error) -> (Void) in
                if let data = data as? [String:Any], let url = data["ImageUrl"] as? String {
                    let image = DrivingSchoolImages()
                    image.imageUrl = url
                    image.imageId = data["ImageId"] as? Int
                    image.imageName = data["ImageName"] as? String
                    image.isActive = true
                    self.drivingScoolImages.append(image)
                }
                DispatchQueue.main.async {
                    self.hideProgress()
                    self.collectionView.reloadData()
                }
                
            }
        }
    }
}


extension InstructorProfileEditViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {

        viewController.dismiss(animated: true, completion: nil)

        let suburb  = InstructorSuburbs()
        suburb.latitude = "\(place.coordinate.latitude)"
        suburb.longitude = "\(place.coordinate.longitude)"
        
        let postalCodeComponent =  place.addressComponents?.filter({$0.type == "postal_code"}).first
//        if let postalCodeComponent =  place.addressComponents?.filter({$0.type == "locality"}).first {
//            suburb.suburb = postalCodeComponent.name
//        }
        let suburbString = place.formattedAddress?.replacingOccurrences(of: ", Australia", with: "")
        suburb.suburb = suburbString
        suburb.createdBy = Instructor.shared.userID!
        suburb.isActive = true
        if postalCodeComponent != nil {
            suburb.postalCode = (postalCodeComponent?.name)
        }
        
        if locations.filter({$0.suburb == suburbString}).count > 0 {
            AppDelegate.shared.window?.makeToast("Location already added with same name.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        
        
        locations.append(suburb)
        applyLocationsData()
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error.localizedDescription)")
        viewController.dismiss(animated: true, completion: nil)

    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        viewController.dismiss(animated: true, completion: nil)
    }
    
}







