

import Foundation


import Foundation

enum IRequestType {

    case instructorProfile
    case login
    case instructorRequestsList
    case instructosSearchlist
    case regiseterInstructor
    case changePassword
    case forgetPassword
    case facebookRegistration
    case packages
    case savePackages
    case learnerUpdateProfile
    case masterProfile
    case uploadCarImage
    case updateInstructorProfile
    case sendRequest
    case lernerRequestsList
    case instructorMessageList
    case sendMessage
    case instructorPackages
    case bookLesson
    case instructorBookingHistory
    case learnerLessons
    case learnerMessages
    case learnerSendMessage
    case updateLessonStatus
    case lernerRatingsAvailability
    case checkLernerNeedRating
    case submitRating
}
struct RequestConstants {
    
    static let IURL = "http://lcddev.azurewebsites.net/api/"

}
class INetworkRequests {
    
    // GET Requests
    static func getRequestofType(_ requestType:IRequestType, headers:NSDictionary?,  params:NSDictionary?) -> URLRequest {
        var request:URLRequest!
        switch requestType {
    
        case .lernerRatingsAvailability:
            let path = "Instructor/GetInstructorProfile"
            let endpoint = RequestConstants.IURL + path
            request = self.createGETRequest(endpoint, headers: headers, params: params)
            break

            
        case .instructorProfile:
            let path = "Instructor/GetInstructorProfile"
            let endpoint = RequestConstants.IURL + path
            request = self.createGETRequest(endpoint, headers: headers, params: params)
            break
        case .instructorRequestsList:
            let path = "Request/GetInstructorRequests"
            let endpoint = RequestConstants.IURL + path
            request = self.createGETRequest(endpoint, headers: headers,params: params)
            break
            
        case .lernerRequestsList:
            //Request/GetLearnerRequests
            let path = "Request/GetLearnerRequests"
            let endpoint = RequestConstants.IURL + path
            request = self.createGETRequest(endpoint, headers: headers,params: params)
            break

        case .packages:
            let path = "Instructor/GetPackages"
            let endpoint = RequestConstants.IURL + path
            request = self.createGETRequest(endpoint, headers: headers,params: params)
            break
        case .masterProfile:
            let path = "Master/GetMasterDetails?masterTypeId=0"
            let endpoint = RequestConstants.IURL + path
            request = self.createGETRequest(endpoint, headers: headers,params: params)
            break
        case .instructorMessageList:
            let path = "Request/GetRequestMessages"
            let endpoint = RequestConstants.IURL + path
            request = self.createGETRequest(endpoint, headers: headers,params: params)
            break
        case .instructorPackages:
            let path = "Instructor/GetPackages"
            let endpoint = RequestConstants.IURL + path
            request = self.createGETRequest(endpoint, headers: headers,params: params)
            break
        case .learnerMessages:
            let path = "Lesson/GetLessonMessages"
            let endpoint = RequestConstants.IURL + path
            request = self.createGETRequest(endpoint, headers: headers,params: params)
            break
        case .checkLernerNeedRating:
            let path = "/Users/GetVerifyUserRatings"
            let endpoint = RequestConstants.IURL + path
            request = self.createGETRequest(endpoint, headers: headers,params: params)
            break
        default:
            break
        }
        
        return request
    }
    

    
    // POST Requests
    static func postRequestofType(_ requestType:IRequestType,headers:NSDictionary?, params:NSDictionary?, payload :[String:Any]? ) -> URLRequest {
        var request:URLRequest!
        switch requestType {

        case .login:
            let path = "Users/Login"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!,auth:false)
            break
            
        case .instructosSearchlist:
            let path = "Instructor/GetInstructorsByArea"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!,auth:false)
            break
            
        case .regiseterInstructor:
            let path = "Users/RegisterUser"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!)
            break
        case .changePassword:
            let path = "Users/ChangePassword"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!)
            break
        case .forgetPassword:
            let path = "Users/ForgotPassord"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!)
            break
        case .facebookRegistration:
            let path = "Users/FacebookLogin"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!)
            break
        case .savePackages:
            let path = "Instructor/AddOrupdatePackages"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!)
            break
        case .learnerUpdateProfile:
            let path = "Users/UpdateLearnerProfile"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!)
            break
        case .uploadCarImage:
            let path = "Instructor/AddOrUpdateImage"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!)
            break
        case .updateInstructorProfile:
            let path = "Instructor/InsUpdInstructorProfile"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!)
            break
        case .sendRequest:
            let path = "Request/SendRequest"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!)
            break
        case .sendMessage:
            let path = "Request/SendRequestMessage"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!)
            break
        case .learnerSendMessage:
            let path = "Lesson/SendLessonMessage"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!)
            break
        case .bookLesson:
            let path = "Lesson/InsertOrUpdateLesson"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!)
            break
        case .instructorBookingHistory:
            let path = "Lesson/GetInstructorLessons"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!)
        case .learnerLessons :
            let path = "Lesson/GetLearnerLessons"
            let endpoint = RequestConstants.IURL + path
           request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!)
            break
        case .updateLessonStatus:
            let path = "Lesson/UpdateLessonStatus"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!)
            break
        case .submitRating:
            let path = "Lesson/SubmitReview"
            let endpoint = RequestConstants.IURL + path
            request = self.createPOSTRequest(endpoint, headers: headers,params: params, payload: payload!)
            break
        default:
            break
        }
        return request
    }
    
    static func uploadRequestofType(_ requestType:IRequestType,params:NSDictionary?, headers:NSDictionary?, payload:NSDictionary?,media : Array<MPMedia> ) -> URLRequest {
        var request:URLRequest!
        switch requestType {
//        case .uploadCarImage:
//            let endpoint = "Instructor/AddOrUpdateImage"
//            let url = RequestConstants.IURL + endpoint
//            request = createMultiPartPOSTRequest(url, queryParams: params, headers: headers, payload: payload, media: media)
//            break
        default:
            break
        }
        return request
    }
    
    
    static func createGETRequest(_ baseURL:String , headers:NSDictionary?, params:NSDictionary?,auth: Bool = true) -> URLRequest {
        var headerAsString:String = ""
        if (params != nil && params!.count > 0) {
            var separator = "?"
            for (key,value) in params! {
                headerAsString += separator
                headerAsString += key as! String
                headerAsString += "="
                headerAsString += value as! String
                separator = "&"
            }
        }
        
        
        
        
        let fullUrlString = baseURL + headerAsString;
        let url = URL(string: fullUrlString)
        var request = NSMutableURLRequest(url: url!)
        
        if headers != nil {
            for (key,value) in headers! {
                request.addValue(value as! String, forHTTPHeaderField: key as! String)
            }
        }
        
        if auth{
            self.setAuthHeaders(request: &request)
        }
        request.httpMethod = "GET"
        request.timeoutInterval = 20
        request.httpShouldHandleCookies=false
        return request as URLRequest
    }
    
    static func createPOSTRequest(_ baseURL:String ,headers:NSDictionary?,params: NSDictionary?, payload:[String:Any], auth: Bool = true) -> URLRequest {
        var headerAsString:String = ""
        
        if (params != nil && params!.count > 0) {
            var separator = "?"
            for (key,value) in params! {
                headerAsString += separator
                headerAsString += key as! String
                headerAsString += "="
                headerAsString += value as! String
                separator = "&"
            }
        }
        
        
        
        let fullUrlString = baseURL + headerAsString;
        let url = URL(string: fullUrlString)
        var request = NSMutableURLRequest(url: url!)
        if headers != nil {
            for (key,value) in headers! {
                request.addValue(value as! String, forHTTPHeaderField: key as! String)
            }
        }
        
        do {
            let data = try JSONSerialization.data(withJSONObject: payload, options: [])
            let post = NSString(data: data, encoding: String.Encoding.utf8.rawValue) as! String
            request.httpBody = post.data(using: String.Encoding.utf8);
        }catch {
            print("json error: \(error)")
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        if auth{
            self.setAuthHeaders(request: &request)
        }
        
        request.httpMethod = "POST"
        request.timeoutInterval = 80
        request.httpShouldHandleCookies=false
        return request as URLRequest
    }
    
    static func createPOSTRequestWithFormData(_ baseURL:String ,headers:NSDictionary?, payload:NSDictionary?, auth: Bool = true) -> URLRequest {
        var headerAsString:String = ""
        
        if (headers != nil && headers!.count > 0) {
            var separator = "?"
            for (key,value) in headers! {
                headerAsString += separator
                headerAsString += key as! String
                headerAsString += "="
                headerAsString += value as! String
                separator = "&"
            }
        }
        
        
        let fullUrlString = baseURL + headerAsString;
        let url = URL(string: fullUrlString)
        var request = NSMutableURLRequest(url: url!)
        
        var payloadString = ""
        if (payload != nil && payload!.count > 0) {
            var separator = ""
            for (key,value) in payload! {
                payloadString += separator
                payloadString += key as! String
                payloadString += "="
                payloadString += value as! String
                separator = "&"
            }
        }
        
        request.httpBody = payloadString.data(using: String.Encoding.utf8);
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        if auth{
            self.setAuthHeaders(request: &request)
        }
        request.httpMethod = "POST"
        request.timeoutInterval = 80
        request.httpShouldHandleCookies=false
        return request as URLRequest
    }
    
    static fileprivate func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    
    static func createDELETERequest(_ baseURL:String , headers:NSDictionary?,auth: Bool = true) -> URLRequest {
        var headerAsString:String = ""
        
        if (headers != nil && headers!.count > 0) {
            var separator = "?"
            for (key,value) in headers! {
                headerAsString += separator
                headerAsString += key as! String
                headerAsString += "="
                headerAsString += value as! String
                separator = "&"
            }
            
        }
        
        let fullUrlString = baseURL + headerAsString;
        let url = URL(string: fullUrlString)
        var request = NSMutableURLRequest(url: url!)
        if auth{
            self.setAuthHeaders(request: &request)
        }
        request.httpMethod = "DELETE"
        request.timeoutInterval = 20
        request.httpShouldHandleCookies=false
        return request as URLRequest
    }
    
    static func createPUTRequest(_ baseURL:String ,headers:NSDictionary?, payload:NSDictionary?,auth: Bool = true) -> URLRequest {
        var headerAsString:String = ""
        
        if (headers != nil && headers!.count > 0) {
            var separator = "?"
            for (key,value) in headers! {
                headerAsString += separator
                headerAsString += key as! String
                headerAsString += "="
                headerAsString += value as! String
                separator = "&"
            }
        }
        
        
        let fullUrlString = baseURL + headerAsString;
        let url = URL(string: fullUrlString)
        var request = NSMutableURLRequest(url: url!)
        
        do {
            let data = try JSONSerialization.data(withJSONObject: payload!, options: [])
            let post = NSString(data: data, encoding: String.Encoding.utf8.rawValue) as! String
            request.httpBody = post.data(using: String.Encoding.utf8);
        } catch {
            print("json error: \(error)")
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        if auth{
            self.setAuthHeaders(request: &request)
        }
        request.httpMethod = "PUT"
        request.timeoutInterval = 80
        request.httpShouldHandleCookies=false
        return request as URLRequest
    }
    
    
    static func createMultiPartPOSTRequest(_ SikkaURL:String ,queryParams:NSDictionary?, headers:NSDictionary?, payload:NSDictionary?,media : Array<MPMedia>) -> URLRequest {
        var headerAsString:String = ""
        
        let boundary = generateBoundaryString()
        
        if (queryParams != nil && queryParams!.count > 0) {
            var separator = "?"
            for (key,value) in queryParams! {
                headerAsString += separator
                headerAsString += key as! String
                headerAsString += "="
                headerAsString += value as! String
                separator = "&"
            }
        }
        
        
        let fullUrlString = SikkaURL + headerAsString;
        let url = URL(string: fullUrlString)
        let request = NSMutableURLRequest(url: url!)
        
        let body = createMPBody(payload: payload, media: media , boundary: boundary)
        request.httpBody = body
       
        if let headers = headers {
            for (key,value) in headers{
                request.addValue(value as! String, forHTTPHeaderField: key as! String)

                
            }
        }
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
        
        request.httpMethod = "POST"
        request.timeoutInterval = 80
        request.httpShouldHandleCookies=false
        return request as URLRequest
    }
    
    static fileprivate func createMPBody(payload: NSDictionary?, media : Array<MPMedia>, boundary: String) -> Data {
        let body = NSMutableData();
        
        if payload != nil {
            for (key, value) in payload! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        for mp: MPMedia in media {
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(mp.fileKey!)\"; filename=\"\(mp.fileName!)\"\r\n")
            body.appendString("Content-Type: \(mp.mimeType!)\r\n\r\n")
            body.append(mp.fileData! as Data)
            body.appendString("\r\n")
            
            body.appendString("--\(boundary)--\r\n")
        }
        return body as Data
    }
    

    static func setAuthHeaders(request:inout NSMutableURLRequest){
//        if let sessionRandomIdentifier = UserDefaults.standard.value(forKey: "session-random-identifier") as? String {
//            request.addValue(sessionRandomIdentifier, forHTTPHeaderField: "Authorization")
//            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        }
    }
}

extension NSMutableData {
    
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
