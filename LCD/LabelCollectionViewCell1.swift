import UIKit

class LabelCollectionViewCell1: UICollectionViewCell
{
    var textContent: String!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var mesglabelWidthLayoutConstraint: NSLayoutConstraint! //used for setting the width via constraint

    /**
        Allows you to generate a cell without dequeueing one from a table view.
        - Returns: The cell loaded from its nib file.
    */
    class func fromNib() -> LabelCollectionViewCell1?
    {
        var cell: LabelCollectionViewCell1?
        let nibViews = Bundle.main.loadNibNamed("LabelCollectionViewCell1", owner: nil, options: nil)
        for nibView in nibViews! {
            if let cellView = nibView as? LabelCollectionViewCell1 {
                cell = cellView
            }
        }
        return cell
    }
    
    /**
        Sets the cell styles and content.
    */
    func configureWithIndexPath(indexPath: NSIndexPath)
    {
        layer.borderWidth = 1
        layer.borderColor = UIColor.black.cgColor
        backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0)
        
        messageLabel.text = textContent
        messageLabel.preferredMaxLayoutWidth = 50
    }
    
    /**
        Generate a simple label text.
        - Returns: The text for the label.
    */

}
