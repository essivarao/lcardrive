//
//  ColorExtension.swift
//  DrivingAUS
//
//  Created by Narendra Kumar on 2/19/17.
//  Copyright © 2017 Sarella. All rights reserved.
//

import UIKit


    func UIColorFromRGB(rgbValue: Int) -> UIColor {
        let iRGB = UInt(rgbValue)
        return UIColor(
            red: CGFloat((iRGB & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((iRGB & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(iRGB & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
