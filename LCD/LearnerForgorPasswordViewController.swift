//
//  LearnerForgorPasswordViewController.swift
//  LCD
//
//  Created by Admin on 02/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class LearnerForgorPasswordViewController: UIViewController {

    @IBOutlet var emailIDTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func submitClicked(_ sender: Any) {
 
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        

        
        if(!isValid(email: emailIDTextField.text!)) && (!isValid(phoneNumber: emailIDTextField.text!)) {
            appdelegate.window?.makeToast("Please enter email address/phone number", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
            
            
            
        else{
            
            self.showProgress()
            NetworkInterface.fetchJSON(.forgetPassword, payload: ["EmailOrMobileNumber":emailIDTextField.text!], requestCompletionHander: { (success, data, response, errir) -> (Void) in
                DispatchQueue.main.async {
                    self.hideProgress()
                    if let status = data?["ServiceStatus"] as? [String:Any], status["StatusCode"] as? Int == 0, let message = status["ServiceMessage"] as? String {
                        appdelegate.window?.makeToast(message, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                        self.dismiss(animated: true, completion: {
                        })
                    }else if let status = data?["ServiceStatus"] as? [String:Any],let message = status["ErrorMessage"] as? String {
                        appdelegate.window?.makeToast(message, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    }else{
                        appdelegate.window?.makeToast("Please try later", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    }
                }
            })
            
        }
    
    }
    func displayAlert(_ title:String,message:String)  {
        let alert = UIAlertController(title: title , message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) -> Void in
            NSLog("button OK")
        }
        
        alert.addAction(defaultAction)
        present(alert, animated: true, completion:nil)  // 11
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
