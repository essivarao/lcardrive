
import Foundation

public class LessonList {
	public var lessonId : Int?
	public var learnerId : Int?
	public var instructorId : Int?
	public var instructorName : String?
	public var instructorPhoneNumber : String?
	public var instructorProfilePic : String?
	public var package : DPackage?
	public var rate : Int?
	public var rateModeTypeId : Int?
	public var rateModeType : String?
	public var suburb : String?
	public var postalCode : String?
	public var latitude : String?
	public var longitude : String?
	public var lessonTypeId : Int?
	public var lessonType : String?
	public var lessonDate : String?
	public var learnerStatus : String?
	public var instructorStatus : String?
	public var status : String?
	public var comments : String?
	public var isInstructor : Bool?

	required public init?(dictionary: NSDictionary) {

		lessonId = dictionary["LessonId"] as? Int
		learnerId = dictionary["LearnerId"] as? Int
		instructorId = dictionary["InstructorId"] as? Int
		instructorName = dictionary["InstructorName"] as? String
		instructorPhoneNumber = dictionary["InstructorPhoneNumber"] as? String
		instructorProfilePic = dictionary["InstructorProfilePic"] as? String
		if (dictionary["Package"] != nil) { package = DPackage(dictionary: dictionary["Package"] as! NSDictionary) }
		rate = dictionary["Rate"] as? Int
		rateModeTypeId = dictionary["RateModeTypeId"] as? Int
		rateModeType = dictionary["RateModeType"] as? String
		suburb = dictionary["Suburb"] as? String
		postalCode = dictionary["PostalCode"] as? String
		latitude = dictionary["Latitude"] as? String
		longitude = dictionary["Longitude"] as? String
		lessonTypeId = dictionary["LessonTypeId"] as? Int
		lessonType = dictionary["LessonType"] as? String
		lessonDate = dictionary["LessonDate"] as? String
		learnerStatus = dictionary["LearnerStatus"] as? String
		instructorStatus = dictionary["InstructorStatus"] as? String
		status = dictionary["Status"] as? String
		comments = dictionary["Comments"] as? String
		isInstructor = dictionary["IsInstructor"] as? Bool
	}


}
