//
//  InstructorMessagesTableViewCell.swift
//  LCD
//
//  Created by Admin on 10/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class InstructorMessagesTableViewCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var profileImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        profileImageView.layer.cornerRadius = 25
        profileImageView.layer.masksToBounds = true

        // Configure the view for the selected state
    }

}
