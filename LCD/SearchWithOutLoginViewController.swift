//
//  SearchWithOutLoginViewController.swift
//  LCD
//
//  Created by Admin on 09/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit
import GooglePlaces


class SearchWithOutLoginViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var suberbNameLabel: UILabel!
    @IBOutlet weak var sortByView: UIView!
    @IBOutlet weak var experienceButton: UIButton!
    @IBOutlet weak var ratingsButton: UIButton!
    @IBOutlet weak var rateButton: UIButton!
    
    var sortBy: SortInstructorsBy = .experience
    
    var totalUsers: [SearchedInstructor] = []
    var filterdUsers: [SearchedInstructor] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        searchTextField.delegate = self
        searchTextField.addTarget(self, action: #selector(SearchWithOutLoginViewController.textFieldDidChange(textField:)), for: .editingChanged)
        
        sortByView.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        sortByView.layer.borderWidth = 1
        sortByView.layer.borderColor = UIColor.lightGray.cgColor

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        applyFilterAndSort(searchText: searchTextField.text!)
        fetchInstrocturs()
        sortByView.isHidden = true
        suberbNameLabel.text = SelectedLocation.shared.suburb ?? ""
    }
    
    func applyFilterAndSort(searchText: String){
        filterdUsers.removeAll()
        if searchText.characters.count > 0 {
            filterdUsers = totalUsers.filter({ (($0.firstName ?? "") + " " + ($0.lastName ?? "")).lowercased().range(of: searchText.lowercased()) != nil })
        }else{
            filterdUsers.append(contentsOf: totalUsers)
        }
        
        if let type  = SearchFilterManager.shared.carType {
            switch type  {
            case .suv:
                filterdUsers = filterdUsers.filter({$0.carBodyType?.lowercased() == "SUV".lowercased()})
                break
            case .hatchback:
                filterdUsers = filterdUsers.filter({$0.carBodyType?.lowercased() == "Hatchback".lowercased()})
                break
            case .sedan:
                filterdUsers = filterdUsers.filter({$0.carBodyType?.lowercased() == "Sedan".lowercased()})
                break
            }
        }
        
        if let type  = SearchFilterManager.shared.genderType {
            switch type  {
            case .any:
                break
            case .male:
                filterdUsers = filterdUsers.filter({$0.gender?.lowercased() == "m"})
                break
            case .female:
                filterdUsers = filterdUsers.filter({$0.gender?.lowercased() == "f"})
                break
            }
        }
        
        if let type  = SearchFilterManager.shared.transmittionTpe {
            switch type  {
            case .both:
                filterdUsers = filterdUsers.filter({$0.transmissionType?.lowercased() == "both"})
                break
            case .auto:
                filterdUsers = filterdUsers.filter({$0.transmissionType?.lowercased() == "automatic"})
                break
            case .manual:
                filterdUsers = filterdUsers.filter({$0.transmissionType?.lowercased() == "manual"})
                break
            }
        }
        
        
        
        
        filterdUsers = filterdUsers.sorted { (user1, user2) -> Bool in
            switch sortBy {
            case .experience:
                return user1.experience ?? 0 > user2.experience ?? 0
            case .rate:
                return user1.instructorPackages?.filter({$0.hour == 1}).first?.rate ?? 0 > user2.instructorPackages?.filter({$0.hour == 1}).first?.rate ?? 0
            case .rating:
                return user1.noofRatings ?? 0 > user2.noofRatings ?? 0
            }
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func fetchInstrocturs(){
        

        showProgress()
        
        var payload:[String:Any] = [:]
            
        if SelectedLocation.shared.suburb != nil {
            // Add suburb later here
        }
        
        if let code = SelectedLocation.shared.postalCode {
            payload["PostalCode"] = "\(code)"
        }else{
            payload["PostalCode"] = "2450"
        }
        
        NetworkInterface.fetchJSON(.instructosSearchlist, headers: [:], params: [:], payload: payload) { (success, data, response, error) -> (Void) in
            if let list = data?["InstructorsProfile"] as? [[String:Any]] {
                self.totalUsers.removeAll()
                for (_,user) in list.enumerated() {
                    if let inst = SearchedInstructor.init(dictionary: user as NSDictionary) {
                        self.totalUsers.append(inst)
                    }
                }
            }else{
                // Error
            }
            DispatchQueue.main.async {
                self.hideProgress()
                self.applyFilterAndSort(searchText: self.searchTextField.text!)
                
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //TableView
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterdUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchWithOutSignIn") as? SearchWithOutSignInTableViewCell
        let user = filterdUsers[indexPath.row]
        cell?.nameTextField.text = (user.firstName ?? "") + " " + (user.lastName ?? "")
        cell?.drivingSchoolLabel.text = user.schoolName ?? "School Name Not Available"
        cell?.maleExpLabel.text = "\(user.gender!.lowercased() == "m" ? "Male" : "Female"), Exp: \(user.experience ?? 0)yrs"
        cell?.modeLabel.text = "Mode : \(user.transmissionType ?? "")"
        cell?.ratingLabel.text = user.ratings ?? "-/-"
        cell?.ratingLabel.backgroundColor = UIColorFromRGB(rgbValue: 0x19BC9D)
        cell?.noOfRatingsLabel.text = "( \(user.noofRatings ?? 0) Ratings )"
        
        if (cell?.noOfRatingsLabel.text == "( 0 Ratings )") {
            cell?.noOfRatingsLabel.text = ""
            cell?.ratingLabel.backgroundColor = UIColor.clear
        }
        
        //ratePerHourLabel
        cell?.ratePerHourLabel.text = "$\(0) Per/hr"
        if let instructorPackages = user.instructorPackages {
            for package in instructorPackages {
                if package.hour == 1 {
                    cell?.ratePerHourLabel.text = "$\(package.rate ?? 0) Per/hr"
                    break
                }
            }
        }
        
        if let imageURL = user.profilePicImageUrl {
            let url = URL(string: imageURL)
            cell?.profileImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultFace"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        cell?.carTypeLabel.text = "Car type: " + (user.carBodyType ?? "")
        
        cell?.priceOneLabel.backgroundColor = UIColor.clear
        cell?.pricetwoLabel.backgroundColor = UIColor.clear
        cell?.priceThreeLabel.backgroundColor = UIColor.clear

        cell?.priceOneLabel.text = ""
        cell?.pricetwoLabel.text = ""
        cell?.priceThreeLabel.text = ""
        
        if let packages = user.instructorPackages {
            
            if packages.count > 0 {
                let package = packages.first
                cell?.priceOneLabel.text = "$\(package?.rate ?? 0) / \(package?.hour == 1 ? "Hour" : "\(package?.hour ?? 0) Hours")"
                cell?.priceOneLabel.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
                cell?.priceOneLabel.layer.masksToBounds = true
                cell?.priceOneLabel.backgroundColor = UIColor.lightGray
                
            }
            if packages.count > 1 {
                let package = packages[1]
                
                cell?.pricetwoLabel.text = "$\(package.rate ?? 0) / \(package.hour == 1 ? "Hour" : "\(package.hour ?? 0) Hours")"
                cell?.pricetwoLabel.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
                cell?.pricetwoLabel.layer.masksToBounds = true
                cell?.pricetwoLabel.backgroundColor = UIColor.lightGray
                
                
            }
            if packages.count > 2 {
                let package = packages[2]
                
                cell?.priceThreeLabel.text = "$\(package.rate ?? 0) / \(package.hour == 1 ? "Hour" : "\(package.hour ?? 0) Hours")"
                cell?.priceThreeLabel.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
                cell?.priceThreeLabel.layer.masksToBounds = true
                cell?.priceThreeLabel.backgroundColor = UIColor.lightGray
            }
        }
        
        cell?.callButton.tag = indexPath.row
        cell?.callButton.addTarget(self, action: #selector(SearchWithOutLoginViewController.callButtonClicked(sender:)), for: .touchUpInside)
        
        cell?.selectionStyle = .none
        return cell!
    }

  
    func callButtonClicked(sender : UIButton) {
        let user = filterdUsers[sender.tag]
        let phoneNum = "+61\(user.phoneNumber!)"
        if let url = URL(string: "telprompt://\(phoneNum)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
        else {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window?.makeToast("Phone number not valid.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyBoard = UIStoryboard.init(name: "Learner", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SearchUserDetailWithOutSignIn") as! SearchUserDetailWithOutSignInViewController
        vc.selectedUser = filterdUsers[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)

    }    


    @IBAction func sortByClicked(_ sender: Any) {

        
//        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        
//        let sendButton = UIAlertAction(title: "EXPERIENCED", style: .default, handler: { (action) -> Void in
//            self.sortBy = .experience
//            self.applyFilterAndSort(searchText: self.searchTextField.text!)
//        })
//        
//        let  deleteButton = UIAlertAction(title: "RATINGS", style: .default, handler: { (action) -> Void in
//            self.sortBy = .rating
//            self.applyFilterAndSort(searchText: self.searchTextField.text!)
//
//        })
        
//        [deleteButton setValue:[[UIImage imageNamed:@"online.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
        
//        [deleteButton .setValue([[UIImage imageNamed:@"online.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal], forKey: <#T##String#>)]
        
//        let rateButton = UIAlertAction(title: "RATE", style: .default, handler: { (action) -> Void in
//            self.sortBy = .rate
//            self.applyFilterAndSort(searchText: self.searchTextField.text!)
//        })
//        
//        let cancelButton = UIAlertAction(title: "CANCEL", style: .cancel, handler: { (action) -> Void in
//            
//        })
//        
//        
//        alertController.addAction(sendButton)
//        alertController.addAction(deleteButton)
//        alertController.addAction(rateButton)
//        alertController.addAction(cancelButton)
//        
//        self.present(alertController, animated: true, completion: nil)
        
        sortByView.isHidden = false
        
    }
    @IBAction func filterButtonClicked(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let filtersView = storyBoard.instantiateViewController(withIdentifier: "LCDFiltersView")
        self.navigationController?.pushViewController(filtersView, animated: true)
    }
    @IBAction func experienceClicked(_ sender: Any) {
    
        
        sortByView.isHidden = true
        self.sortBy = .experience
        self.applyFilterAndSort(searchText: self.searchTextField.text!)
        
        (sender as AnyObject).setImage(#imageLiteral(resourceName: "ic_done"), for: .normal)
        
        rateButton.setImage(nil, for: .normal)
        ratingsButton.setImage(nil, for: .normal)
        
        experienceButton.imageEdgeInsets = UIEdgeInsetsMake(0, 120, 0, 15)
        experienceButton.titleEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        
        rateButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
        ratingsButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)

        
        
    }
    @IBAction func ratingsClicked(_ sender: Any) {
        self.sortBy = .rating
        self.applyFilterAndSort(searchText: self.searchTextField.text!)
        sortByView.isHidden = true
        
        (sender as AnyObject).setImage(#imageLiteral(resourceName: "ic_done"), for: .normal)
        experienceButton.setImage(nil, for: .normal)
        rateButton.setImage(nil, for: .normal)
        ratingsButton.imageEdgeInsets = UIEdgeInsetsMake(0, 120, 0, 15)
        ratingsButton.titleEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        experienceButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
        rateButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)


    }
    @IBAction func rateClicked(_ sender: Any) {
        self.sortBy = .rate
        self.applyFilterAndSort(searchText: self.searchTextField.text!)
        sortByView.isHidden = true
        
        (sender as AnyObject).setImage(#imageLiteral(resourceName: "ic_done"), for: .normal)
        experienceButton.setImage(nil, for: .normal)
        ratingsButton.setImage(nil, for: .normal)
        rateButton.imageEdgeInsets = UIEdgeInsetsMake(0, 120, 0, 15)
        rateButton.titleEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        experienceButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
        ratingsButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)

    }
    @IBAction func searchSuburbClicked(_ sender: Any) {
        let googleAutoVC = GMSAutocompleteViewController.init()
        
        googleAutoVC.delegate = self
        googleAutoVC.view.backgroundColor = UIColor.clear
        let filter = GMSAutocompleteFilter.init()
        filter.country = "AU"
        filter.type = .region

        googleAutoVC.autocompleteFilter = filter
        
        self.present(googleAutoVC, animated: true) {
            
        }

    }
    @IBAction func backButtonClicked(_ sender: Any) {
  
//        AppDelegate.shared.setupRootForNoSession()
        self.navigationController?.popViewController(animated: true)
    }
}



extension SearchWithOutLoginViewController {
    func textFieldDidChange(textField: UITextField) {
        applyFilterAndSort(searchText: textField.text!)
        
    }
}

extension SearchWithOutLoginViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        let location = SelectedLocation.shared
        location.latitude = "\(place.coordinate.latitude)"
        location.longitude = "\(place.coordinate.longitude)"
        
        let suburbString = place.formattedAddress?.replacingOccurrences(of: ", Australia", with: "")
        location.suburb = suburbString
        
        
        if let postalCodeComponent =  place.addressComponents?.filter({$0.type == "postal_code"}).first {
            location.postalCode = Int(postalCodeComponent.name)
        }
        location.synchronize()
        
        viewController.dismiss(animated: true) {
            self.fetchInstrocturs()
        }
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error.localizedDescription)")
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        viewController.dismiss(animated: true, completion: nil)
    }
    
}

