//
//  LearnerViewController.swift
//  LCD
//
//  Created by Admin on 01/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit
import GooglePlaces

class LearnerViewController: UIViewController ,UITextFieldDelegate {

    @IBOutlet var suburbTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        
    
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let googleAutoVC = GMSAutocompleteViewController.init()
        
        googleAutoVC.delegate = self
        googleAutoVC.view.backgroundColor = UIColor.clear
        let filter = GMSAutocompleteFilter.init()
        filter.country = "AU"
        filter.type = .region

        googleAutoVC.autocompleteFilter = filter
        
        self.present(googleAutoVC, animated: true) {
            
        }

        
        
    }


    @IBAction func cancelbuttonClicked(_ sender: Any) {
//        dismiss(animated: true, completion: {
//            
//        })

        self.navigationController?.popViewController(animated: true)
    }

}

extension LearnerViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        let location = SelectedLocation.shared
        location.latitude = "\(place.coordinate.latitude)"
        location.longitude = "\(place.coordinate.longitude)"

        let suburbString = place.formattedAddress?.replacingOccurrences(of: ", Australia", with: "")
        location.suburb = suburbString
        
        if let postalCodeComponent =  place.addressComponents?.filter({$0.type == "postal_code"}).first {
            location.postalCode = Int(postalCodeComponent.name)
        }
        location.synchronize()
        
        viewController.dismiss(animated: true) { 
            let storyBoard = UIStoryboard.init(name: "Learner", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "SearchWithOutLoginViewController")
            self.navigationController?.pushViewController(vc, animated: true)
//            self.present(vc, animated: false, completion: nil)
        }

    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error.localizedDescription)")
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        viewController.dismiss(animated: true, completion: nil)
    }
    
}



