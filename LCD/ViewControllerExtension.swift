//
//  ViewControllerExtension.swift
//  LCD
//
//  Created by Narendra Kumar R on 3/4/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
extension UIViewController {
    func showProgress(){
        DispatchQueue.main.async {
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
    }
    
    func hideProgress(){
        DispatchQueue.main.async {
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }
    
    func alert(title: String, message:String) {
        let vc = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        vc.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
        present(vc, animated: true, completion: nil)
    }
    
    
}

extension UIViewController{
    
    func alert(title: String, message: String, actionTitle: String = "OK", presentedVC:UIViewController, completion: ((UIAlertAction)-> Void)?){
        DispatchQueue.main.async {
            let alertVc = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
            alertVc.addAction(UIAlertAction.init(title: actionTitle, style: .default, handler: completion))
            UIViewController.getCurrentViewController(vc: presentedVC)?.present(alertVc, animated: false, completion: nil)
            
        }
    }
    
    static func getCurrentViewController(vc: UIViewController) -> UIViewController? {
        if let pvc = vc.presentedViewController {
            return getCurrentViewController(vc: pvc)
        }
        else if let svc = vc as? UISplitViewController, svc.viewControllers.count > 0 {
            return getCurrentViewController(vc: svc.viewControllers.last!)
        }
        else if let nc = vc as? UINavigationController, nc.viewControllers.count > 0 {
            return getCurrentViewController(vc: nc.topViewController!)
        }
        else if let tbc = vc as? UITabBarController {
            if let svc = tbc.selectedViewController {
                return getCurrentViewController(vc: svc)
            }
        }
        return vc
    }
}
