

import Foundation
 

public class InstructorRequest {
	public var requestId : Int?
	public var instructorId : Int?
	public var learnerId : Int?
	public var learnerName : String?
	public var learnerPhoneNumber : String?
	public var suburb : String?
	public var postalCode : Int?
	public var latitude : String?
	public var longitude : String?
	public var lessonTypeId : Int?
	public var lessonType : String?
	public var learnerProfilePic : String?
    var learnerPreferredDays : Array<LearnerPreferredDays>?
	public var unReadMessagesCount : Int?


	required public init?(dictionary: NSDictionary) {

		requestId = dictionary["RequestId"] as? Int
		instructorId = dictionary["InstructorId"] as? Int
		learnerId = dictionary["LearnerId"] as? Int
		learnerName = dictionary["LearnerName"] as? String
		learnerPhoneNumber = dictionary["LearnerPhoneNumber"] as? String
		suburb = dictionary["Suburb"] as? String
		postalCode = dictionary["PostalCode"] as? Int
		latitude = dictionary["Latitude"] as? String
		longitude = dictionary["Longitude"] as? String
		lessonTypeId = dictionary["LessonTypeId"] as? Int
		lessonType = dictionary["LessonType"] as? String
		learnerProfilePic = dictionary["LearnerProfilePic"] as? String
		if (dictionary["LearnerPreferredDays"] != nil) { learnerPreferredDays = LearnerPreferredDays.modelsFromDictionaryArray(array: dictionary["LearnerPreferredDays"] as! NSArray) }
		unReadMessagesCount = dictionary["UnReadMessagesCount"] as? Int
	}
}

public class LearnerPreferredDays {
    public var rPDId : Int?
    public var requestId : Int?
    public var weekDay : String?
    public var period : String?
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [LearnerPreferredDays]
    {
        var models:[LearnerPreferredDays] = []
        for item in array
        {
            models.append(LearnerPreferredDays(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        
        rPDId = dictionary["RPDId"] as? Int
        requestId = dictionary["RequestId"] as? Int
        weekDay = dictionary["WeekDay"] as? String
        period = dictionary["Period"] as? String
    }
    
}
