//
//  InstructorBookLessonTableViewCell.swift
//  LCD
//
//  Created by Admin on 12/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class InstructorBookLessonTableViewCell: UITableViewCell {

    @IBOutlet var fullTitleLabel: UILabel!
    @IBOutlet var selectedImageView: UIImageView!
    @IBOutlet var backGroundView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backGroundView.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
