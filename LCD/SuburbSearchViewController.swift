//
//  SuburbSearchViewController.swift
//  LCD
//
//  Created by kvanaworld on 4/2/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit
import GooglePlaces

protocol SubUrbSearchDelegate {
    func didSelectSuburb()
}

class SuburbSearchViewController: UIViewController {

    @IBOutlet weak var suburbTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let googleAutoVC = GMSAutocompleteViewController.init()
        
        googleAutoVC.delegate = self
        googleAutoVC.view.backgroundColor = UIColor.clear
        let filter = GMSAutocompleteFilter.init()
        filter.country = "AU"
        filter.type = .region

        googleAutoVC.autocompleteFilter = filter
        
        self.present(googleAutoVC, animated: true) {
            
        }
        
        
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
    @IBAction func cancelButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        (AppDelegate.shared.window?.rootViewController as! UITabBarController).selectedIndex = 1
    }

}

extension SuburbSearchViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        let location = SelectedLocation.shared
        location.latitude = "\(place.coordinate.latitude)"
        location.longitude = "\(place.coordinate.longitude)"
        
        let suburbString = place.formattedAddress?.replacingOccurrences(of: ", Australia", with: "")
        location.suburb = suburbString
        
        if let postalCodeComponent =  place.addressComponents?.filter({$0.type == "postal_code"}).first {
            location.postalCode = Int(postalCodeComponent.name)
        }
        location.synchronize()
        
        
        viewController.dismiss(animated: false, completion: nil)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error.localizedDescription)")
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        viewController.dismiss(animated: true, completion: nil)
    }
    
}
