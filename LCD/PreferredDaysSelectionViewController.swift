//
//  PreferredDaysSelectionViewController.swift
//  DrivingAUS
//
//  Created by Admin on 21/02/17.
//  Copyright © 2017 Sarella. All rights reserved.
//

import UIKit

class SendRequestPreferDay{
    var weekDay: String?
    var period:String?
}

class SendRequest{
//    "RequestId": 1,
    var learnerId: Int?
    var instructorId:Int?
    var userId:Int?
    var suburb:String?
    var postalCode:String?
    var latitude:String?
    var longitude:String?
    var lessonTypeId:Int?
    var isActive = true
    var message:String?
    var preferDays: [SendRequestPreferDay] = []
    var instructorFullName: String?
    var instructorImageURL: String?
}

class PreferredDaysSelectionViewController: UIViewController {

    @IBOutlet var saturdayMorning: UIButton!
    @IBOutlet var saturdayAfternoon: UIButton!
    @IBOutlet var saturdayEvening: UIButton!
    @IBOutlet var sundaymoring: UIButton!
    @IBOutlet var sundayAfternoon: UIButton!
    @IBOutlet var sundayEvening: UIButton!
    @IBOutlet var mondayMorning: UIButton!
    @IBOutlet var mondayAfternoon: UIButton!
    @IBOutlet var mondayEvening: UIButton!
    @IBOutlet var tuesdayMoring: UIButton!
    @IBOutlet var tuesadayAfternoon: UIButton!
    @IBOutlet var tuesdayEvening: UIButton!
    @IBOutlet var wednesdayMoring: UIButton!
    @IBOutlet var wednesdayAfternoon: UIButton!
    @IBOutlet var wednesdayEvening: UIButton!
    @IBOutlet var thursdayMorning: UIButton!
    @IBOutlet var thursdatAfternoon: UIButton!
    @IBOutlet var thursdayEvening: UIButton!
    @IBOutlet var fridayMoring: UIButton!
    @IBOutlet var fridayAfternoon: UIButton!
    @IBOutlet var fridayEvening: UIButton!
    @IBOutlet var continueButton: UIButton!
    var selectedUser:SearchedInstructor?
    
    var request: SendRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        continueButton.layer.cornerRadius = 15
        continueButton.layer.borderColor = UIColor.gray.cgColor
        continueButton.layer.borderWidth = 3
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 

    @IBAction func selectedButton(_ sender: UIButton) {
        
        if (sender.titleLabel?.text == "satMng"){
            if saturdayMorning.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: saturdayMorning, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: saturdayMorning, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        else if (sender.titleLabel?.text == "satAft"){
            if saturdayAfternoon.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: saturdayAfternoon, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: saturdayAfternoon, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        if (sender.titleLabel?.text == "satEve"){
            if saturdayEvening.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: saturdayEvening, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: saturdayEvening, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        
        if (sender.titleLabel?.text == "sunMng"){
            if sundaymoring.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: sundaymoring, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: sundaymoring, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        else if (sender.titleLabel?.text == "sunAft"){
            if sundayAfternoon.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: sundayAfternoon, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: sundayAfternoon, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        if (sender.titleLabel?.text == "sunEve"){
            if sundayEvening.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: sundayEvening, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: sundayEvening, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        
        
        if (sender.titleLabel?.text == "monMng"){
            if mondayMorning.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: mondayMorning, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: mondayMorning, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        else if (sender.titleLabel?.text == "monAft"){
            if mondayAfternoon.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: mondayAfternoon, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: mondayAfternoon, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        if (sender.titleLabel?.text == "monEve"){
            if mondayEvening.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: mondayEvening, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: mondayEvening, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        
        if (sender.titleLabel?.text == "tueMng"){
            if tuesdayMoring.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: tuesdayMoring, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: tuesdayMoring, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        else if (sender.titleLabel?.text == "tueAft"){
            if tuesadayAfternoon.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: tuesadayAfternoon, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: tuesadayAfternoon, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        if (sender.titleLabel?.text == "tueEve"){
            if tuesdayEvening.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: tuesdayEvening, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: tuesdayEvening, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        
        
        
        if (sender.titleLabel?.text == "wedMng"){
            if wednesdayMoring.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: wednesdayMoring, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: wednesdayMoring, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        else if (sender.titleLabel?.text == "wedAft"){
            if wednesdayAfternoon.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: wednesdayAfternoon, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: wednesdayAfternoon, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        if (sender.titleLabel?.text == "wedEve"){
            if wednesdayEvening.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: wednesdayEvening, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: wednesdayEvening, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        
        if (sender.titleLabel?.text == "thuMng"){
            if thursdayMorning.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: thursdayMorning, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: thursdayMorning, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        else if (sender.titleLabel?.text == "thuAft"){
            if thursdatAfternoon.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: thursdatAfternoon, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: thursdatAfternoon, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        if (sender.titleLabel?.text == "thuEve"){
            if thursdayEvening.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: thursdayEvening, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: thursdayEvening, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        
        if (sender.titleLabel?.text == "friMng"){
            if fridayMoring.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: fridayMoring, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: fridayMoring, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        else if (sender.titleLabel?.text == "friAft"){
            if fridayAfternoon.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: fridayAfternoon, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: fridayAfternoon, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }
        if (sender.titleLabel?.text == "friEve"){
            if fridayEvening.imageView?.image == #imageLiteral(resourceName: "Unchecked Checkbox-48"){
                selectedClicked(sender: fridayEvening, image: #imageLiteral(resourceName: "Checked Checkbox-48"))
            }
            else{
                selectedClicked(sender: fridayEvening, image: #imageLiteral(resourceName: "Unchecked Checkbox-48"))
            }
        }

        
        
    }
    
    func selectedClicked(sender: UIButton , image : UIImage)  {
        sender.setImage(image, for: .normal)
    }
    @IBAction func ContinueClicked(_ sender: Any) {
        
        
        
        if saturdayMorning.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "sat"
            preference.period = "m"
            self.request?.preferDays.append(preference)
        }
        
        if saturdayAfternoon.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "sat"
            preference.period = "a"
            self.request?.preferDays.append(preference)

        }
        
        if saturdayEvening.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "sat"
            preference.period = "e"
            self.request?.preferDays.append(preference)

        }
        
        if sundaymoring.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "sun"
            preference.period = "m"
            self.request?.preferDays.append(preference)

        }
        if sundayAfternoon.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "sun"
            preference.period = "a"
            self.request?.preferDays.append(preference)

        }
        if sundayEvening.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "sun"
            preference.period = "e"
            self.request?.preferDays.append(preference)

        }
        if mondayMorning.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "mon"
            preference.period = "m"
            self.request?.preferDays.append(preference)

        }
        if mondayAfternoon.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "mon"
            preference.period = "a"
            self.request?.preferDays.append(preference)

        }
        if mondayEvening.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "mon"
            preference.period = "e"
            self.request?.preferDays.append(preference)

        }
        if tuesdayMoring.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "tue"
            preference.period = "m"
            self.request?.preferDays.append(preference)
  
        }
        if tuesadayAfternoon.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "tue"
            preference.period = "a"
            self.request?.preferDays.append(preference)

        }
        if tuesdayEvening.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "tue"
            preference.period = "e"
            self.request?.preferDays.append(preference)

        }
        if wednesdayMoring.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "wed"
            preference.period = "m"
            self.request?.preferDays.append(preference)

        }
        if wednesdayAfternoon.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "wed"
            preference.period = "a"
            self.request?.preferDays.append(preference)

        }
        if wednesdayEvening.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "wed"
            preference.period = "e"
            self.request?.preferDays.append(preference)

        }
        if thursdayMorning.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "thu"
            preference.period = "m"
            self.request?.preferDays.append(preference)

        }
        if thursdatAfternoon.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "thu"
            preference.period = "a"
            self.request?.preferDays.append(preference)

        }
        if thursdayEvening.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "thu"
            preference.period = "e"
            self.request?.preferDays.append(preference)

        }
        if fridayMoring.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "fri"
            preference.period = "m"
            self.request?.preferDays.append(preference)

        }
        if fridayAfternoon.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "fri"
            preference.period = "a"
            self.request?.preferDays.append(preference)

        }
        if fridayEvening.imageView?.image == #imageLiteral(resourceName: "Checked Checkbox-48") {
            let preference = SendRequestPreferDay.init()
            preference.weekDay = "fri"
            preference.period = "e"
            self.request?.preferDays.append(preference)
        }
        

        if self.request?.preferDays.count == 0 {
            
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window?.makeToast("Please select atleast one preference", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            
            return
        }
  
        let storyBoard = UIStoryboard.init(name: "Learner", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SendRequestViewController") as! SendRequestViewController
        vc.request = request
        vc.selectedUser = selectedUser
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
  
    @IBAction func backButtonClicked(_ sender: Any) {
        SearchUserDetailWithOutSignInViewController.selectedUserInMemory = nil
        _ = self.navigationController?.popViewController(animated: true)
    }

}
