//
//  LCDFiltersViewController.swift
//  LCD
//
//  Created by Admin on 01/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit


enum GenderFilterType: Int {
    case male = 0
    case female = 1
    case any = 2
}

enum TransmitionFilterType: Int{
    case auto = 0
    case manual = 1
    case both = 2
}

enum CarFilterType:Int{
    case hatchback = 0
    case sedan = 1
    case suv = 2
}


@objc class SearchFilterManager:NSObject,NSCoding{
    
    var genderType: GenderFilterType?
    var transmittionTpe: TransmitionFilterType?
    var carType: CarFilterType?
    
    static let shared = SearchFilterManager()
    override init() {
        
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        if let genderValue = aDecoder.decodeObject(forKey: "genderType") as? Int {
            self.genderType = GenderFilterType.init(rawValue: genderValue)
        }
        
        if let value = aDecoder.decodeObject(forKey: "transmittionTpe") as? Int {
            self.transmittionTpe = TransmitionFilterType.init(rawValue: value)
        }
        
        if let value = aDecoder.decodeObject(forKey: "carType") as? Int {
            self.carType = CarFilterType.init(rawValue: value)
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(genderType?.rawValue, forKey: "genderType")
        aCoder.encode(transmittionTpe?.rawValue, forKey: "transmittionTpe")
        aCoder.encode(carType?.rawValue, forKey: "carType")
    }
    
    func synchronize(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(SearchFilterManager.shared, toFile: Utils.filtersPath().path)
        if !isSuccessfulSave {
            print("Failed to save user...")
        }
    }
    
    func unarchive() {
        let user = NSKeyedUnarchiver.unarchiveObject(withFile: Utils.filtersPath().path) as? SearchFilterManager
        genderType = user?.genderType
        transmittionTpe = user?.transmittionTpe
        carType = user?.carType
    }
}

class ReselectableSegmentedControl: UISegmentedControl {
    @IBInspectable var allowReselection: Bool = true
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let previousSelectedSegmentIndex = self.selectedSegmentIndex
        super.touchesEnded(touches, with: event)
        if allowReselection && previousSelectedSegmentIndex == self.selectedSegmentIndex {
            if let touch = touches.first {
                let touchLocation = touch.location(in: self)
                if bounds.contains(touchLocation) {
                    self.sendActions(for: .valueChanged)
                }
            }
        }
    }
}

class LCDFiltersViewController: UIViewController{
    
    @IBOutlet var navigationbar: UIView!
    @IBOutlet var clearButton: UIButton!
    @IBOutlet var applyButton: UIButton!
    @IBOutlet var headerLabel: UILabel!
    @IBOutlet var sexSegementedControl: UISegmentedControl!
    @IBOutlet var transmissionSegmentedControl: UISegmentedControl!
    @IBOutlet var carTypeSegmentedControl: UISegmentedControl!
    @IBOutlet var genderLabel: UILabel!
    @IBOutlet var gearTypeLabel: UILabel!
    @IBOutlet var carTypeLabel: UILabel!
    
    var genderSelectedIndex: Int?
    var transmissionSelectedIndex: Int?
    var carTypeSelectedIndex: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSetUp()
        sexSegementedControl.selectedSegmentIndex = UISegmentedControlNoSegment
        transmissionSegmentedControl.selectedSegmentIndex = UISegmentedControlNoSegment
        carTypeSegmentedControl.selectedSegmentIndex = UISegmentedControlNoSegment
        
        
        if  let type = SearchFilterManager.shared.genderType {
            sexSegementedControl.selectedSegmentIndex = type.rawValue
            genderSelectedIndex = type.rawValue
        }
        
        if  let type = SearchFilterManager.shared.transmittionTpe {
            transmissionSegmentedControl.selectedSegmentIndex = type.rawValue
            transmissionSelectedIndex = type.rawValue

        }
        
        if  let type = SearchFilterManager.shared.carType {
            carTypeSegmentedControl.selectedSegmentIndex = type.rawValue
            carTypeSelectedIndex = type.rawValue
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backButtonClicked(_ sender: Any) {
        sexSegementedControl.selectedSegmentIndex = UISegmentedControlNoSegment
        transmissionSegmentedControl.selectedSegmentIndex = UISegmentedControlNoSegment
        carTypeSegmentedControl.selectedSegmentIndex = UISegmentedControlNoSegment
        
        SearchFilterManager.shared.genderType = GenderFilterType.init(rawValue: sexSegementedControl.selectedSegmentIndex)
        SearchFilterManager.shared.transmittionTpe = TransmitionFilterType.init(rawValue: transmissionSegmentedControl.selectedSegmentIndex)
        SearchFilterManager.shared.carType = CarFilterType.init(rawValue: carTypeSegmentedControl.selectedSegmentIndex)
        SearchFilterManager.shared.synchronize()
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func applyButtonClicked(_ sender: Any) {
        
        SearchFilterManager.shared.genderType = GenderFilterType.init(rawValue: sexSegementedControl.selectedSegmentIndex)
        SearchFilterManager.shared.transmittionTpe = TransmitionFilterType.init(rawValue: transmissionSegmentedControl.selectedSegmentIndex)
        SearchFilterManager.shared.carType = CarFilterType.init(rawValue: carTypeSegmentedControl.selectedSegmentIndex)
        SearchFilterManager.shared.synchronize()
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func sexSegmentedClicked(_ sender: UISegmentedControl) {
        if (sender.selectedSegmentIndex == genderSelectedIndex) {
            sender.selectedSegmentIndex =  UISegmentedControlNoSegment;
            genderSelectedIndex = UISegmentedControlNoSegment;
        }
        else {
            genderSelectedIndex = sender.selectedSegmentIndex;
        }
    }
    @IBAction func trasmissionSegmentedClicked(_ sender: UISegmentedControl) {
        if (sender.selectedSegmentIndex == transmissionSelectedIndex) {
            sender.selectedSegmentIndex =  UISegmentedControlNoSegment;
            transmissionSelectedIndex = UISegmentedControlNoSegment;
        }
        else {
            transmissionSelectedIndex = sender.selectedSegmentIndex;
        }
    }
    
    
    @IBAction func carTypeSelectedClicked(_ sender: UISegmentedControl) {
        if (sender.selectedSegmentIndex == carTypeSelectedIndex) {
            sender.selectedSegmentIndex =  UISegmentedControlNoSegment;
            carTypeSelectedIndex = UISegmentedControlNoSegment;
        }
        else {
            carTypeSelectedIndex = sender.selectedSegmentIndex;
        }
        
    }
    
    
}

extension LCDFiltersViewController{
    func viewSetUp() {
        navigationbar.backgroundColor = UIColorFromRGB(rgbValue: Color.APP_THEMECOLOR)
        headerLabel.font = UIFont (name:FontName.REGULAR , size: CGFloat(FontSize.TEXTFILED_SMALL))
        clearButton.titleLabel?.font = UIFont (name:FontName.REGULAR , size: CGFloat(FontSize.BUTTON_MEDIUM))
        applyButton.titleLabel?.font = UIFont (name:FontName.REGULAR , size: CGFloat(FontSize.BUTTON_MEDIUM))
        genderLabel.font = UIFont (name:FontName.LIGHT , size: CGFloat(FontSize.TEXTFILED_SMALL))
        gearTypeLabel.font = UIFont (name:FontName.LIGHT , size: CGFloat(FontSize.TEXTFILED_SMALL))
        carTypeLabel.font = UIFont (name:FontName.LIGHT , size: CGFloat(FontSize.TEXTFILED_SMALL))
        
    }
}
