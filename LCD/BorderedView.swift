

import UIKit

class BorderedView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
    }
    
    
    

}
