//
//  DiscountsViewController.swift
//  DrivingAUS
//
//  Created by Kiran Sarella on 17/10/16.
//  Copyright © 2016 Sarella. All rights reserved.
//

import UIKit

class Package{
    var rate: Int?
    var hours: Int?
    var type: Int?
    var discription:String?
    var id: Int?
    var name: String?
    var isActive: Bool = false
    var updated: Bool = false
}

class DiscountsViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDiscountsView: UIView!
    
    var packages: [Package] = []
    // MARK: - Properties
    var email:String?
    
    // MARK: - Actions
    
    @IBAction func saveButtonClicked(_ sender: Any) {
        
        var pkgs:[[String:Any]] = []
        let updates = packages.filter({$0.updated == true})
        if updates.count > 0 {
            for package in updates {
                var pkg = [
                    "Rate" : package.rate ?? 0,
                    "Hour": package.hours ?? 0,
                    "RateModeId": 10,
                    "PackageTypeId": package.type ?? 15 ,
                    "IsActive": package.isActive
                ] as [String:Any]
                if let pkgId = package.id {
                    pkg["PackageId"] = pkgId
                }
                if let description = package.discription {
                    pkg["PackageDesc"] = description
                }
                pkgs.append(pkg)
            }
            let dict = [
                "UserId": Instructor.shared.userID!,
                "InstructorPackage":pkgs
                ] as [String : Any]
            
            self.showProgress()
            NetworkInterface.fetchJSON(.savePackages, payload: dict) { (success, data, response, error) -> (Void) in
                DispatchQueue.main.async {
                    if data?["StatusCode"] as? Int == 0 {
                        //                    self.alert(title: "Success", message: "Package successfully added.")
                    }
                    let _ = self.navigationController?.popViewController(animated: true)
                    self.hideProgress()
                }
            }
        }else{
            let _ = self.navigationController?.popViewController(animated: true)
        }

        
    }
    @IBAction func backAction(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addDiscountAction(_ sender: UIButton) {
        
        newDiscount()
    }
    
    @IBOutlet weak var updateButton: UIButton!
    
    @IBAction func updateAction(_ sender: UIButton) {
        
    }
    
    @IBAction func addFirstDiscountAction(_ sender: UIButton) {
        
        tableView.isHidden = false
        noDiscountsView.isHidden = true
        updateButton.isHidden = false
        
        newDiscount()
    }
    
    func newDiscount() {
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
        fetchPackages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
//        updateContent()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    
    
    // MARK:-
    func updateContent() {
        
    }
    
    
}


extension DiscountsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 1 {
            return 100
        }
        
//        if indexPath.row ==  {
//            return 50.0
//        }
        else {
            
            return 44.0
        }
    }
    
}

func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
}

extension DiscountsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return self.packages.filter({$0.type! == 15 && $0.isActive}).count
        }
        else {
            return self.packages.filter({$0.type! == 16 && $0.isActive}).count
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.section == 0 {
            let array = self.packages.filter({$0.type == 15 && $0.isActive})
            let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountsTableViewCell") as? DiscountsTableViewCell
            let pachage =  array[indexPath.row]
            cell?.priceTextField.leftViewMode = .always
            let dollarView =  UIImageView(image: UIImage(named: "ic_dolar"))
            dollarView.frame = CGRect(x: 2, y: 0, width: 20, height: 22)
            cell?.priceTextField.leftView = dollarView
            cell?.priceTextField.clipsToBounds = true
            cell?.myIndexPath = indexPath
            
            cell?.priceTextField.text = "\(pachage.rate ?? 0)" //discounts[indexPath.row].price ?? ""
            cell?.hourTextField.text = "\(pachage.hours ?? 0)" //discounts[indexPath.row].hour ?? ""
            
            return cell!

        }else{
            let array = self.packages.filter({$0.type == 16 && $0.isActive})
            let pachage =  array[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "descrriptionCell") as? DescriptionTableViewCell
            cell?.priceTextField.leftViewMode = .always
            let dollarView =  UIImageView(image: UIImage(named: "ic_dolar"))
            dollarView.frame = CGRect(x: 2, y: 0, width: 20, height: 22)
            cell?.priceTextField.leftView = dollarView
            cell?.priceTextField.clipsToBounds = true
            
//            cell?.priceTextField.text =  "\(package?.rate ?? "")" //testDrives[indexPath.row].price ?? ""
            
            if let hours  = pachage.rate {
                cell?.priceTextField.text = "\(hours)"
            }
            
            cell?.descriptionLabel.text = pachage.discription ?? "Description : --" //testDrives[indexPath.row].description ?? ""
            
            return cell!

        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let vc : EnterPriceAndHoursViewController = self.storyboard?.instantiateViewController(withIdentifier: "enterPriceAndHoursViewController") as! EnterPriceAndHoursViewController
            let array = self.packages.filter({$0.type == 15 && $0.isActive})
            let package = array[indexPath.row]
            vc.package = package
            vc.delegate = self
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: false, completion: nil)

        }
        
        if indexPath.section == 1 {
            let vc : EnterPriceAndHoursViewController = self.storyboard?.instantiateViewController(withIdentifier: "enterPriceAndHoursViewController") as! EnterPriceAndHoursViewController
            let array = self.packages.filter({$0.type == 16 && $0.isActive})
        //var package = array.first
        // if package == nil {
        //     package = Package()
        //     packages.append(package!)
        // }
            let package = array[indexPath.row]
            package.type = 16
            vc.package = package
            vc.delegate = self
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: false, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        //if section == 0{
            return 44
        //}
        //else{
          //  return 0
        //}
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        else{
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        
        let line = UIView()
        line.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 1)
        line.backgroundColor = UIColor.lightGray
        headerView.addSubview(line)
        
        headerView.backgroundColor = UIColor.white
        let titleView:UILabel = UILabel(frame: CGRect(x: 25, y: 5, width: 250, height: 30))
        titleView.text = "Driving Test Package"
        titleView.font = UIFont (name: "Montserrat-Light", size: 14)
        headerView.addSubview(titleView)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        //if ( section == 0){
            
            let footerRow = UIView()
            footerRow.backgroundColor = UIColor.white
            let button:UIButton = UIButton(frame: CGRect(x: 25, y: 5, width: 100, height: 30))
            button.backgroundColor = UIColorFromRGB(rgbValue: 0x19BC9D)
            button.setTitle("ADD", for: .normal)
            button.titleLabel?.font = UIFont (name: "Montserrat-Light", size: 14)
            button.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
            
            let imageView = UIImageView()
            imageView.frame = CGRect(x: 10 , y: 0, width: 30, height: 30)
            imageView.image = #imageLiteral(resourceName: "add")
            button.addSubview(imageView)
            button.titleEdgeInsets = UIEdgeInsetsMake(0, 15 , 0, 0)
            
            button.addTarget(self, action:#selector(addrowCilced(sender:)), for: .touchUpInside)
            button.tag = section
            footerRow.addSubview(button)
            return footerRow
        //}
        //else{
        //    let footerRow = UIView()
        //    return footerRow
            
        //}
    }
    
    func addrowCilced(sender : UIButton){
        let vc : EnterPriceAndHoursViewController = self.storyboard?.instantiateViewController(withIdentifier: "enterPriceAndHoursViewController") as! EnterPriceAndHoursViewController
        let package = Package()

        if sender.tag == 0 {
            package.type = 15
        }else{
            package.type = 16
        }
        vc.package = package
        packages.append(package)
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        present(vc, animated: false, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        //if indexPath.section == 0 {
            return true
        //}
        //return false
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.delete {
             if indexPath.section == 0 {
                let array = self.packages.filter({$0.type == 15 && $0.isActive})
                let package = array[indexPath.row]
                package.isActive = false
                package.updated = true
                tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
             }else{
                let array = self.packages.filter({$0.type == 16 && $0.isActive})
                let package = array[indexPath.row]
                package.isActive = false
                package.updated = true
                tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            }
            
        }
    }
    
//    func deletePackage(package: BLInstructor){
//        
//    }
    
}

extension DiscountsViewController {
    func fetchPackages(){
        self.showProgress()
        NetworkInterface.fetchJSON(.packages, headers: [:], params: ["userId": "\(Instructor.shared.userID!)"]) { (success, data, response, error) -> (Void) in
            DispatchQueue.main.async {
                self.hideProgress()
                if let data = data as? [String:Any], let packagesArray = data["InstructorPackage"] as? [[String:Any]] {
                    for package in packagesArray {
                        let pac = Package()
                        pac.discription = package["PackageDesc"] as? String
                        pac.rate = package["Rate"] as? Int
                        pac.hours = package["Hour"] as? Int
                        pac.name = package["PackageName"] as? String
                        pac.id = package["PackageId"] as? Int
                        pac.isActive = package["IsActive"] as? Bool ?? false
                        pac.type = package["PackageTypeId"] as? Int
                        self.packages.append(pac)
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
}

extension DiscountsViewController: PackagePriceAndHoursDelegate{
    func didSavePackage(package: Package?) {
        package?.updated = true
        package?.isActive = true
        tableView.reloadData()
    }
    func didCancelPackage(package: Package?) {
        if package?.id != nil {
            return
        }
        packages = packages.filter() { $0 !== package }
        tableView.reloadData()
    }
}




