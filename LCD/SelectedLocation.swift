//
//  SelectedLocation.swift
//  LCD
//
//  Created by Narendra Kumar R on 3/9/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import Foundation

class SelectedLocation:NSObject,NSCoding{

    static let shared = SelectedLocation()
    
    public var suburb : String?
    public var postalCode : Int?
    public var latitude : String?
    public var longitude : String?
    
    required convenience init(coder decoder: NSCoder) {
        self.init()
        self.suburb = decoder.decodeObject(forKey: "suburb") as? String
        self.postalCode = decoder.decodeObject(forKey: "postalCode") as? Int
        self.latitude = decoder.decodeObject(forKey: "latitude") as? String
        self.longitude = decoder.decodeObject(forKey: "longitude") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(suburb, forKey: "suburb")
        aCoder.encode(postalCode, forKey: "postalCode")
        aCoder.encode(latitude, forKey: "latitude")
        aCoder.encode(longitude, forKey: "longitude")
    }
    
    func synchronize(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(SelectedLocation.shared, toFile: Utils.sessionObjectPath().path)
        if !isSuccessfulSave {
            print("Failed to save user...")
        }
    }
    
    func unarchive() {
        let user = NSKeyedUnarchiver.unarchiveObject(withFile: Utils.sessionObjectPath().path) as? SelectedLocation
        suburb = user?.suburb
        postalCode = user?.postalCode
        latitude = user?.latitude
        longitude = user?.longitude
    }
    
    func delete(){
        suburb = nil
        postalCode = nil
        latitude = nil
        longitude = nil
        synchronize()
    }
    
}
