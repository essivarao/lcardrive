//
//  ButtonForCornerRadius.swift
//  LCD
//
//  Created by Admin on 16/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import Foundation

class ButtonForCornerRadius: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
    }
    
    
    
    
}
