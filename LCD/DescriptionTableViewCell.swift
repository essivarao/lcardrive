//
//  DescriptionTableViewCell.swift
//  DrivingAUS
//
//  Created by Narendra Kumar on 2/19/17.
//  Copyright © 2017 Sarella. All rights reserved.
//

import UIKit

class DescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
