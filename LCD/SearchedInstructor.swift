

import Foundation

public class SearchedInstructor {
	public var profileId : Int?
	public var userId : Int?
	public var firstName : String?
	public var lastName : String?
	public var phoneNumber : String?
	public var email : String?
	public var profilePicImageUrl : String?
	public var profilePicImage : String?
	public var schoolName : String?
	public var address : String?
	public var aboutMe : String?
	public var gender : String?
	public var carBodyTypeId : Int?
	public var transmissionTypeId : Int?
	public var experience : Int?
	public var isLicensed : Bool?
	public var ratings : String?
	public var noofRatings : Int?
	public var isActive : String?
	public var transmissionType : String?
	public var carBodyType : String?
	public var createdDate : String?
	public var createdBy : Int?
	public var modifiedDate : String?
	public var modifiedBy : Int?
    var instructorSuburbs : Array<InstructorSuburbs>?
    var drivingSchoolImages : Array<DrivingSchoolImages>?
    var instructorPackages : Array<InstructorPackages>?
	public var instructorRatings : String?
	public var serviceStatus : String?


    required public init?(dictionary: NSDictionary) {

		profileId = dictionary["ProfileId"] as? Int
		userId = dictionary["UserId"] as? Int
		firstName = dictionary["FirstName"] as? String
		lastName = dictionary["LastName"] as? String
		phoneNumber = dictionary["PhoneNumber"] as? String
		email = dictionary["Email"] as? String
		profilePicImageUrl = dictionary["ProfilePicImageUrl"] as? String
		profilePicImage = dictionary["ProfilePicImage"] as? String
		schoolName = dictionary["SchoolName"] as? String
		address = dictionary["Address"] as? String
		aboutMe = dictionary["AboutMe"] as? String
		gender = dictionary["Gender"] as? String
		carBodyTypeId = dictionary["CarBodyTypeId"] as? Int
		transmissionTypeId = dictionary["TransmissionTypeId"] as? Int
		experience = dictionary["Experience"] as? Int
		isLicensed = dictionary["IsLicensed"] as? Bool
		ratings = dictionary["Ratings"] as? String
		noofRatings = dictionary["NoofRatings"] as? Int
		isActive = dictionary["IsActive"] as? String
		transmissionType = dictionary["TransmissionType"] as? String
		carBodyType = dictionary["CarBodyType"] as? String
		createdDate = dictionary["CreatedDate"] as? String
		createdBy = dictionary["CreatedBy"] as? Int
		modifiedDate = dictionary["ModifiedDate"] as? String
		modifiedBy = dictionary["ModifiedBy"] as? Int
		if (dictionary["InstructorSuburbs"] != nil) { instructorSuburbs = InstructorSuburbs.modelsFromDictionaryArray(array: dictionary["InstructorSuburbs"] as! NSArray) }
        if let scoolImages = dictionary["DrivingSchoolImages"] as? [[String:Any]] {
            drivingSchoolImages = DrivingSchoolImages.modelsFromDictionaryArray(array: scoolImages as NSArray)
        }
        if (dictionary["InstructorPackages"] != nil) { instructorPackages = InstructorPackages.modelsFromDictionaryArray(array: dictionary["InstructorPackages"] as! NSArray) }
		instructorRatings = dictionary["InstructorRatings"] as? String
		serviceStatus = dictionary["ServiceStatus"] as? String
	}

}


