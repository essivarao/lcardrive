//
//  LCDRegisterViewController.swift
//  LCD
//
//  Created by Admin on 01/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit
import MBProgressHUD

class LCDRegisterViewController: UIViewController , UITextFieldDelegate{
    
    @IBOutlet var lastNAmeTextField: UITextField!
    @IBOutlet var firstNameTextField: UITextField!
    @IBOutlet var phoneNumberTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var registerButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewSetup()
        firstNameTextField.delegate = self
        phoneNumberTextField.delegate = self
        lastNAmeTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        phoneNumberTextField.keyboardType = .numberPad
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == firstNameTextField) || (textField == lastNAmeTextField){
            let characterSet = CharacterSet.letters
            if string.rangeOfCharacter(from: characterSet.inverted) != nil {
                return false
            }
            else if  ((textField.text?.characters.count)! >= 50  && string != ""){
                return false
            }
        }
       else if (textField == phoneNumberTextField) {
//            let characterSet = CharacterSet.decimalDigits
//            if string.rangeOfCharacter(from: characterSet.inverted) == nil {
//                return false
//            }
//            else
                if  ((textField.text?.characters.count)! >= 9  && string != ""){
                return false
            }
        }
        else if (textField == emailTextField) {
            if  ((textField.text?.characters.count)! >= 50  && string != ""){
                return false
                
            }
        }
            
        else if (textField == passwordTextField) {
            if  ((textField.text?.characters.count)! >= 30  && string != ""){
                return false
                
            }
        }
            return true
    }
    
    @IBAction func registerButtonClicked(_ sender: Any) {
        
        let firstName = self.firstNameTextField.text
        let lastName = self.lastNAmeTextField.text
        let mobileNumber = self.phoneNumberTextField.text
        let emailId = self.emailTextField.text
        let password = self.passwordTextField.text
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate

        
        if (!isValid(firstName: firstName!)) {
            appdelegate.window?.makeToast("Please enter valid first name", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        else if (!isValid(lastName: lastName!)) {
            appdelegate.window?.makeToast("Please enter valid last name", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        else if (!isValid(phoneNumber: mobileNumber!)) {
            appdelegate.window?.makeToast("Mobile number should be 9 digits", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        else if (!isValid(email: emailId!)) {
            appdelegate.window?.makeToast("Please enter valid email address", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        else if (!isValid(password: password!)) {
            appdelegate.window?.makeToast("Please enter password", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        else{
        
        let payload:Dictionary = ["FirstName": firstName,"LastName": lastName, "PhoneNumber": mobileNumber, "Email": emailId, "Password":password, "UserType": "I"] as Dictionary
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        NetworkInterface.fetchJSON(.regiseterInstructor, payload: payload) { (success, data, response, error) -> (Void) in
            DispatchQueue.main.async {
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                
                MBProgressHUD.hide(for: self.view, animated: true)
                if let serviceStatus = data?["ServiceStatus"] as? [String:Any],serviceStatus["StatusCode"] as? Int == 1,  let errorMessage = serviceStatus["ErrorMessage"] as? String {
                    appdelegate.window?.makeToast(errorMessage, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                }else
                    if let data = data, data["UserID"] as? Int != nil {
                        Instructor.shared.create(dictionary: data)
                        
                        if Instructor.shared.isInstructorEdit == true {
                            let tabvc = self.storyboard?.instantiateViewController(withIdentifier: "instructorTabBar") as! UITabBarController
                            appdelegate.window?.rootViewController = tabvc
                            tabvc.selectedIndex = 3
                        }else{
                            self.fetchInstructorProfile()
                        }
                        
                        if let serviceStatus = data["ServiceStatus"] as? [String:Any] , let successMessage = serviceStatus["ServiceMessage"] as? String {

                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                appdelegate.window?.makeToast(successMessage, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                            }

                        }
                        
                    }
                    else{
                        appdelegate.window?.makeToast("Register failed, Please try again", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                }
            }
        }
        }
    }
    
    
    func fetchInstructorProfile(){
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let params = ["userId":  "\(Instructor.shared.userID ?? 0)"]
        NetworkInterface.fetchJSON(.instructorProfile, headers: [:], params: params as NSDictionary?) { (success, data, response, error) -> (Void) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                if let serviceStatus = data?["ServiceStatus"] as? [String:Any],serviceStatus["StatusCode"] as? Int == 0,  let errorMessage = serviceStatus["ErrorMessage"] as? String {
                    appdelegate.window?.makeToast(errorMessage, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                }else if data?["ProfileId"] != nil,let data = data {
                    Instructor.shared.updateProfile(dictionary: data)
                    let tabbar = self.storyboard?.instantiateViewController(withIdentifier: "instructorTabBar") as! UITabBarController
                    appdelegate.window?.rootViewController = tabbar
                    tabbar.selectedIndex = 1
                }
                else{
                    appdelegate.window?.makeToast("Register failed, Please try again", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                }
            }
            
        }
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func displayAlert(_ title:String,message:String)  {
        let alert = UIAlertController(title: title , message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) -> Void in
            NSLog("button OK")
        }
        
        alert.addAction(defaultAction)
        present(alert, animated: true, completion:nil)  // 11
    }
}


extension LCDRegisterViewController{
    
    func  viewSetup()  {
        
        
        cancelButton.titleLabel?.font = UIFont (name:FontName.REGULAR , size: CGFloat(FontSize.BUTTON_MEDIUM))
        registerButton.titleLabel?.font = UIFont (name:FontName.REGULAR , size: CGFloat(FontSize.BUTTON_MEDIUM))
        cancelButton.backgroundColor = UIColorFromRGB(rgbValue: Color.GRAY_COLOR)
        registerButton.backgroundColor = UIColorFromRGB(rgbValue: Color.APP_THEMECOLOR)
        
    }
    
}

