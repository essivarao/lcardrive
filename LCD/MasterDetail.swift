//
//  MasterDetail.swift
//  LCD
//
//  Created by Narendra Kumar R on 3/7/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import Foundation

class MasterDetail{
    var masterDetailId: Int?
    var masterType: String?
    var masterTypeId: Int?
    var serviceStatus:Int?
    var value: String?
    init(data: [String:Any]) {
        self.masterType = data["MasterType"] as? String
        self.masterDetailId = data["MasterDetailId"] as? Int
        self.masterTypeId = data["MasterTypeId"] as? Int
        self.value = data["Value"] as? String
    }
}


class ConfigValues{
    var configKey: String?
    var configValue: String?
    init(data: [String:Any]) {
        self.configKey = data["ConfigKey"] as? String
        self.configValue = data["ConfigValue"] as? String

    }
}
