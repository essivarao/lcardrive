

import Foundation


class InstructorMessage{

	var instructor : String?
	var instructorId : Int?
	var instructorProfilePic : String?
	var isRead : Bool?
	var learner : String?
    var learnerId:Int?
	var learnerProfilePic : String?
	var lessonId : Int?
	var message : String?
	var messageDate : String?
	var messageId : Int?
	var requestId : Int?

	init(fromDictionary dictionary: NSDictionary){
		instructor = dictionary["Instructor"] as? String
		instructorId = dictionary["InstructorId"] as? Int
		instructorProfilePic = dictionary["InstructorProfilePic"] as? String
		isRead = dictionary["IsRead"] as? Bool
		learner = dictionary["Learner"] as? String
		learnerId = dictionary["LearnerId"] as? Int
		learnerProfilePic = dictionary["LearnerProfilePic"] as? String
		lessonId = dictionary["LessonId"] as? Int
		message = dictionary["Message"] as? String
		messageDate = dictionary["MessageDate"] as? String
		messageId = dictionary["MessageId"] as? Int
		requestId = dictionary["RequestId"] as? Int
	}
}
