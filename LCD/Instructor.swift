
//
//  File.swift
//  LCD
//
//  Created by kvanamac3 on 3/1/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import Foundation

enum SortInstructorsBy{
    case experience
    case rate
    case rating
}


class Instructor: NSObject, NSCoding{
    
    
    static let shared = Instructor()
    var userID : Int?
    var firstName : String?
    var lastName : String?
    var phoneNumber : String?
    var email : String?
    var userType : String?
    var loginTypeId : Int?
    var loginType : String?
    var sCNKey : String?
    var profilePicImageUrl : String?
    var isInstructorEdit : Bool?
    
    var profileId : Int?
    var profilePicImage : String?
    var schoolName : String?
    var address : String?
    var aboutMe : String?
    var gender : String?
    var carBodyTypeId : Int?
    var transmissionTypeId : Int?
    var experience : Int?
    var isLicensed : Bool?
    var ratings : String?
    var noofRatings : Int?
    var isActive : String?
    var transmissionType : String?
    var carBodyType : String?
    var createdDate : String?
    var createdBy : Int?
    var modifiedDate : String?
    var modifiedBy : String?
    var instructorSuburbs : [InstructorSuburbs]?
    var drivingSchoolImages : Array<DrivingSchoolImages>?
    var instructorPackages : Array<InstructorPackages>?
    var instructorRatings : Array<String>?
    var serviceStatus : ServiceStatus?
    
    

    required convenience init(coder decoder: NSCoder) {
        self.init()
        self.userID = decoder.decodeObject(forKey: "userID") as? Int
        self.firstName = decoder.decodeObject(forKey: "firstName") as? String
        self.lastName = decoder.decodeObject(forKey: "lastName") as? String
        self.phoneNumber = decoder.decodeObject(forKey: "phoneNumber") as? String
        self.email = decoder.decodeObject(forKey: "email") as? String
        self.userType = decoder.decodeObject(forKey: "userType") as? String
        self.loginTypeId = decoder.decodeObject(forKey: "loginTypeId") as? Int
        self.loginType = decoder.decodeObject(forKey: "loginType") as? String
        self.sCNKey = decoder.decodeObject(forKey: "sCNKey") as? String
        self.profilePicImageUrl = decoder.decodeObject(forKey: "profilePicImageUrl") as? String
        self.isInstructorEdit = decoder.decodeObject(forKey: "isInstructorEdit") as? Bool
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(userID, forKey: "userID")
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(phoneNumber, forKey: "phoneNumber")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(userType, forKey: "userType")
        aCoder.encode(loginTypeId, forKey: "loginTypeId")
        aCoder.encode(loginType, forKey: "loginType")
        aCoder.encode(sCNKey, forKey: "sCNKey")
        aCoder.encode(profilePicImageUrl, forKey: "profilePicImageUrl")
        aCoder.encode(isInstructorEdit, forKey: "isInstructorEdit")
    }
    
    func updateProfile(dictionary: NSDictionary) {
        
        profileId = dictionary["ProfileId"] as? Int
        firstName = dictionary["FirstName"] as? String
        lastName = dictionary["LastName"] as? String
        phoneNumber = dictionary["PhoneNumber"] as? String
        email = dictionary["Email"] as? String
        profilePicImageUrl = dictionary["ProfilePicImageUrl"] as? String
        profilePicImage = dictionary["ProfilePicImage"] as? String
        schoolName = dictionary["SchoolName"] as? String
        address = dictionary["Address"] as? String
        aboutMe = dictionary["AboutMe"] as? String
        gender = dictionary["Gender"] as? String
        carBodyTypeId = dictionary["CarBodyTypeId"] as? Int
        transmissionTypeId = dictionary["TransmissionTypeId"] as? Int
        experience = dictionary["Experience"] as? Int
        isLicensed = dictionary["IsLicensed"] as? Bool
        ratings = dictionary["Ratings"] as? String
        noofRatings = dictionary["NoofRatings"] as? Int
        isActive = dictionary["IsActive"] as? String
        transmissionType = dictionary["TransmissionType"] as? String
        carBodyType = dictionary["CarBodyType"] as? String
        createdDate = dictionary["CreatedDate"] as? String
        createdBy = dictionary["CreatedBy"] as? Int
        modifiedDate = dictionary["ModifiedDate"] as? String
        modifiedBy = dictionary["ModifiedBy"] as? String
        
        if let suburbs = dictionary["InstructorSuburbs"] as? [[String:Any]] {
            instructorSuburbs = InstructorSuburbs.modelsFromDictionaryArray(array: suburbs as NSArray)
        }
        
        if let scoolImages = dictionary["DrivingSchoolImages"] as? [[String:Any]] {
            drivingSchoolImages = DrivingSchoolImages.modelsFromDictionaryArray(array: scoolImages as NSArray)
        }
        
        if let packages = dictionary["InstructorPackages"] as? [[String:Any]] {
            instructorPackages = InstructorPackages.modelsFromDictionaryArray(array: packages as NSArray)
        }

    }
    
    
    
    func create(dictionary: NSDictionary) {
        userID = dictionary["UserID"] as? Int
        firstName = dictionary["FirstName"] as? String
        lastName = dictionary["LastName"] as? String
        phoneNumber = dictionary["PhoneNumber"] as? String
        email = dictionary["Email"] as? String
        userType = dictionary["UserType"] as? String
        loginTypeId = dictionary["LoginTypeId"] as? Int
        loginType = dictionary["LoginType"] as? String
        sCNKey = dictionary["SCNKey"] as? String
        profilePicImageUrl = dictionary["ProfilePicImageUrl"] as? String
        isInstructorEdit = dictionary["IsInstructorEdit"] as? Bool
    }
    
    
    func logout(){
        userID = nil
        firstName = nil
        lastName = nil
        phoneNumber = nil
        email = nil
        userType = nil
        loginTypeId = nil
        loginType = nil
        sCNKey = nil
        profilePicImageUrl = nil
        isInstructorEdit = nil
        profileId = nil
        profilePicImage = nil
        schoolName = nil
        address = nil
        aboutMe = nil
        gender = nil
        carBodyTypeId = nil
        transmissionTypeId = nil
        experience = nil
        isLicensed = nil
        ratings = nil
        noofRatings = nil
        isActive = nil
        transmissionType = nil
        carBodyType = nil
        createdDate = nil
        createdBy = nil
        modifiedDate = nil
        modifiedBy = nil
        instructorSuburbs = nil
        drivingSchoolImages = nil
        instructorPackages = nil
        instructorRatings = nil
        serviceStatus = nil
        SelectedLocation.shared.delete()
        self.synchronize()
        
    }
 
}

extension Instructor {
    
    func synchronize(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(Instructor.shared, toFile: Utils.instructorSessionObjectPath().path)
        if !isSuccessfulSave {
            print("Failed to save user...")
        }
    }
    
    func unarchive() {
        let user = NSKeyedUnarchiver.unarchiveObject(withFile: Utils.instructorSessionObjectPath().path) as? Instructor
        userID = user?.userID
        firstName = user?.firstName
        lastName = user?.lastName
        phoneNumber = user?.phoneNumber
        email = user?.email
        userType = user?.userType
        loginTypeId = user?.loginTypeId
        loginType = user?.loginType
        sCNKey = user?.sCNKey
        profilePicImageUrl = user?.profilePicImageUrl
        isInstructorEdit = user?.isInstructorEdit
    }
}

class InstructorSuburbs {
    public var suburbId : Int?
    public var profileId : Int?
    public var suburb : String?
    public var postalCode : String?
    public var latitude : String?
    public var longitude : String?
    public var isActive : Bool?
    public var createdDate : String?
    public var createdBy : Int?
    public var modifiedDate : String?
    public var modifiedBy : String?
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [InstructorSuburbs]
    {
        var models:[InstructorSuburbs] = []
        for item in array
        {
            models.append(InstructorSuburbs(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    init() {
        
    }
    
    required public init?(dictionary: NSDictionary) {
        
        suburbId = dictionary["SuburbId"] as? Int
        profileId = dictionary["ProfileId"] as? Int
        suburb = dictionary["Suburb"] as? String
        postalCode = dictionary["PostalCode"] as? String
        latitude = dictionary["Latitude"] as? String
        longitude = dictionary["Longitude"] as? String
        isActive = dictionary["IsActive"] as? Bool
        createdDate = dictionary["CreatedDate"] as? String
        createdBy = dictionary["CreatedBy"] as? Int
        modifiedDate = dictionary["ModifiedDate"] as? String
        modifiedBy = dictionary["ModifiedBy"] as? String
    }
    
}

class DrivingSchoolImages {
    public var imageId : Int?
    public var userId : Int?
    public var imageName : String?
    public var image : String?
    public var imageUrl : String?
    public var isActive : Bool?
    public var serviceStatus : String?
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [DrivingSchoolImages]
    {
        var models:[DrivingSchoolImages] = []
        for item in array
        {
            models.append(DrivingSchoolImages(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    init() {
        
    }
    
    required public init?(dictionary: NSDictionary) {
        
        imageId = dictionary["ImageId"] as? Int
        userId = dictionary["UserId"] as? Int
        imageName = dictionary["ImageName"] as? String
        image = dictionary["Image"] as? String
        imageUrl = dictionary["ImageUrl"] as? String
        isActive = dictionary["IsActive"] as? Bool
        serviceStatus = dictionary["ServiceStatus"] as? String
    }
    
}

class InstructorPackages {
    public var packageId : Int?
    public var userId : Int?
    public var packageName : String?
    public var packageDesc : String?
    public var rate : Int?
    public var hour : Int?
    public var rateModeId : Int?
    public var rateMode : String?
    public var packageTypeId : Int?
    public var packageType : String?
    public var isActive : String?
    public var createdDate : String?
    public var createdBy : Int?
    public var modifiedDate : String?
    public var modifiedBy : String?
    
    public var ratePerHours: String{
        if hour == 0 {
            return "\(rate ?? 0)/Hour"
        }else if hour != nil{
            return "\(rate ?? 0)/(\(hour!) Hour"
        }else{
            return "\(rate ?? 0)/Hour"
        }
    }
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [InstructorPackages]
    {
        var models:[InstructorPackages] = []
        for item in array
        {
            models.append(InstructorPackages(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    
    required public init?(dictionary: NSDictionary) {
        
        packageId = dictionary["PackageId"] as? Int
        userId = dictionary["UserId"] as? Int
        packageName = dictionary["PackageName"] as? String
        packageDesc = dictionary["PackageDesc"] as? String
        rate = dictionary["Rate"] as? Int
        hour = dictionary["Hour"] as? Int
        rateModeId = dictionary["RateModeId"] as? Int
        rateMode = dictionary["RateMode"] as? String
        packageTypeId = dictionary["PackageTypeId"] as? Int
        packageType = dictionary["PackageType"] as? String
        isActive = dictionary["IsActive"] as? String
        createdDate = dictionary["CreatedDate"] as? String
        createdBy = dictionary["CreatedBy"] as? Int
        modifiedDate = dictionary["ModifiedDate"] as? String
        modifiedBy = dictionary["ModifiedBy"] as? String
    }
}




