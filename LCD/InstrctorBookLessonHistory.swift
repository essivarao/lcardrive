

import Foundation



class LessonPackage {
    var rate : Int?
    var rateMode : String?
    var packageType : String?
    var packageTypeId : Int?
    var packageName : String?
    var packageDesc : String?
    var packageId : Int?
    var rateModeId : Int?
    var hour: Int?
    required  init?(dictionary: NSDictionary) {
        
        rate = dictionary["Rate"] as? Int
        rateMode = dictionary["RateMode"] as? String
        packageType = dictionary["PackageType"] as? String
        packageTypeId = dictionary["PackageTypeId"] as? Int
        packageName = dictionary["PackageName"] as? String
        packageDesc = dictionary["PackageDesc"] as? String
        packageId = dictionary["PackageId"] as? Int
        rateModeId = dictionary["RateModeId"] as? Int
        hour = dictionary["hour"] as? Int
    }
}
 
class InstrctorBookLessonHistory {
	 var learnerStatus : String?
	 var instructorId : Int?
	 var postalCode : Int?
	 var learnerProfilePic : String?
	 var comments : String?
	 var isInstructor : Bool?
	 var instructorStatus : String?
	 var learnerPhoneNumber : String?
	 var learnerId : Int?
	 var package : LessonPackage?
	 var lessonTypeId : Int?
	 var lessonType : String?
	 var longitude : String?
	 var lessonDate : String?
	 var rateModeTypeId : Int?
	 var learnerName : String?
	 var lessonId : Int?
	 var rate : Int?
	 var rateModeType : String?
	 var latitude : String?
	 var status : String?
	 var suburb : String?

	required public init?(dictionary: NSDictionary) {

		learnerStatus = dictionary["LearnerStatus"] as? String
		instructorId = dictionary["InstructorId"] as? Int
		postalCode = dictionary["PostalCode"] as? Int
		learnerProfilePic = dictionary["LearnerProfilePic"] as? String
		comments = dictionary["Comments"] as? String
		isInstructor = dictionary["IsInstructor"] as? Bool
		instructorStatus = dictionary["InstructorStatus"] as? String
		learnerPhoneNumber = dictionary["LearnerPhoneNumber"] as? String
		learnerId = dictionary["LearnerId"] as? Int
		if (dictionary["Package"] != nil) { package = LessonPackage(dictionary: dictionary["Package"] as! NSDictionary) }
		lessonTypeId = dictionary["LessonTypeId"] as? Int
		lessonType = dictionary["LessonType"] as? String
		longitude = dictionary["Longitude"] as? String
		lessonDate = dictionary["LessonDate"] as? String
		rateModeTypeId = dictionary["RateModeTypeId"] as? Int
		learnerName = dictionary["LearnerName"] as? String
		lessonId = dictionary["LessonId"] as? Int
		rate = dictionary["Rate"] as? Int
		rateModeType = dictionary["RateModeType"] as? String
		latitude = dictionary["Latitude"] as? String
		status = dictionary["Status"] as? String
		suburb = dictionary["Suburb"] as? String
	}


}
