//
//  AppDelegate.swift
//  LCD
//
//  Created by Narendra Kumar on 2/28/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GooglePlaces
import GoogleMaps
import Firebase
import FirebaseMessaging
import UserNotifications
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    /// The callback to handle data message received via FCM for devices running iOS 10 or above.
    
    
    var deviceToken: String?
    var window: UIWindow?
    var masterDetails: [MasterDetail] = []
    var configValues: [ConfigValues] = []
    
    class var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    let gcmMessageIDKey = "gcm.message_id"
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        GMSServices.provideAPIKey("AIzaSyB2ln4B_uA5z1dvWn5WwoBd_LtKqePQLI4")
        GMSPlacesClient.provideAPIKey("AIzaSyBUudcG2guHkghjTIfTpkWoPYsmHLgCMfU")
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // [END register_for_notifications]
        FIRApp.configure()
        
        // [START add_token_refresh_observer]
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: .firInstanceIDTokenRefresh,
                                               object: nil)
        
        
        SelectedLocation.shared.unarchive()
        
        if let mastrDetailsArray = MasterDetalsManager.masterDetails(), let configArray = MasterDetalsManager.config() {
           self.masterDetails.removeAll()
            mastrDetailsArray.forEach({ (dataObj) in
                let obj = MasterDetail.init(data: dataObj)
                self.masterDetails.append(obj)
            })
            self.configValues.removeAll()
            configArray.forEach({ (dataObj) in
                let obj = ConfigValues.init(data: dataObj)
                self.configValues.append(obj)
            })
            self.setupRootFromLaunch(application: application, launchOptions: launchOptions)
        }else{
            
            //self.window?.rootViewController?.showProgress()

            showSplash()
            updateMasterDetails {
                DispatchQueue.main.async {
                    self.window?.rootViewController?.hideProgress()
                    self.setupRootForNoSession()
                    self.setupRootFromLaunch(application: application, launchOptions: launchOptions)
                }
            }
        }
        window?.makeKeyAndVisible()
        return true
    }
    
    func updateMasterDetails(completion:@escaping ()->Void){
        NetworkInterface.fetchJSON(.masterProfile) { (success, data, reponse, error) -> (Void) in
            DispatchQueue.main.async {
                
                if let data = data as? [String:Any], let mastrDetailsArray = data["MasterDetail"] as? [[String:Any]] {
                    MasterDetalsManager.saveMasterDetails(array: mastrDetailsArray as NSArray)
                    self.masterDetails.removeAll()
                    mastrDetailsArray.forEach({ (dataObj) in
                        let obj = MasterDetail.init(data: dataObj)
                        self.masterDetails.append(obj)
                    })
                }
                
                if let data = data as? [String:Any], let configArray = data["ConfigValues"] as? [[String:Any]] {
                    self.configValues.removeAll()
                    
                    configArray.forEach({ (dataObj) in
                        let obj = ConfigValues.init(data: dataObj)
                        self.configValues.append(obj)
                    })
                    MasterDetalsManager.saveConfig(array: configArray)
                }
                completion()}
        }
    }
    
    
    
    func setupRootFromLaunch(application: UIApplication, launchOptions: [UIApplicationLaunchOptionsKey: Any]?){
        IQKeyboardManager.sharedManager().enable = true
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        Learner.shared.unarchive()
        if Learner.shared.userID != nil {
            self.learnerWithSession()
            self.checkLernerNeedToRate()
        }
        Instructor.shared.unarchive()
        if Instructor.shared.userID != nil {
            self.instructorWithSession()
        }
        
        if let remoteNotif = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable : Any] {
            self.handlePush(userInfo: remoteNotif)
        }
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        //        if url.scheme == "fb1024236167681457" {
        //
        //        }
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        print("AppDelegate -> application:openURL: \(String(describing: url.scheme))")
        
        // Facebook Signin
        if (url.scheme?.hasPrefix("fb"))!  {
            
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
            //            return true
            
        } else {
            return false
        }
    }
    
    func learnerWithSession(){
        let storyBoard = UIStoryboard.init(name: "Learner", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "leaenerTabbar") as! UITabBarController
        vc.selectedIndex = 1
        self.window?.rootViewController = vc
    }
    
    func showSplash(){
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SplashScreenViewController") as! SplashScreenViewController
        self.window?.rootViewController = vc
    }
    
    func instructorWithSession(){
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "instructorTabBar") as! UITabBarController
        vc.selectedIndex = 1
        self.window?.rootViewController = vc
    }
    
    func setupRootForNoSession(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "navigationController") as! NavigationViewController
        self.window?.rootViewController = viewController
    }
    
    func checkLernerNeedToRate(){
        let params =  ["LearnerId":"\(Learner.shared.userID ?? 0)"]
        //let params = ["LearnerId": "166"]
        NetworkInterface.fetchJSON(.checkLernerNeedRating, params: params as NSDictionary?) { (success, data, response, error) -> (Void) in
            DispatchQueue.main.async {
                if let data = data as? [String:Any],
                    let ServiceStatus = data["ServiceStatus"] as? [String:Any],
                    ServiceStatus["StatusCode"] as? Int == 0,
                    let lessonId = data["LessonId"] as? Int, let instructorId = data["InstructorId"] as? Int {
                    if let currentVCRoot = UIApplication.shared.keyWindow?.rootViewController,let currentVC = UIViewController.getCurrentViewController(vc: currentVCRoot) {
                        let vc : SubmitReviewViewController =  UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SubmitReviewViewController") as! SubmitReviewViewController
                        vc.lessonId = lessonId
                        vc.instructorId = instructorId
                        currentVC.present(vc, animated: true, completion: {
                            
                        })
                    }
                }
            }
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        if application.applicationState == .inactive {
            handlePush(userInfo: userInfo)
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        FIRMessaging.messaging().appDidReceiveMessage(userInfo)
        
        handlePush(userInfo: userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }
        connectToFcm()
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        guard let token = FIRInstanceID.instanceID().token() else {
            return
        }
        
        FIRMessaging.messaging().disconnect()
        
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM. Token \(token)")
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the InstanceID token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        self.deviceToken = deviceToken.base16EncodedString(uppercase : true)
        // With swizzling disabled you must set the APNs token here.
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.unknown)
    }
    
    // [START connect_on_active]
    func applicationDidBecomeActive(_ application: UIApplication) {
        connectToFcm()
        updateMasterDetails {
            
        }
    }
    // [END connect_on_active]
    // [START disconnect_from_fcm]
    func applicationDidEnterBackground(_ application: UIApplication) {
        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }
}

@available(iOS 10, *)
extension AppDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // Print message ID.
        print("Message ID: \(userInfo["gcm.message_id"]!)")
        completionHandler([.alert,.sound])
        // Print full message.
        print("%@", userInfo)
    }
    
    @available(iOS 10, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Userinfo \(response.notification.request.content.userInfo)")
        handlePush(userInfo: response.notification.request.content.userInfo)
    }
}

extension AppDelegate : FIRMessagingDelegate {
    // Receive data message on iOS 10 devices.
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print("%@", remoteMessage.appData)
    }
}

