//
//  ViewController.swift
//  LCD
//
//  Created by Admin on 09/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var iamLabel: UILabel!
    @IBOutlet var learnerButtonLabel: UILabel!
    @IBOutlet var learnerButtonView: UIView!
    @IBOutlet var drivingInstrucorButtonView: UIView!
    @IBOutlet var drivingButtonLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSetup()
        // Do any additional setup after loading the view.
    }
    
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }



}

extension ViewController{
    func  viewSetup()  {
        iamLabel.font = UIFont(name: FontName.LIGHT, size: CGFloat(FontSize.LABEL_LARGE))
        learnerButtonLabel.font = UIFont(name: FontName.LIGHT, size: CGFloat(FontSize.LABEL_MEDIUM))
        drivingButtonLabel.font = UIFont(name: FontName.LIGHT, size: CGFloat(FontSize.LABEL_MEDIUM))
        learnerButtonView.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        drivingInstrucorButtonView.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        drivingInstrucorButtonView.backgroundColor = UIColorFromRGB(rgbValue: Color.APP_THEMECOLOR)
        learnerButtonView.backgroundColor = UIColorFromRGB(rgbValue: Color.BUTTON_LIGHT_COLOR)
        
        
    }
    
}
