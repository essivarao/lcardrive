//
//  LearnerSearchViewController.swift
//  LCD
//
//  Created by Admin on 02/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit
import MBProgressHUD
import GooglePlaces

class LearnerSearchViewController: UIViewController , UITextFieldDelegate , UITableViewDelegate , UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var sortByView: UIView!
    @IBOutlet var experienceButton: UIButton!
    @IBOutlet var rateButton: UIButton!
    @IBOutlet var ratingsButton: UIButton!
    @IBOutlet var suberbNameLabel: UILabel!
    var totalUsers:[SearchedInstructor] = []
    
    var sortBy: SortInstructorsBy = .experience
    
    var filterdUsers:[SearchedInstructor] = []


    override func viewDidLoad() {
        super.viewDidLoad()
        SearchFilterManager.shared.unarchive()


        searchTextField.delegate = self
        sortByView.isHidden = true
        sortByView.layer.cornerRadius = 10
        // Do any additional setup after loading the view.
        searchTextField.addTarget(self, action: #selector(LearnerSearchViewController.textFieldDidChange(textField:)), for: .editingChanged)

        
        experienceButton.imageEdgeInsets = UIEdgeInsetsMake(0, 120, 0, 15)
        experienceButton.titleEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        experienceButton.setImage(#imageLiteral(resourceName: "ic_done"), for: .normal)
        
        sortByView.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        sortByView.layer.borderWidth = 1
        sortByView.layer.borderColor = UIColor.lightGray.cgColor

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        applyFilterAndSort(searchText: searchTextField.text!)
        suberbNameLabel.text = SelectedLocation.shared.suburb ?? ""


    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let user = SearchUserDetailWithOutSignInViewController.selectedUserInMemory {
            
            if SearchUserDetailWithOutSignInViewController.selectedType == "request" {
                let storyBoard = UIStoryboard.init(name: "Learner", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "preferredDaysSelectionViewController") as! PreferredDaysSelectionViewController
                let request = SendRequest()
                request.instructorImageURL = Learner.shared.profilePicImageUrl
                request.userId = Learner.shared.userID
                request.instructorId = user.userId
                request.learnerId = Learner.shared.userID
                vc.request = request
                vc.selectedUser = user
                self.navigationController?.pushViewController(vc, animated: false)
            }else{
                let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "InstructorBookLess") as! InstructorBookLessViewController
                let request = BookLessonRequest()
                request.imageURL = user.profilePicImageUrl
                request.InstructorId = user.userId
                request.LearnerId = Learner.shared.userID
                request.UserId = Learner.shared.userID
                request.instructorName = (user.firstName ?? "") + " " + (user.lastName ?? "")
                vc.request = request
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }else{
            fetchInstrocturs()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchInstrocturs(){
        
        
        var payload:[String:Any] = [:]
        
        guard let code = SelectedLocation.shared.postalCode else{
            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "suburbSearchViewController") as! SuburbSearchViewController
            self.navigationController?.present(vc, animated: true, completion: nil)
            return
        }
        
        payload["PostalCode"] = "\(code)"
        
        MBProgressHUD.showAdded(to: self.view, animated: true)

        
        NetworkInterface.fetchJSON(.instructosSearchlist, headers: [:], params: [:], payload: payload) { (success, data, response, error) -> (Void) in
            if let list = data?["InstructorsProfile"] as? [[String:Any]] {
                self.totalUsers.removeAll()
                for (_,user) in list.enumerated() {
                    if let inst = SearchedInstructor.init(dictionary: user as NSDictionary) {
                        self.totalUsers.append(inst)
                    }
                }
            }else{
                // Error
            }
            DispatchQueue.main.async {
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                self.applyFilterAndSort(searchText: self.searchTextField.text!)
                
            }
        }
    }

    
    //TableView
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterdUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LearnerSearchTableViewCell") as? LearnerSearchTableViewCell
        let user = filterdUsers[indexPath.row]
        cell?.nameTextField.text = (user.firstName ?? "") + " " + (user.lastName ?? "")
        cell?.drivingSchoolLabel.text = user.schoolName ?? "School Name Not Available"
        cell?.maleExpLabel.text = "\(user.gender!.lowercased() == "m" ? "Male" : "Female"), Exp: \(user.experience ?? 0)yrs"
        cell?.modeLabel.text = "Mode : \(user.transmissionType ?? "")"
        cell?.ratingLabel.text = user.ratings ?? "-/-"
        cell?.ratingLabel.backgroundColor = UIColorFromRGB(rgbValue: 0x19BC9D)
        cell?.noOfRatingsLabel.text = "( \(user.noofRatings ?? 0) Ratings )"
        
        cell?.callButton.tag = indexPath.row
        cell?.callButton.addTarget(self, action: #selector(LearnerSearchViewController.callButtonClicked(sender:)), for: .touchUpInside)

        
        if (cell?.noOfRatingsLabel.text == "( 0 Ratings )") {
            cell?.noOfRatingsLabel.text = ""
            cell?.ratingLabel.backgroundColor = UIColor.clear
        }
        
        cell?.ratePerHourLabel.text = "$\(0) /hr"
        if let instructorPackages = user.instructorPackages {
            for package in instructorPackages {
                if package.hour == 1 {
                    cell?.ratePerHourLabel.text = "$\(package.rate ?? 0) /hr"
                    break
                }
            }
            
            
        }
        
        if let imageURL = user.profilePicImageUrl {
            let url = URL(string: imageURL)
            cell?.profileImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultFace"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        cell?.carTypeLabel.text = "Car type: " + (user.carBodyType ?? "")
        
        if let packages = user.instructorPackages {
            
            
            cell?.priceOneLabel.backgroundColor = UIColor.clear
            cell?.pricetwoLabel.backgroundColor = UIColor.clear
            cell?.priceThreeLabel.backgroundColor = UIColor.clear
            
            cell?.priceOneLabel.text = ""
            cell?.pricetwoLabel.text = ""
            cell?.priceThreeLabel.text = ""
//
//            if packages.count > 0 {
//                let package = packages.first
//                cell?.priceOneLabel.text = "$\(package?.rate ?? 0) / \(package?.hour == 1 ? "Hour" : "\(package?.hour ?? 0) Hours")"
//                cell?.priceOneLabel.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
//                cell?.priceOneLabel.layer.masksToBounds = true
//                cell?.priceOneLabel.backgroundColor = UIColor.lightGray
//              
//            }
//            if packages.count > 1 {
//                let package = packages[1]
//
//                cell?.pricetwoLabel.text = "$\(package.rate ?? 0) / \(package.hour == 1 ? "Hour" : "\(package.hour ?? 0) Hours")"
//                cell?.pricetwoLabel.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
//                cell?.pricetwoLabel.layer.masksToBounds = true
//                cell?.pricetwoLabel.backgroundColor = UIColor.lightGray
//                
//                
//            }
//            if packages.count > 2 {
//                let package = packages[2]
//
//                cell?.priceThreeLabel.text = "$\(package.rate ?? 0) / \(package.hour == 1 ? "Hour" : "\(package.hour ?? 0) Hours")"
//                cell?.priceThreeLabel.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
//                cell?.priceThreeLabel.layer.masksToBounds = true
//                cell?.priceThreeLabel.backgroundColor = UIColor.lightGray                
//            }
        }
        
        
        cell?.selectionStyle = .none
        return cell!
    }
    
    func callButtonClicked(sender : UIButton) {
        let user = filterdUsers[sender.tag]
     let phoneNum = "+61\(user.phoneNumber!)"
        if let url = URL(string: "telprompt://\(phoneNum)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
        else {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.window?.makeToast("Phone number not valid.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyBoard = UIStoryboard.init(name: "Learner", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "LearnerSearchDetialViewController") as! LearnerSearchDetialViewController
        vc.selectedUser = filterdUsers[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func filtersButtonClicked(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let filtersView = storyBoard.instantiateViewController(withIdentifier: "LCDFiltersView")
        self.navigationController?.pushViewController(filtersView, animated: true)
    }

    @IBAction func soryByClicked(_ sender: Any) {
        sortByView.isHidden = false

    }
    @IBAction func experienceClicked(_ sender: UIButton) {
        sortByView.isHidden = true
        
        sortBy = .experience
        applyFilterAndSort(searchText: searchTextField.text!)
        sender.setImage(#imageLiteral(resourceName: "ic_done"), for: .normal)
        
        rateButton.setImage(nil, for: .normal)
        ratingsButton.setImage(nil, for: .normal)
        
        experienceButton.imageEdgeInsets = UIEdgeInsetsMake(0, 120, 0, 15)
        experienceButton.titleEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        
        rateButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
        ratingsButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)

    }
    @IBAction func ratingsClicked(_ sender: UIButton) {
        sortByView.isHidden = true
       
                        sortBy = .rating
                applyFilterAndSort(searchText: searchTextField.text!)
        sender.setImage(#imageLiteral(resourceName: "ic_done"), for: .normal)
        
        experienceButton.setImage(nil, for: .normal)
        rateButton.setImage(nil, for: .normal)
        
        
        
        ratingsButton.imageEdgeInsets = UIEdgeInsetsMake(0, 120, 0, 15)
        ratingsButton.titleEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        
        experienceButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
        rateButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)


    }
    @IBAction func rateClicked(_ sender: UIButton) {
        sortByView.isHidden = true
        
        sortBy = .rate
        applyFilterAndSort(searchText: searchTextField.text!)
        sender.setImage(#imageLiteral(resourceName: "ic_done"), for: .normal)
        
        experienceButton.setImage(nil, for: .normal)
        ratingsButton.setImage(nil, for: .normal)
        
        rateButton.imageEdgeInsets = UIEdgeInsetsMake(0, 120, 0, 15)
        rateButton.titleEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        
        experienceButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
        ratingsButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)


    }
    
    func applyFilterAndSort(searchText: String){
        filterdUsers.removeAll()
        if searchText.characters.count > 0 {
            filterdUsers = totalUsers.filter({ (($0.firstName ?? "") + " " + ($0.lastName ?? "")).lowercased().range(of: searchText.lowercased()) != nil })
        }else{
            filterdUsers.append(contentsOf: totalUsers)
        }
        
        if let type  = SearchFilterManager.shared.carType {
            switch type  {
            case .hatchback:
                filterdUsers = filterdUsers.filter({$0.carBodyType?.lowercased() == "Hatchback".lowercased()})
                break
            case .sedan:
                filterdUsers = filterdUsers.filter({$0.carBodyType?.lowercased() == "Sedan".lowercased()})
                break
            case .suv:
                filterdUsers = filterdUsers.filter({$0.carBodyType?.lowercased() == "Suv".lowercased()})
                break
            }
        }
        
        if let type  = SearchFilterManager.shared.genderType {
            switch type  {
            case .any:
                break
            case .male:
                filterdUsers = filterdUsers.filter({$0.gender?.lowercased() == "m"})
                break
            case .female:
                filterdUsers = filterdUsers.filter({$0.gender?.lowercased() == "f"})
                break
            }
        }
        
        if let type  = SearchFilterManager.shared.transmittionTpe {
            switch type  {
            case .both:
                filterdUsers = filterdUsers.filter({$0.transmissionType?.lowercased() == "both"})
                break
            case .auto:
                filterdUsers = filterdUsers.filter({$0.transmissionType?.lowercased() == "automatic"})
                break
            case .manual:
                filterdUsers = filterdUsers.filter({$0.transmissionType?.lowercased() == "manual"})
                break
            }
        }
        

        
        
        filterdUsers = filterdUsers.sorted { (user1, user2) -> Bool in
            switch sortBy {
            case .experience:
                return user1.experience ?? 0 > user2.experience ?? 0
            case .rate:
                return user1.instructorPackages?.filter({$0.hour == 1}).first?.rate ?? 0 > user2.instructorPackages?.filter({$0.hour == 1}).first?.rate ?? 0
            case .rating:
                return user1.noofRatings ?? 0 > user2.noofRatings ?? 0
            }
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    @IBAction func searchSuburbClicked(_ sender: Any) {
        let googleAutoVC = GMSAutocompleteViewController.init()
        googleAutoVC.delegate = self
        googleAutoVC.view.backgroundColor = UIColor.clear
        let filter = GMSAutocompleteFilter.init()
        filter.country = "AU"
        filter.type = .region
        googleAutoVC.autocompleteFilter = filter
        
        self.present(googleAutoVC, animated: true) {
            
        }
    }

}

extension LearnerSearchViewController {
    func textFieldDidChange(textField: UITextField) {
        applyFilterAndSort(searchText: textField.text!)
    }
}

extension LearnerSearchViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        let location = SelectedLocation.shared
        location.latitude = "\(place.coordinate.latitude)"
        location.longitude = "\(place.coordinate.longitude)"

        let suburbString = place.formattedAddress?.replacingOccurrences(of: ", Australia", with: "")
        location.suburb = suburbString
        
        if let postalCodeComponent =  place.addressComponents?.filter({$0.type == "postal_code"}).first {
            location.postalCode = Int(postalCodeComponent.name)
        }
        location.synchronize()
        
        viewController.dismiss(animated: true) {
            self.fetchInstrocturs()
        }
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error.localizedDescription)")
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        viewController.dismiss(animated: true, completion: nil)
    }
}
