//
//  LCDSignInViewController.swift
//  LCD
//
//  Created by Admin on 01/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit
import MBProgressHUD
import Firebase

class LCDSignInViewController: UIViewController {
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var forgetButton: UIButton!
    @IBOutlet var newhereLabel: UILabel!
    @IBOutlet var registerButton: UIButton!
    let facebookButton = FBSDKLoginButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSetup()
   
        facebookButton.readPermissions = ["public_profile","email"]
        facebookButton.delegate = self
        
        let fbButton = UIButton.init(frame: CGRect(x: 44, y: passwordTextField.frame.size.height + passwordTextField.frame.origin.y + 400 , width: self.view.frame.size.width - 88, height: 40))
        fbButton.center.x = self.view.center.x
        fbButton.titleLabel?.font = UIFont(name: FontName.REGULAR, size: CGFloat(FontSize.BUTTON_MEDIUM))
        fbButton.backgroundColor = UIColorFromRGB(rgbValue: 0x3b5998)
        fbButton.layer.cornerRadius = CGFloat(CornerRadius.BUTTON_CORNERRADIUS)
        self.view.addSubview(fbButton)
        fbButton.setTitle("Login with Facebook", for: .normal)
        fbButton.addTarget(self, action: #selector(LCDSignInViewController.facebookButtonAction), for: .touchUpInside)
        
        
        
        let facebookBurronImage = UIImageView()
        facebookBurronImage.frame = CGRect(x: 40, y: 5, width: 30, height: 30)
        facebookBurronImage.image = #imageLiteral(resourceName: "facebookBurron")
        fbButton.addSubview(facebookBurronImage)
        fbButton.titleEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        
        
        if FBSDKAccessToken.current() != nil {
            FBSDKLoginManager().logOut()
        }
        
        // Do any additional setup after loading the view.
    }
    
    func facebookButtonAction(){
        facebookButton.sendActions(for: .touchUpInside)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    @IBAction func logInButtonClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate

       
            
        if  (!isValid(email: emailTextField.text!)) && (!isValid(signInPhoneNumber: emailTextField.text!)) {
            appdelegate.window?.makeToast("Email/Phone number required.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)

            return
        }
        else if (!isValid(password: passwordTextField.text!)) {
//            displayAlert("ERROR", message: "INVALID PASSWORD")
            appdelegate.window?.makeToast("Please enter password.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)

            return
        }
        else{
            MBProgressHUD.showAdded(to: self.view, animated: true)
            var params:Dictionary = ["UserName": emailTextField.text!, "Password":passwordTextField.text!,   "UserType": "I"] as [String:Any]
            if let firToken = FIRInstanceID.instanceID().token() {
                params["FCMRegistrationId"] = firToken
                params["DeviceToken"] = AppDelegate.shared.deviceToken ?? ""
                params["DeviceTypeId"] = 18
            }
            
            NetworkInterface.fetchJSON(.login, headers: [:], params: [:], payload: params) { (success, data, response, error) -> (Void) in
                DispatchQueue.main.async {
                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let data = data, let userid = data["UserID"] as? Int, userid != 0 {
                        Instructor.shared.create(dictionary: data)
                        Instructor.shared.synchronize()
                        if Instructor.shared.isInstructorEdit == true {
                            let tabvc = self.storyboard?.instantiateViewController(withIdentifier: "instructorTabBar") as! UITabBarController
                            appdelegate.window?.rootViewController = tabvc
                            tabvc.selectedIndex = 3
                        }else{
                            self.fetchInstructorProfile()
                        }
                    }else{
                        appdelegate.window?.makeToast("Login failed, invalid user credentials", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    }
                }
            }
        }
    }
    
    func fetchInstructorProfile(){
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let params = ["userId":  "\(Instructor.shared.userID ?? 0)"]
        NetworkInterface.fetchJSON(.instructorProfile, headers: [:], params: params as NSDictionary?) { (success, data, response, error) -> (Void) in
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                
                if data?["ProfileId"] != nil,let data = data {
                    Instructor.shared.updateProfile(dictionary: data)
                    let tabBar = self.storyboard?.instantiateViewController(withIdentifier: "instructorTabBar") as! UITabBarController
                    appdelegate.window?.rootViewController = tabBar
                    tabBar.selectedIndex = 1
                }else{
                    Instructor.shared.logout()
                    appdelegate.window?.makeToast("failed to fetch profile, invalid user credentials", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                }
            }
            
        }
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func displayAlert(_ title:String,message:String)  {
        let alert = UIAlertController(title: title , message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) -> Void in
            NSLog("button OK")
        }
        
        alert.addAction(defaultAction)
        present(alert, animated: true, completion:nil)  // 11
    }
    
    @IBAction func facebookLoginClicked(_ sender: Any) {
    }
    
}

extension LCDSignInViewController: FBSDKLoginButtonDelegate {
    
    public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
        guard error == nil else {
            AppDelegate.shared.window?.makeToast("Error on facebook signin. Please try again.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            self.hideProgress()
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        //        let params = ["fields":"picture,email,name"]
        let params = ["fields":"picture,email,first_name,last_name"]
        
        let req:FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: params)
        req.start { (reqConnection, result, err) in
            
            guard let result = result as? [String:AnyObject],
                let email = result["email"] as? String,
                let first_name = result["first_name"] as? String,
                let id = result["id"] as? String,
                let last_name = result["last_name"] as? String
                else{
                    AppDelegate.shared.window?.makeToast("Error on facebook signin. Please try again.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    self.hideProgress()
                    return
            }
            let payload:Dictionary = ["FirstName":first_name,"LastName":last_name,"PhoneNumber":"" ,"Email":email,"UserType":"I","SCNKey":id] as Dictionary
            self.showProgress()
            NetworkInterface.fetchJSON(.facebookRegistration, payload: payload, requestCompletionHander: { (success, data, response, error) -> (Void) in
                DispatchQueue.main.async {
                    self.hideProgress()
                    if let ServiceStatus = data?["ServiceStatus"] as? [String:Any], ServiceStatus["StatusCode"] as? Int == 0 {
                        let instructor  = Instructor.shared
                        instructor.userID = data?["UserID"] as? Int
                        let name = data?["UserName"] as? String ?? " "
                        instructor.firstName = name.components(separatedBy: " ").first ?? ""
                        instructor.lastName = name.components(separatedBy: " ").count > 0 ? name.components(separatedBy: " ")[1] : ""
                        instructor.isInstructorEdit = data?["IsInstructorEdit"] as? Bool
                        instructor.email = email
                        instructor.synchronize()
                        
                        if Instructor.shared.isInstructorEdit == true {
                            let tabvc = self.storyboard?.instantiateViewController(withIdentifier: "instructorTabBar") as! UITabBarController
                            AppDelegate.shared.window?.rootViewController = tabvc
                            tabvc.selectedIndex = 3
                        }else{
                            self.fetchInstructorProfile()
                        }

                        
                    }else{
                        AppDelegate.shared.window?.makeToast("Error on facebook signin. Please try again.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                        FBSDKLoginManager().logOut()
                    }
                }
            })
            
            
        }
    }
    
    
    
    public func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        NSLog("Logged out here")
        //Empty all the values here
        
    }
    
}

extension LCDSignInViewController{
    func  viewSetup()  {
        cancelButton.titleLabel?.font = UIFont (name:FontName.REGULAR , size: CGFloat(FontSize.BUTTON_MEDIUM))
        loginButton.titleLabel?.font = UIFont (name:FontName.REGULAR , size: CGFloat(FontSize.BUTTON_MEDIUM))
        forgetButton.titleLabel?.font = UIFont (name:FontName.LIGHT , size: CGFloat(FontSize.BUTTON_SMALL))
        registerButton.titleLabel?.font = UIFont (name:FontName.LIGHT , size: CGFloat(FontSize.BUTTON_MEDIUM))
        newhereLabel.font = UIFont (name:FontName.LIGHT , size: CGFloat(FontSize.BUTTON_SMALL))
        cancelButton.backgroundColor = UIColorFromRGB(rgbValue: Color.GRAY_COLOR)
        loginButton.backgroundColor = UIColorFromRGB(rgbValue: Color.BUTTON_LIGHT_COLOR)
        
        
    }
}
