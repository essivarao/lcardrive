//
//  Constants.swift
//  LCD
//
//  Created by Admin on 15/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import Foundation

struct Color {
    static let  APP_THEMECOLOR =  0x18CDBC
    static let GRAY_COLOR = 0xD3D3D3
    static let BUTTON_LIGHT_COLOR = 0xEDD18E
}

struct FontName {
    static let REGULAR = "Montserrat-Regular"
    static let LIGHT = "Montserrat-Light"
    static let SEMIBOLD = "Montserrat-SemiBold"
}
struct CornerRadius {
    static let BUTTON_CORNERRADIUS = 5

    
}

struct FontSize {
    static let BUTTON_SMALL = 12
    static let BUTTON_MEDIUM = 14
    static let BUTTON_LARGE = 16
    
    static let LABEL_EXTRA_SMALL = 12
    static let LABEL_SMALL = 13
    static let LABEL_MEDIUM = 14
    static let LABEL_LARGE = 16
    static let LABEL_EXTRA_LARGE = 18
    
    static let TEXTFILED_EXTRA_SMALL = 12
    static let TEXTFILED_SMALL = 13
    static let TEXTFILED_MEDIUM = 14
    static let TEXTFILED_LARGE = 16
    
    
}
