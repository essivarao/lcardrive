

import Foundation
 

public class DPackage {
	public var packageId : Int?
	public var packageName : String?
	public var packageDesc : String?
	public var rate : Int?
	public var rateModeId : Int?
	public var rateMode : String?
	public var packageTypeId : Int?
	public var packageType : String?
    var hour: Int?

    
    public class func modelsFromDictionaryArray(array:NSArray) -> [DPackage]
    {
        var models:[DPackage] = []
        for item in array
        {
            models.append(DPackage(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {
        hour = dictionary["Hour"] as? Int
		packageId = dictionary["PackageId"] as? Int
		packageName = dictionary["PackageName"] as? String
		packageDesc = dictionary["PackageDesc"] as? String
		rate = dictionary["Rate"] as? Int
		rateModeId = dictionary["RateModeId"] as? Int
		rateMode = dictionary["RateMode"] as? String
		packageTypeId = dictionary["PackageTypeId"] as? Int
		packageType = dictionary["PackageType"] as? String
	}
}
