//
//  ExpandableCell.swift
//  ExpandableCellsExample
//
//  Created by DC on 28.08.2016.
//  Copyright © 2016 Dawid Cedrych. All rights reserved.
//

import UIKit

class ExpandableCell: UITableViewCell {
    var request: InstructorRequest?
    
    @IBOutlet weak var img: UIView!
    @IBOutlet weak var extraViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var fullNameLabel: UILabel!
    @IBOutlet var descriptionTextLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var callButton: UIButton!
    @IBOutlet var bookLessonButton: UIButton!
    @IBOutlet var viewMessageButton: UIButton!
    @IBOutlet var sendMessageButton: UIButton!
    @IBOutlet var prefrencesButton: UIButton!

    
    @IBOutlet var monMor: UIImageView!
    @IBOutlet var tueMor: UIImageView!
    @IBOutlet var wedMor: UIImageView!
    @IBOutlet var thuMor: UIImageView!
    @IBOutlet var friMor: UIImageView!
    @IBOutlet var satMor: UIImageView!
    @IBOutlet var sunMor: UIImageView!
    @IBOutlet var monAft: UIImageView!
    @IBOutlet var tueAft: UIImageView!
    @IBOutlet var wedAft: UIImageView!
    @IBOutlet var thuAft: UIImageView!
    @IBOutlet var friAft: UIImageView!
    @IBOutlet var satAft: UIImageView!
    @IBOutlet var sunAft: UIImageView!
    @IBOutlet var monEve: UIImageView!
    @IBOutlet var tueEve: UIImageView!
    @IBOutlet var wedEve: UIImageView!
    @IBOutlet var thuEve: UIImageView!
    @IBOutlet var friEve: UIImageView!
    @IBOutlet var satEve: UIImageView!
    @IBOutlet var sunEve: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let imageView = UIImageView()
        imageView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imageView.image = #imageLiteral(resourceName: "view_messages")
        viewMessageButton.addSubview(imageView)
        viewMessageButton.titleEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        
        let sendMessageView = UIImageView()
        sendMessageView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        sendMessageView.image = #imageLiteral(resourceName: "send_messages")
        sendMessageButton.addSubview(sendMessageView)
        sendMessageButton.titleEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        
        
        let prefrencesimageV = UIImageView()
        prefrencesimageV.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        prefrencesimageV.image = #imageLiteral(resourceName: "prefrences")
        prefrencesButton.addSubview(prefrencesimageV)
        sendMessageButton.titleEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        
//        
//        let addressLabelImageV = UIImageView()
//        addressLabelImageV.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
//        addressLabelImageV.image = #imageLiteral(resourceName: "address_marker")
//        addressLabel.addSubview(addressLabelImageV)
        
        
        let imageView4 = UIImageView()
        imageView4.frame = CGRect(x: 10 , y: 7, width: 18, height: 18)
        imageView4.image = #imageLiteral(resourceName: "call_plane")
        callButton.addSubview(imageView4)
        callButton.titleEdgeInsets = UIEdgeInsetsMake(0, 20 , 0, 0)
        
        bookLessonButton.layer.cornerRadius = 15
//        bookLessonButton.layer.borderColor = UIColor.gray.cgColor
//        bookLessonButton.layer.borderWidth = 2
        
        
        img.layer.borderWidth = 2
        img.layer.borderColor = UIColor.gray.cgColor
        
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width/2
        profileImageView.isUserInteractionEnabled = true
        profileImageView.layer.masksToBounds = true
        
    }
    
    func applyDataOnCell(reuest: InstructorRequest){
        sunMor.image = #imageLiteral(resourceName: "unselected_table_image")
        monMor.image = #imageLiteral(resourceName: "unselected_table_image")
        tueMor.image = #imageLiteral(resourceName: "unselected_table_image")
        wedMor.image = #imageLiteral(resourceName: "unselected_table_image")
        thuMor.image = #imageLiteral(resourceName: "unselected_table_image")
        friMor.image = #imageLiteral(resourceName: "unselected_table_image")
        satMor.image = #imageLiteral(resourceName: "unselected_table_image")
        monAft.image = #imageLiteral(resourceName: "unselected_table_image")
        tueAft.image = #imageLiteral(resourceName: "unselected_table_image")
        wedAft.image = #imageLiteral(resourceName: "unselected_table_image")
        thuAft.image = #imageLiteral(resourceName: "unselected_table_image")
        friAft.image = #imageLiteral(resourceName: "unselected_table_image")
        satAft.image = #imageLiteral(resourceName: "unselected_table_image")
        sunAft.image = #imageLiteral(resourceName: "unselected_table_image")
        monEve.image = #imageLiteral(resourceName: "unselected_table_image")
        tueEve.image = #imageLiteral(resourceName: "unselected_table_image")
        wedEve.image = #imageLiteral(resourceName: "unselected_table_image")
        thuEve.image = #imageLiteral(resourceName: "unselected_table_image")
        friEve.image = #imageLiteral(resourceName: "unselected_table_image")
        satEve.image = #imageLiteral(resourceName: "unselected_table_image")
        sunEve.image = #imageLiteral(resourceName: "unselected_table_image")
        
        
        self.fullNameLabel.text = request?.learnerName ?? ""
        self.addressLabel.text = request?.suburb ?? ""
        self.descriptionTextLabel.text = request?.lessonType ?? ""
        if let imageURL = request?.learnerProfilePic {
            let url = URL(string: imageURL)
            self.profileImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultFace"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        request?.learnerPreferredDays?.forEach({ (preference) in
            setSelectedSchedules(week: preference.weekDay?.lowercased() ?? "", period: preference.period?.lowercased() ?? "")
        })
        
    }
    
    func setSelectedSchedules(week: String, period: String){
        switch week {
        case "sun":
            switch period {
            case "m":
                sunMor.image = #imageLiteral(resourceName: "selected_image")
                break
            case "a":
                sunAft.image = #imageLiteral(resourceName: "selected_image")
                break
            case "e":
                sunEve.image = #imageLiteral(resourceName: "selected_image")
                break
            default:
                break
            }
            break
        case "mon":
            switch period {
            case "m":
                monMor.image = #imageLiteral(resourceName: "selected_image")
                break
            case "a":
                monAft.image = #imageLiteral(resourceName: "selected_image")
                break
            case "e":
                monEve.image = #imageLiteral(resourceName: "selected_image")
                break
            default:
                break
            }
            break
        case "tue":
            switch period {
            case "m":
                tueMor.image = #imageLiteral(resourceName: "selected_image")
                break
            case "a":
                tueAft.image = #imageLiteral(resourceName: "selected_image")
                break
            case "e":
                tueEve.image = #imageLiteral(resourceName: "selected_image")
                break
            default:
                break
            }
            break
        case "wed":
            switch period {
            case "m":
                wedMor.image = #imageLiteral(resourceName: "selected_image")
                break
            case "a":
                wedAft.image = #imageLiteral(resourceName: "selected_image")
                break
            case "e":
                wedEve.image = #imageLiteral(resourceName: "selected_image")
                break
            default:
                break
            }
            break
        case "thu":
            switch period {
            case "m":
                thuMor.image = #imageLiteral(resourceName: "selected_image")
                break
            case "a":
                thuAft.image = #imageLiteral(resourceName: "selected_image")
                break
            case "e":
                thuEve.image = #imageLiteral(resourceName: "selected_image")
                break
            default:
                break
            }
            break
        case "fri":
            switch period {
            case "m":
                friMor.image = #imageLiteral(resourceName: "selected_image")
                break
            case "a":
                friAft.image = #imageLiteral(resourceName: "selected_image")
                break
            case "e":
                friEve.image = #imageLiteral(resourceName: "selected_image")
                break
            default:
                break
            }
            break
        case "sat":
            switch period {
            case "m":
                satMor.image = #imageLiteral(resourceName: "selected_image")
                break
            case "a":
                satAft.image = #imageLiteral(resourceName: "selected_image")
                break
            case "e":
                satEve.image = #imageLiteral(resourceName: "selected_image")
                break
            default:
                break
            }
            break
        default:
            break
        }
    }
    
    var isExpanded:Bool = false
        {
        didSet
        {
            if !isExpanded {
                self.extraViewHeightConstraint.constant = 0.0
                
            } else {
                self.extraViewHeightConstraint.constant = 208.0
            }
        }
    }

}
