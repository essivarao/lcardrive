//
//  DiscountsTableViewCell.swift
//  DrivingAUS
//
//  Created by Kiran Sarella on 08/11/16.
//  Copyright © 2016 Sarella. All rights reserved.
//

import UIKit

class DiscountsTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var hourTextField: UITextField!
    
    var myIndexPath:IndexPath = IndexPath(row: 0, section: 0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
