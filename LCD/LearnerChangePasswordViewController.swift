//
//  LearnerChangePasswordViewController.swift
//  LCD
//
//  Created by Admin on 03/03/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import UIKit
import MBProgressHUD

class LearnerChangePasswordViewController: UIViewController {

    @IBOutlet var currentPassword: UITextField!
    @IBOutlet var newPassword: UITextField!
    @IBOutlet var confirmPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func cancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func submitClicked(_ sender: Any) {
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        

        
        if (!isValid(password: currentPassword.text!)) {
            appdelegate.window?.makeToast("Please enter current password", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        
       else if (!isValid(password: newPassword.text!)) {
            appdelegate.window?.makeToast("Please enter new password", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        
        else if (!isValid(password: confirmPassword.text!)) {
            appdelegate.window?.makeToast("Please enter confirm password", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
            
        else if (currentPassword.text == newPassword.text) {
            appdelegate.window?.makeToast("New password cannot be same as old password", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        
        else if (newPassword.text != confirmPassword.text) {
            appdelegate.window?.makeToast("Password doesn't match with new password", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
            return
        }
        
        else{
            let payload = [
                "UserId": Learner.shared.userID ?? "",
                "OldPassword": currentPassword.text!,
                "NewPassword": newPassword.text!
                ] as [String : Any]
            
            self.showProgress()
            NetworkInterface.fetchJSON(.changePassword, payload: payload) { (success, data, response, error) -> (Void) in
                DispatchQueue.main.async {
                    self.hideProgress()
                    if data?["StatusCode"] as? Int == 0, let message = data?["ServiceMessage"] as? String {

                        self.dismiss(animated: true, completion: {
                        })
//                        self.alert(title: "Success", message: message)
                          appdelegate.window?.makeToast( message, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)

                    }else if let message = data?["ErrorMessage"] as? String {
//                        self.alert(title: "Error", message: message)
                           appdelegate.window?.makeToast(message, duration: TimeInterval(kToastDuration), position: CSToastPositionTop)
                    }else{
//                        self.alert(title: "Error", message: "Please try again.")
                        appdelegate.window?.makeToast("Please try again.", duration: TimeInterval(kToastDuration), position: CSToastPositionTop)

                    }
                }
            }            
        }
        
    }
    
    func displayAlert(_ title:String,message:String)  {
        let alert = UIAlertController(title: title , message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) -> Void in
            NSLog("button OK")
        }
        
        alert.addAction(defaultAction)
        present(alert, animated: true, completion:nil)  // 11
    }
}
