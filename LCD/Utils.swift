//
//  Utils.swift
//  LCD
//
//  Created by Narendra Kumar R on 3/4/17.
//  Copyright © 2017 LCarDrive. All rights reserved.
//

import Foundation
struct Utils {
    static func sessionObjectPath() -> URL{
        let documentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
        return documentsDirectory.appendingPathComponent("usersession")
    }
    
    static func instructorSessionObjectPath() -> URL{
        let documentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
        return documentsDirectory.appendingPathComponent("instructorsession")
    }
    
    static func filtersPath() -> URL{
        let documentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
        return documentsDirectory.appendingPathComponent("filters")
    }
}


